//
//  SubProductsHeader.swift
//  Ded C
//
//  Created by iOS Dev 1 on 15/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class SubProductsHeader: UITableViewCell {

    @IBOutlet var sectionButton: UIButton!
    @IBOutlet var toggleImageView: UIImageView!
    @IBOutlet var headView: UIView!
    @IBOutlet var countLabel: UILabel!
    
    @IBOutlet var TitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
