//
//  SubProductsCell.swift
//  Ded C
//
//  Created by iOS Dev 1 on 15/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class SubProductsCell: UITableViewCell {

    @IBOutlet var countLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        countLabel.layer.borderWidth = 1.5
        countLabel.layer.backgroundColor = UIColor(red:19.0/255.0, green:66.0/255.0, blue:64.0/255.0, alpha:1).CGColor
        countLabel.layer.cornerRadius = 3.0
        countLabel.clipsToBounds = true

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func plusAction(sender: NSIndexPath) {
        let plusCount = Int(self.countLabel.text!)
        let value:NSString = String(plusCount!+1)
        self.countLabel.text = value as String
//        let storyboard = UIStoryboard(name: "Groceries", bundle: nil)
//        let vc = storyboard.instantiateViewControllerWithIdentifier("SubProductsViewController") as! SubProductsViewController
//        
//        header.countLabel.text = "5"
        
    }
   
    @IBAction func minusAction(sender: AnyObject) {
        let plusCount = Int(self.countLabel.text!)
        let value:NSString = String(plusCount!-1)
        if value == "0"{
            
        }
        else{
            self.countLabel.text = value as String

        }
    }
    
}
