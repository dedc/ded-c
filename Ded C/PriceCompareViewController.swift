//
//  PriceCompareViewController.swift
//  Ded C
//
//  Created by iOS Dev 1 on 12/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class PriceCompareViewController: UIViewController {
    
    var sectionTitleArray = NSArray()
    @IBOutlet var cartButton: BadgeButton!
    var langDict = NSDictionary()
    var selectDict = NSDictionary()
    
    @IBOutlet var placeOrderButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.langDict = LanguageClass.shared.languageDict.valueForKey("selectBranch") as! NSDictionary

       
        self.cartButton.badge.badgeValue = 1
        
        sectionTitleArray = ["Lulu Hyper","Hyper Panda"]
        self.title = self.langDict.valueForKey("selectBranch") as? String
        self.placeOrderButton.setTitle(self.langDict.valueForKey("placeOrder") as? String, forState: .Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        else{
            return 3
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         self.selectDict = LanguageClass.shared.languageDict.valueForKey("yourOrderDelivery") as! NSDictionary
        if DC_DEFAULTS.valueForKey("language") as? String == "ar"{
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("CellBranchArabic1", forIndexPath: indexPath) as! BranchCell
                cell.selectionStyle = .None
                cell.labelTitle.textColor = UIColor.blackColor()
                cell.labelTotal.text = self.selectDict.valueForKey("totalPrice") as? String
                cell.labelTitle.text = self.selectDict.valueForKey("branchAddress") as? String
                cell.statusBtn.setImage(UIImage(named: "check_white filled") , forState:.Selected)
                return cell
            }
            else { // section = 1
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCellWithIdentifier("CellBranchArabic1", forIndexPath: indexPath) as! BranchCell
                    cell.selectionStyle = .None
                     cell.labelTitle.textColor = UIColor.blackColor()
                    cell.labelTotal.text = self.selectDict.valueForKey("totalPrice") as? String
                    cell.labelTitle.text = self.selectDict.valueForKey("branchAddress") as? String
                     cell.statusBtn.setImage(UIImage(named: "check_white filled") , forState:.Selected)
                    return cell
                }
                else{
                    let cell = tableView.dequeueReusableCellWithIdentifier("CellBranchArabic2", forIndexPath: indexPath) as! BranchCell
                    cell.selectionStyle = .None
                     cell.labelTitle.textColor = UIColor.blackColor()
                    cell.labelTotal.text = self.selectDict.valueForKey("totalPrice") as? String
                    cell.labelTitle.text = self.selectDict.valueForKey("branchAddress") as? String
                     cell.statusBtn.setImage(UIImage(named: "check_white filled") , forState:.Selected)
                    return cell
                }
                
            }
        }
            else{
                if indexPath.section == 0 {
                    let cell = tableView.dequeueReusableCellWithIdentifier("CellBranchEnglish1", forIndexPath: indexPath) as! BranchCell
                    cell.selectionStyle = .None
                     cell.labelTitle.textColor = UIColor.blackColor()
                    cell.labelTotal.text = self.selectDict.valueForKey("totalPrice") as? String
                    cell.labelTitle.text = self.selectDict.valueForKey("branchAddress") as? String
                 cell.statusBtn.setImage(UIImage(named: "check_white filled") , forState:.Selected)
                return cell
                }
                else { // section = 1
                    if indexPath.row == 0 {
                        let cell = tableView.dequeueReusableCellWithIdentifier("CellBranchEnglish1", forIndexPath: indexPath) as! BranchCell
                        cell.selectionStyle = .None
                         cell.labelTitle.textColor = UIColor.blackColor()
                        cell.labelTotal.text = self.selectDict.valueForKey("totalPrice") as? String
                        cell.labelTitle.text = self.selectDict.valueForKey("branchAddress") as? String
                         cell.statusBtn.setImage(UIImage(named: "check_white filled") , forState:.Selected)
                        return cell
                    }
                    else{
                        let cell = tableView.dequeueReusableCellWithIdentifier("CellBranchEnglish2", forIndexPath: indexPath) as! BranchCell
                        cell.selectionStyle = .None
                         cell.labelTitle.textColor = UIColor.blackColor()
                        cell.labelTotal.text = self.selectDict.valueForKey("totalPrice") as? String
                        cell.labelTitle.text = self.selectDict.valueForKey("branchAddress") as? String
                         cell.statusBtn.setImage(UIImage(named: "check_white filled") , forState:.Selected)
                        return cell
                    }
                    
                }

            }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        if indexPath.section == 0 {
            return 70.0
        }
        else { // section = 1
            if indexPath.row == 0 {
                return 70.0
            }
            else{
                return 84.0
            }
        }
    }
    
    func tableView(tableView: UITableView,viewForHeaderInSection section: Int) -> UIView?{
        var header : UIView!
        if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            header = tableView.dequeueReusableCellWithIdentifier("HeaderArabic")! as UIView
            let titleLabel:UILabel = header.viewWithTag(200) as! UILabel
            // NSLog("Text: %@", titleLabel.text!)
            titleLabel.text = sectionTitleArray.objectAtIndex(section) as? String
            
        }
        if DC_DEFAULTS.valueForKey("language") as! String == "en"{
            header = tableView.dequeueReusableCellWithIdentifier("HeaderEnglish")! as UIView
            let titleLabel:UILabel = header.viewWithTag(200) as! UILabel
            // NSLog("Text: %@", titleLabel.text!)
            titleLabel.text = sectionTitleArray.objectAtIndex(section) as? String
            }
        return header
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 60
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! BranchCell
        cell.contentView.backgroundColor = UIColor.init(red: 63.0/255.0, green: 88.0/255.0, blue: 99.0/255.0, alpha: 1.0)
        cell.contentView.layer.borderWidth = 2.3
        cell.contentView.layer.borderColor = UIColor.init(red: 239.0/255.0, green: 230.0/255.0, blue: 71.0/255.0, alpha: 1.0).CGColor
        cell.labelTotal.textColor = UIColor.init(red: 239.0/255.0, green: 230.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        cell.labelPrice.textColor = UIColor.init(red: 239.0/255.0, green: 230.0/255.0, blue: 71.0/255.0, alpha: 1.0)
        cell.statusBtn.selected = true
        cell.labelTitle.textColor = UIColor.whiteColor()
        
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath){
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! BranchCell
        cell.contentView.backgroundColor = UIColor.init(red: 233.0/255.0, green: 242.0/255.0, blue: 241.0/255.0, alpha: 1.0)
        cell.contentView.layer.borderWidth = 2.3
        cell.contentView.layer.borderColor = UIColor.init(red: 233.0/255.0, green: 242.0/255.0, blue: 241.0/255.0, alpha: 1.0).CGColor
        cell.labelTotal.textColor = UIColor.init(red: 19.0/255.0, green: 66.0/255.0, blue: 64.0/255.0, alpha: 1.0)
        cell.labelPrice.textColor = UIColor.init(red: 19.0/255.0, green: 66.0/255.0, blue: 64.0/255.0, alpha: 1.0)
        cell.statusBtn.selected = false
        cell.labelTitle.textColor = UIColor.blackColor()
    }

   
}
