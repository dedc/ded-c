//
//  AppDelegate.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 01/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Variables
    var loadingView: UIView = UIView()
    var actView: UIView = UIView()
    var titleLabel: UILabel = UILabel()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var iPadPortrait = false
    var window: UIWindow?
    var reachability: Reachability!
    let useClosures = false
    
    var pickUpType = ""
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        DC_DEFAULTS.setObject("en", forKey: "language")
        print(LANGUAGE)
        LanguageClass.shared.loadTranslation()
       
        // Override point for customization after application launch.
        checkReachability()
        customizeNavigationBarAppearance()
        
        if iPhone {
            UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: true)
        }
        
        
        return true
    }
    
    //MARK:- Reachability
    func checkReachability() {
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
        }
        
        if (useClosures) {
            reachability.whenReachable = { reachability in
                IS_REACHABLE = true
            }
            reachability.whenUnreachable = { reachability in
                IS_REACHABLE = false
                //                self.NoNetwork()
            }
        } else {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        }
        do {
            try reachability.startNotifier()
        }
        catch let error as NSError {
            print(error)
        }
        
        // Initial reachability check
        if reachability.isReachable() {
            IS_REACHABLE = true
        } else {
            IS_REACHABLE = false
            
        }
        
    }
    deinit {
        reachability.stopNotifier()
        if (!useClosures) {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: ReachabilityChangedNotification, object: nil)
        }
    }
    func reachabilityChanged(note: NSNotification) {
        let reachability = note.object as? Reachability
        if reachability!.isReachable() {
            IS_REACHABLE = true
        } else {
            IS_REACHABLE = false
            DC_DELEGATE.showAlert("Message", message: "No Internet Connection", buttonTitle: "OK")
        }
    }

    func showActivity(myView: UIView, myTitle: String) {
        myView.userInteractionEnabled = false
        myView.window?.userInteractionEnabled = false
        myView.endEditing(true)
        actView.frame = CGRectMake(0, 0, myView.frame.width, myView.frame.height)
        actView.center = myView.center
        actView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = myView.center
        loadingView.backgroundColor = THEME_COLOUR
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 15
        
        activityIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        activityIndicator.center = CGPointMake(loadingView.frame.size.width / 2, loadingView.frame.size.height / 2);
        
        titleLabel.frame = CGRectMake(5, loadingView.frame.height-20, loadingView.frame.width-10, 20)
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.text = myTitle
        titleLabel.font = UIFont(name: FONT_NAME_R, size: 10)
        
        loadingView.addSubview(activityIndicator)
        actView.addSubview(loadingView)
        loadingView.addSubview(titleLabel)
        myView.addSubview(actView)
        activityIndicator.startAnimating()
    }
    func removeActivity(myView: UIView) {
        myView.userInteractionEnabled = true
        myView.window?.userInteractionEnabled = true
        activityIndicator.stopAnimating()
        actView.removeFromSuperview()
    }
    func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask
    {
        if iPad && iPadPortrait == false
        {
            return UIInterfaceOrientationMask.Landscape
        }
        
        return UIInterfaceOrientationMask.Portrait
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }


    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "DC.Ded_C" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Ded_C", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    //MARK : navigationBar Customization
    
    func customizeNavigationBarAppearance()
    {
        var barColor: UIColor!
        if iPad {
            barColor = UIColor(red:0.11, green:0.49, blue:0.48, alpha:1.0)
        }else {
            barColor = UIColor.clearColor()
        }
        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: .Default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().translucent = true
        UINavigationBar.appearance().backgroundColor = barColor
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        let statusBar: UIView = UIApplication.sharedApplication().valueForKey("statusBar") as! UIView
        if statusBar.respondsToSelector(Selector("setBackgroundColor:")) {
            statusBar.backgroundColor = barColor
        }
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -120), forBarMetrics: UIBarMetrics.Default)
        UIBarButtonItem.appearance().tintColor = UIColor.whiteColor()
    }

}

extension String {
    func localized(lang:String) ->String {
        
        let path = NSBundle.mainBundle().pathForResource(lang, ofType: "lproj")
        let bundle = NSBundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}