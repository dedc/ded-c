//
//  DCLoginTable.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 27/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit
protocol loginDelegate {
    func didClickedLogin(isLoggeIn: Bool)
}
class DCLoginTable: UITableViewController {

    // MARK: - Variables
    
    var delegate: loginDelegate?
    @IBOutlet weak var UserNameText: UITextField!
    @IBOutlet weak var PasswordText: UITextField!
    var userPlaceHolder = "User Name"
    var passwordPlaceHolder = "Password"
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    // MARK: - Actions
    @IBAction func Login(sender: AnyObject) {
        delegate?.didClickedLogin(true)
    }
    @IBAction func ForgetPassword(sender: AnyObject) {
    }

    // MARK: - TextField Delegates
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == UserNameText {
            if textField.text == userPlaceHolder {
                textField.text = ""
            }
        }else {
            if textField.text == passwordPlaceHolder {
                textField.text = ""
                textField.secureTextEntry = true
            }
        }
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == UserNameText {
            if textField.text == userPlaceHolder || textField.text == "" {
                textField.text = userPlaceHolder
            }
        }else {
            if textField.text == passwordPlaceHolder || textField.text == "" {
                textField.text = passwordPlaceHolder
                textField.secureTextEntry = false
            }
        }
    }
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    /*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
