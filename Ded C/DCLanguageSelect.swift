//
//  DCLanguageSelect.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 26/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCLanguageSelect: UIViewController {

    // MARK: - Variables
    
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.navigationController?.navigationBarHidden = true
        // Do any additional setup after loading the view.
    }

    // MARK: - Actions
    @IBAction func English(sender: AnyObject) {
        if DC_DEFAULTS.valueForKey("language") as! String != "en" {
            DC_DEFAULTS.setValue("en", forKey: "language")
            LANGUAGE = "en"
            DCStoryBoards.sharedStoryBoard.changeLanguage()
        }
        LanguageClass.shared.loadTranslation()
        performSegueWithIdentifier("Login", sender: self)
    }
    
    @IBAction func Arabic(sender: AnyObject) {
        if DC_DEFAULTS.valueForKey("language") as! String != "ar" {
            DC_DEFAULTS.setValue("ar", forKey: "language")
            LANGUAGE = "ar"
            DCStoryBoards.sharedStoryBoard.changeLanguage()
        }
        LanguageClass.shared.loadTranslation()
        performSegueWithIdentifier("Login", sender: self)
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
