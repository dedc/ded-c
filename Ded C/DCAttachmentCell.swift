//
//  DCAttachmentView.swift
//  Ded C
//
//  Created by Focaloid on 8/6/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

protocol DCAttachmentDelegate : NSObjectProtocol
{
    func attachmentDidCloseItem(attachmentCell:DCAttachmentCell)
    func attachmentDidChangeText(attachmentCell:DCAttachmentCell, text:String)
}

class DCAttachmentCell: UITableViewCell,UITextViewDelegate
{

    @IBOutlet var centreView: UIView!
    @IBOutlet var attachmentImageView: UIImageView!
    @IBOutlet var commentTextView: UITextView!
    
    var placeHolderText = "Add Comment to attachment".localized(LANGUAGE!)
    
    weak var attachmentDelegate : DCAttachmentDelegate?
    
    @IBAction func closeAction(sender: AnyObject)
    {
        attachmentDelegate?.attachmentDidCloseItem(self)
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        centreView.clipsToBounds = false
        
//        centreView.layer.shadowColor = UIColor.blackColor().CGColor
//        centreView.layer.masksToBounds = false
//        centreView.layer.shadowOffset = CGSize(width: 2, height: 1)
//        centreView.layer.shadowOpacity = 0.5
//        let rect = CGRect(x: -5, y: 5, width: UIScreen.mainScreen().bounds.size.width - 66, height: 133)
//        centreView.layer.shadowPath = UIBezierPath(rect:rect).CGPath
//        centreView.layer.shadowRadius = 2
        
    }
    
    
    
    //MARK: TextView DelegateMethods
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        
        var text = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
        
        if text == placeHolderText
        {
            text = ""
        }
        
        self.attachmentDelegate?.attachmentDidChangeText(self, text: text)
        
        return true
        
    }
   
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == placeHolderText || textView.text == ""
        {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = placeHolderText
        }
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        
        if textView.text == placeHolderText
        {
            textView.textColor = UIColor.blackColor()
            textView.text = ""
        }

        
        return true
    }
    
}
