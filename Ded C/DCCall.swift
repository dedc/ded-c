//
//  DCCall.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 03/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCCall: UIViewController {

    //MARK:- Variables
    @IBOutlet weak var InfoContainer: UIView!
    @IBOutlet weak var formEnglishContainer: UIView!
    @IBOutlet weak var contactSegment: ADVSegmentedControl!
    
    //MARK:- View Methods
    override func viewWillAppear(animated: Bool) {
//        self.navigationController?.navigationBarHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        contactSegment.items = ["Form", "Info"]
        // Do any additional setup after loading the view.
    }

    //MARK:- System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
