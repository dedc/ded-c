//
//  SelectProductsViewController.swift
//  Ded C
//
//  Created by iOS Dev 1 on 12/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class SelectProductsViewController: UIViewController {

    @IBOutlet var cartButton: BadgeButton!
    
    var fromWhichView = ""
    var langDict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.langDict = LanguageClass.shared.languageDict.valueForKey("selectProducts") as! NSDictionary
        
        if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            self.title = self.langDict.valueForKey("selectProducts")as? String
        }
        else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
            self.title = self.langDict.valueForKey("selectProducts")as? String
        }

        

        
    self.cartButton.badge.badgeValue = 1
       

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 10
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ProductsCell",forIndexPath: indexPath) as! PromotionsCell
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView!,
                        layout collectionViewLayout: UICollectionViewLayout!,
                               sizeForItemAtIndexPath indexPath: NSIndexPath!) -> CGSize {
        return CGSize(width:((self.view.frame.size.width/2)-20), height:((self.view.frame.size.width/2)-20))
    }
    

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
      let subProductsVC = self.storyboard?.instantiateViewControllerWithIdentifier("SubProductsViewController") as! SubProductsViewController
       subProductsVC.viewType = fromWhichView
        self.navigationController?.pushViewController(subProductsVC, animated: true)
        
    }

}
