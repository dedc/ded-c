//
//  PromotionsMainVC.swift
//  Ded C
//
//  Created by iOS Dev 1 on 09/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit
import Sheriff

class PromotionsMainVC: UIViewController {

    @IBOutlet var cartButton: BadgeButton!
    var langDict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.langDict = LanguageClass.shared.languageDict.valueForKey("buyYourGroceries") as! NSDictionary
        if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            self.title = self.langDict.valueForKey("promotions") as? String
        }else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
            self.title = self.langDict.valueForKey("promotions") as? String
        }
        
        self.cartButton.badge.badgeValue = 1
  

    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 10
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PromotionsCell",
                                                                         forIndexPath: indexPath) as! PromotionsCell
        if(indexPath.row == 0){
            cell.itemImageView.image = UIImage(named: "1.jpg")
            
        }
        if(indexPath.row == 1){
            cell.itemImageView.image = UIImage(named: "2.jpg")
            
        }
        
        if(indexPath.row == 2){
            cell.itemImageView.image = UIImage(named: "3.jpg")
            
        }
        
        if(indexPath.row == 3){
            cell.itemImageView.image = UIImage(named: "4.jpg")
            
        }
        if(indexPath.row == 4){
            cell.itemImageView.image = UIImage(named: "2.jpg")
            
        }
        if(indexPath.row == 5){
            cell.itemImageView.image = UIImage(named: "3.jpg")
            
        }
        if(indexPath.row == 6){
            cell.itemImageView.image = UIImage(named: "1.jpg")
            
        }
        if(indexPath.row == 7){
            cell.itemImageView.image = UIImage(named: "2.jpg")
            
        }
        if(indexPath.row == 8){
            cell.itemImageView.image = UIImage(named: "3.jpg")
            
        }
        if(indexPath.row == 9){
            cell.itemImageView.image = UIImage(named: "4.jpg")
            
        }
        
        
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView!,
                        layout collectionViewLayout: UICollectionViewLayout!,
                               sizeForItemAtIndexPath indexPath: NSIndexPath!) -> CGSize {
        return CGSize(width:((self.view.frame.size.width/2)-20), height:((self.view.frame.size.width/2)-20))
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
       // self.performSegueWithIdentifier("ShowDetailView", sender: indexPath)
        let subProductsVC = self.storyboard?.instantiateViewControllerWithIdentifier("DetailMainVC") as! DetailMainVC
        self.navigationController?.pushViewController(subProductsVC, animated: true)
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
