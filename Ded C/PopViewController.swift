//
//  PopViewController.swift
//  Ded C
//
//  Created by iOS Dev 1 on 10/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit


class PopViewController: UIViewController {

    var tabType = ""           // grocery or cart
    var clickOption = ""       // (grocery case): control coming from detail page or start shopping click
    
    @IBOutlet var startShoppingLabel: UILabel!
    @IBOutlet var pickupButton: UIButton!
    @IBOutlet var deliveryButtton: UIButton!
    var langDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.langDict = LanguageClass.shared.languageDict.valueForKey("shoppingType") as! NSDictionary
        if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            let pickup = self.langDict.valueForKey("pickUp") as! String
            self.pickupButton.setTitle(pickup, forState: .Normal)
            let delivery = self.langDict.valueForKey("delivery") as! String
            self.deliveryButtton.setTitle(delivery, forState: .Normal)
            let start = self.langDict.valueForKey("startShopping") as! String
            self.startShoppingLabel.text = start
        }
        else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
            let pickup = self.langDict.valueForKey("pickUp") as! String
            self.pickupButton.setTitle(pickup, forState: .Normal)
            let delivery = self.langDict.valueForKey("delivery") as! String
            self.deliveryButtton.setTitle(delivery, forState: .Normal)
            let start = self.langDict.valueForKey("startShopping") as! String
            self.startShoppingLabel.text = start
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("DeliveryType")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    

    @IBAction func deliveryBtnClk(sender: AnyObject) {
        
    NSUserDefaults.standardUserDefaults().setObject("Delivery", forKey: "DeliveryType")
    NSUserDefaults.standardUserDefaults().synchronize()
  
      self.presentingPopinViewController().dismissCurrentPopinControllerAnimated(true)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
        if self.tabType == "Grocery" {
            if self.clickOption == "GroceryDetail" {
                let editProductsVC = self.storyboard?.instantiateViewControllerWithIdentifier("EditViewController") as! EditViewController
                editProductsVC.fromWhichView = "GroceryTab"
                self.navigationController?.pushViewController(editProductsVC, animated: true)
            }
            else if self.clickOption == "StartShopping"{
                let selectBranchesVC = self.storyboard?.instantiateViewControllerWithIdentifier("SelectBranchesViewController") as! SelectBranchesViewController
                selectBranchesVC.fromWhichView = "GroceryTab"
                self.navigationController?.pushViewController(selectBranchesVC, animated: true)
            }
        }
           
        else if self.tabType == "Cart"{
            let selectBranchesVC = self.storyboard?.instantiateViewControllerWithIdentifier("SelectBranchesViewController") as! SelectBranchesViewController
            selectBranchesVC.fromWhichView = "CartTab"
            self.navigationController?.pushViewController(selectBranchesVC, animated: true)
          }
        }
    }
   
    
    
    @IBAction func pickUpBtnClk(sender: AnyObject) {
        
        NSUserDefaults.standardUserDefaults().setObject("PickUp", forKey: "DeliveryType")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        self.presentingPopinViewController().dismissCurrentPopinControllerAnimated(true)
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
            
            if self.tabType == "Grocery" {
                if self.clickOption == "GroceryDetail" {
                    let editProductsVC = self.storyboard?.instantiateViewControllerWithIdentifier("EditViewController") as! EditViewController
                    editProductsVC.fromWhichView = "GroceryTab"
                    self.navigationController?.pushViewController(editProductsVC, animated: true)
                }
                else if self.clickOption == "StartShopping"{
                    let selectBranchesVC = self.storyboard?.instantiateViewControllerWithIdentifier("SelectBranchesViewController") as! SelectBranchesViewController
                    selectBranchesVC.fromWhichView = "GroceryTab"
                    self.navigationController?.pushViewController(selectBranchesVC, animated: true)
                }
            }
                
            else if self.tabType == "Cart"{
                let selectBranchesVC = self.storyboard?.instantiateViewControllerWithIdentifier("SelectBranchesViewController") as! SelectBranchesViewController
                selectBranchesVC.fromWhichView = "CartTab"
                self.navigationController?.pushViewController(selectBranchesVC, animated: true)
            }
        }
    }
}
