//
// DCBusinessComplaintTableVC.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 03/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

@objc protocol DefendandDelegate
{
    func defendandClicked(isClicked: Bool)
}

class DCBusinessComplaintTableVC: UITableViewController
{

    // MARK: - Variables
    
    @IBOutlet weak var sectionSpace: UITableViewCell!
    @IBOutlet var tradeLicenseTableView: DCAttachmentTableView!
    @IBOutlet var proxyTableView: DCAttachmentTableView!
    
    var delegate: DefendandDelegate?
    let placeholderText = "Address".localized(LANGUAGE!)
    @IBOutlet var originLabel: UILabel!
    
    @IBOutlet var tradeOriginButtons: [UIButton]!
    @IBOutlet var tradeOriginLabels: [UILabel]!
    @IBOutlet var tradeOriginCheck: [UIImageView]!
    
    @IBOutlet weak var tradeName: DCImagedTextField!
    @IBOutlet weak var tradeLicence: DCImagedTextField!
    @IBOutlet weak var webSite: DCImagedTextField!
    @IBOutlet weak var email: DCImagedTextField!
    @IBOutlet weak var phone: DCImagedTextField!
    @IBOutlet weak var mobile: DCImagedTextField!
    @IBOutlet weak var address: UITextView!
    @IBOutlet weak var poBox: UITextField!
    @IBOutlet weak var repName: DCImagedTextField!
    @IBOutlet weak var jobTitle: DCImagedTextField!
    @IBOutlet weak var repEmail: DCImagedTextField!
    @IBOutlet weak var repMobile: DCImagedTextField!
    var variables = DCBusinessVariables()
    
    // MARK: - View Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if iPad {
            sectionSpace.backgroundColor = UIColor.clearColor()
        }
//        self.tradeLicenseTableView.attachmentCellDelegate = self
//        self.proxyTableView.attachmentCellDelegate = self
        
        self.tradeLicenseTableView.type = "trade"
        self.proxyTableView.type = "proxy"
        
        self.tradeLicenseTableView.proxyCell = true
        self.proxyTableView.proxyCell = true

    }
    
    //MARK: TableView Delegate Methods
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if iPhone {
            if indexPath.row == 1
            {
                return 131
            }
            if indexPath.row == 8
            {
                return 67
            }
            if indexPath.row == 9
            {
                return 50
            }
            
            if indexPath.row == 18
            {
                return 60
            }
            
            if indexPath.row == 11 || indexPath.row == 19
            {
                return 15
            }
            
            if indexPath.row == 17 // proxy attachemnt
            {
                return 50 + CGFloat(self.proxyTableView.attachmentsArray.count) * 80
            }
            
            
            if indexPath.row == 10 // trade license attachment
            {
                return 50 + CGFloat(self.tradeLicenseTableView.attachmentsArray.count) * 80
                
            }
        }else {
            if indexPath.row == 1
            {
                return 131
            }
            if indexPath.row == 6
            {
                return 67
            }
            if indexPath.row == 7 // trade license attachment
            {
                return 50 + CGFloat(self.tradeLicenseTableView.attachmentsArray.count) * 80
                
            }
            if indexPath.row == 8
            {
                return 16
            }
            if indexPath.row == 12 // proxy attachemnt
            {
                return 50 + CGFloat(self.proxyTableView.attachmentsArray.count) * 80
            }
            if indexPath.row == 13
            {
                return 60
            }
            
            
        }
        
        return 44
    }

    // MARK: - TextField Delegates
    func textFieldDidEndEditing(textField: UITextField) {
        
        switch textField {
            case tradeName:
                variables.complaint_tradeName = textField.text!
            case tradeLicence:
                variables.complaint_tradeLicence = textField.text!
            case webSite:
                variables.complaint_website = textField.text!
            case email:
                variables.complaint_email = textField.text!
            case phone:
                variables.complaint_phone = textField.text!
            case mobile:
                variables.complaint_mobile = textField.text!
            case address:
                variables.complaint_address = textField.text!
            case poBox:
                variables.complaint_poBox = textField.text!
            case repName:
                variables.complaint_repName = textField.text!
            case jobTitle:
                variables.complaint_repJobTitle = textField.text!
            case repEmail:
                variables.complaint_repEmail = textField.text!
            case repMobile:
                variables.complaint_repMobile = textField.text!
        default:
            break;
        }
        print(variables)
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if textView.text == placeholderText
        {
            textView.textColor = UIColor.blackColor()
            textView.text = ""
        }
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == placeholderText || textView.text == ""
        {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = placeholderText
        }
    }
    // MARK: - Actions 
    
    @IBAction func addProxyAttachment(sender: AnyObject)
    {
//        let commentVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.AddCommentVC) as! DCAddCommentVC
//        commentVC.type = "proxy"
//        commentVC.commentDelegate = self
//        self.presentViewController(commentVC, animated: true, completion: nil)
        
        if self.proxyTableView.attachmentsArray.count > 0
        {
            if self.proxyTableView.attachmentsArray[0].images.count >= 5
            {
                UIAlertView(title: "", message: "5 Images are supported", delegate: nil, cancelButtonTitle: "OK").show()
                return
            }
        }
        
        self.openImagesActionSheet
            { (image) in
                
                if let pickedImage = image
                {
                    if self.proxyTableView.attachmentsArray.count > 0
                    {
                        self.proxyTableView.attachmentsArray[0].images.append(pickedImage)
                    }
                    else
                    {
                        let attachment = DCAttachmentObject()
                        attachment.images.append(pickedImage)
                        self.proxyTableView.attachmentsArray = [attachment]
                    }
                    
                    self.proxyTableView.commentArray.append("")
                    self.proxyTableView.reloadData()
                    self.tableView.reloadData()
                }
                
        }

        
    }
    
    @IBAction func addTradeLicenseAttachment(sender: AnyObject)
    {
        if self.tradeLicenseTableView.attachmentsArray.count > 0
        {
            if self.tradeLicenseTableView.attachmentsArray[0].images.count >= 5
            {
                UIAlertView(title: "", message: "5 Images are supported", delegate: nil, cancelButtonTitle: "OK").show()
                return 
            }
        }
        
        self.openImagesActionSheet
        { (image) in
            
            if let pickedImage = image
            {
                if self.tradeLicenseTableView.attachmentsArray.count > 0
                {
                    self.tradeLicenseTableView.attachmentsArray[0].images.append(pickedImage)
                }
                else
                {
                    let attachment = DCAttachmentObject()
                    attachment.images.append(pickedImage)
                    self.tradeLicenseTableView.attachmentsArray = [attachment]
                }
                
                self.tradeLicenseTableView.commentArray.append("")
                self.tradeLicenseTableView.reloadData()
                self.tableView.reloadData()
            }
            
        }
        
//        let commentVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.AddCommentVC) as! DCAddCommentVC
//        commentVC.type = "trade"
//        commentVC.commentDelegate = self
//        self.presentViewController(commentVC, animated: true, completion: nil)
        
        
    }
    
    @IBAction func tradeOriginAction(sender: UIButton)
    {
        if let selectedIndex = tradeOriginButtons.indexOf(sender)
        {
            for label in tradeOriginLabels
            {
                label.textColor = UIColor.lightGrayColor()
                
            }
            for imageView in tradeOriginCheck {
                imageView.hidden = true
            }
            tradeOriginCheck[selectedIndex].hidden = false
            tradeOriginLabels[selectedIndex].textColor = UIColor.blackColor()
            
        }
    }
    
    
    
    @IBAction func DefendandDetails(sender: AnyObject)
    {
        delegate?.defendandClicked(true)
    }
    
    @IBAction func originAction(sender: AnyObject)
    {
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var originArray: [String] = []
        
        if LOOKUP_ORIGIN?.count != 0 {
            for i in 0..<LOOKUP_ORIGIN!.count {
                originArray.append(LOOKUP_ORIGIN![i][fetchKey] as! String)
            }
        }
        
        self.showSelectionTableView("ORIGIN", options: originArray)
        { (selectedString, index) in
            
            self.originLabel.text = selectedString
            
        }
    }
    
    
    // MARK: - System
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    //MARK: Multiple Attachment Delegates
//    
//    func attachmentCellDidPressMore(cell: DCMultipleAttachmentCell)
//    {
//        
//        let alertController = UIAlertController()
//        
//        let editAction = UIAlertAction(title: "Edit", style: .Default)
//        { (action) in
//            
//            
//            let commentVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.AddCommentVC) as! DCAddCommentVC
//            commentVC.commentDelegate = self
//            commentVC.images = cell.images
//            commentVC.comment = cell.comment
//            commentVC.editMode = true
//            if let indexPath = self.tradeLicenseTableView.indexPathForCell(cell) where cell.attachmentType == "trade"
//            {
//                commentVC.indexPath = indexPath
//                commentVC.type = "trade"
//
//            }
//            if let indexPath = self.proxyTableView.indexPathForCell(cell) where cell.attachmentType == "proxy"
//            {
//                commentVC.indexPath = indexPath
//                commentVC.type = "proxy"
//
//            }
//            self.presentViewController(commentVC, animated: true, completion: nil)
//            
//        }
//        
//        let deleteAction = UIAlertAction(title: "Delete", style: .Default)
//        { (action) in
//            
//            
//            if let indexPath = self.tradeLicenseTableView.indexPathForCell(cell) where cell.attachmentType == "trade"
//            {
//            
//                self.tradeLicenseTableView.attachmentsArray.removeAtIndex(indexPath.row)
//                self.tradeLicenseTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
//                self.tableView.beginUpdates()
//                self.tableView.endUpdates()
//                
//            }
//            
//            if let indexPath = self.proxyTableView.indexPathForCell(cell) where cell.attachmentType == "proxy"
//            {
//                
//                self.proxyTableView.attachmentsArray.removeAtIndex(indexPath.row)
//                //                self.tradeLicenseTableView.commentArray.removeAtI\ ndex(indexPath.row)
//                self.proxyTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
//                self.tableView.beginUpdates()
//                self.tableView.endUpdates()
//                
//            }
//
//            
//        }
//        
//        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel)
//        { (action) in
//            
//            alertController.dismissViewControllerAnimated(true
//                , completion:nil)
//        }
//        
//        alertController.addAction(editAction)
//        alertController.addAction(deleteAction)
//        alertController.addAction(cancelAction)
//        
//        self.presentViewController(alertController, animated: true, completion: nil)
//        
//    }
//
    
//    //MARK:  Attachment Delegate Methods
//    
//    func attachmentDidCloseItem(attachmentCell:DCAttachmentCell)
//    {
//        if let indexPath = self.tradeLicenseTableView.indexPathForCell(attachmentCell)
//        {
//            self.tradeLicenseTableView.attachmentsArray.removeAtIndex(indexPath.row)
//            self.tradeLicenseTableView.commentArray.removeAtIndex(indexPath.row)
//            self.tradeLicenseTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
//            self.tableView.beginUpdates()
//            self.tableView.endUpdates()
//        }
//    }
//    
//    func attachmentDidChangeText(attachmentCell:DCAttachmentCell, text:String)
//    {
//        if let indexPath = self.tradeLicenseTableView.indexPathForCell(attachmentCell)
//        {
//            self.tradeLicenseTableView.commentArray[indexPath.row] = text
//        }
//    }
//  
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
//    
//    //MARK: Add Attchment s Delegate
//    
//    func commentVCDidAddComments(attachmentObject: DCAttachmentObject,type:String)
//    {
//        if type == "trade"
//        {
//            self.tradeLicenseTableView.attachmentsArray.append(attachmentObject)
//            self.tradeLicenseTableView.reloadData()
//            self.tableView.reloadData()
//        }
//        else
//        {
//            self.proxyTableView.attachmentsArray.append(attachmentObject)
//            self.proxyTableView.reloadData()
//            self.tableView.reloadData()
//        }
//
//    }
//    
//    func commentVCDidEditComments(attachmentObject : DCAttachmentObject,indexPath : NSIndexPath?,type:String)
//    {
//        
//        
//        if let indexPath = indexPath where type == "trade"
//        {
//            self.tradeLicenseTableView.attachmentsArray[indexPath.row] = attachmentObject
//            self.tradeLicenseTableView.reloadData()
//            self.tableView.reloadData()
//        }
//        
//        if let indexPath = indexPath where type == "proxy"
//        {
//            self.proxyTableView.attachmentsArray[indexPath.row] = attachmentObject
//            self.proxyTableView.reloadData()
//            self.tableView.reloadData()
//        }
//        
//    }
//    
}
