//
//  LanguageClass.swift
//  KFIC
//
//  Created by Abdur Rehman on 09/11/15.
//  Copyright © 2015 hashinclude.io. All rights reserved.
//

import UIKit

class LanguageClass: NSObject {
    static let shared = LanguageClass()
    var languageDict = NSDictionary()
    
    func loadTranslation() {
        let plist = NSBundle.mainBundle().pathForResource("LanguageList", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: plist)!
        let lang = DC_DEFAULTS.stringForKey("language")!
        languageDict = dict.valueForKey(lang) as! NSDictionary
        print(languageDict)
    }
    
}
