//
//  DCTradeAttachmentCell.swift
//  Ded C
//
//  Created by Focaloid on 8/15/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCTradeAttachmentCell: UITableViewCell
{
    @IBOutlet var imagesCollection: UICollectionView!
    
    @IBOutlet var centreView: UIView!
    
    var images : [UIImage] = []
    var attachmentType : String = ""    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        imagesCollection.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "imageCell")
        
        // Initialization code
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        centreView.clipsToBounds = false
        
        //        centreView.layer.shadowColor = UIColor.blackColor().CGColor
        //        centreView.layer.masksToBounds = false
        //        centreView.layer.shadowOffset = CGSize(width: 2, height: 2)
        //        centreView.layer.shadowOpacity = 0.5
        //        let rect = CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.size.width - 66, height: 130)
        //        centreView.layer.shadowPath = UIBezierPath(rect:rect).CGPath
        //        centreView.layer.shadowRadius = 2
        
    }
    
    
    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
      //MARK : CollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("imageCell", forIndexPath: indexPath)
        
        var imageView = cell.contentView.viewWithTag(102) as? UIImageView
        if imageView == nil
        {
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            imageView?.contentMode =  .ScaleToFill
            imageView?.clipsToBounds = true
            imageView?.tag = 102
            cell.contentView.addSubview(imageView!)
        }
        
        imageView?.image = images[indexPath.row]
        
        
        return cell
        
        
    }
    
}
