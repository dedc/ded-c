//
//  SelectBrandsVC.swift
//  Ded C
//
//  Created by iOS Dev 1 on 10/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class SelectBrandsVC: UIViewController {

    @IBOutlet var brandsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int{
        return 5
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 5
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = brandsTableView.dequeueReusableCellWithIdentifier("SelectBrandCell", forIndexPath: indexPath) as! SelectBrandCell
        
        return cell
    }
    
//    func headerViewForSection(section: Int) -> UITableViewHeaderFooterView?{
//        
//        return
//    }
//    
    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
//        
//
//    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
