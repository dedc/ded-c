 //
//  DCBusinessComplaintDetailsMainVC.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 04/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCBusinessComplaintDetailsMainVC: UIViewController, showAlertDelegate,DCComplaintSubmitDelegate {

    // MARK: - Variables
    
    let popupConfig = STZPopupViewConfig()

    
    // MARK: - View Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.addTapGesture()

        // Do any additional setup after loading the view.
    }
    

   
    func submitClicked(isClicked: Bool)
    {
        
//        self.getWorkflowRequestObject(DCWorkflowType.BusinessComplaint) { (workflowID, success, WorkflowRequest) in
//            
//            print("id : \(workflowID) , workflowRequest : \(WorkflowRequest)")
//            
//            if let workflowRequest = WorkflowRequest where success == true
//            {
//                
//                //Workflow Request object detected
//                
//            }
//            
//            
//        }

        
        if isClicked == true
        {
//            AlertView.hidden = false


            popupConfig.dismissTouchBackground = true
            popupConfig.cornerRadius = 2
            popupConfig.showAnimation = STZPopupShowAnimation.SlideInFromBottom
            popupConfig.dismissAnimation = STZPopupDismissAnimation.SlideOutToBottom
            popupConfig.dismissCompletion = nil
            
            let popupView = NSBundle.mainBundle().loadNibNamed("DCComplaintSubmitView", owner: nil, options: nil)[0] as! DCComplaintSubmitView
            popupView.delegate  = self
            
            popupView.frame = CGRect(x: 0, y: 0, width: 280, height: 141)
            self.presentPopupView(popupView, config: popupConfig)

            
        }
    }
    
    //MARK: Complaint Submit Delegate Methods
    
    func submitViewDidPressSubmit() {
        
        self.dismissPopupView()
        
    }

    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "detailContainer" {
            let complaints = segue.destinationViewController as? DCBusinessComplaintDetailsTableVC
            complaints?.delegate = self
        }else if segue.identifier == "InquieryContainer" {
            let inquiry = segue.destinationViewController as? DCBusinessInquiryDetailsTableVC
            inquiry?.delegate = self
        }
    }

}
