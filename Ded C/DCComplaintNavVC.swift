//
//  DCComplaintNavVC.swift
//  DedC
//
//  Created by Focaloid on 8/2/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCComplaintNavVC: UINavigationController
{

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.translucent = true
        self.navigationBar.backgroundColor = UIColor.clearColor()
        self.view.backgroundColor = UIColor.clearColor()
        self.navigationBar.tintColor = UIColor.whiteColor()
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -120), forBarMetrics: UIBarMetrics.Default)
        UIBarButtonItem.appearance().tintColor = UIColor.whiteColor()
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func pushViewController(viewController: UIViewController, animated: Bool)
    {
        self.visibleViewController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .Plain, target: nil, action: nil)
        super.pushViewController(viewController, animated: animated)
    }

}
