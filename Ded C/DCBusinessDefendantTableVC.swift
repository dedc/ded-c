//
//  DCBusinessDefendantTableVC.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 04/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCBusinessDefendantTableVC: UITableViewController
{

    var placeholderText = "Address".localized(LANGUAGE!)
    @IBOutlet var emirateLabel: UILabel!

    @IBOutlet weak var sectionSpace: UITableViewCell!
    @IBOutlet var attachmentsTableView: DCAttachmentTableView!
    @IBOutlet weak var tradeName: DCImagedTextField!
    @IBOutlet weak var tradeLicence: DCImagedTextField!
    @IBOutlet weak var website: DCImagedTextField!
    @IBOutlet weak var email: DCImagedTextField!
    @IBOutlet weak var Phone: DCImagedTextField!
    @IBOutlet weak var Mobile: DCImagedTextField!
    @IBOutlet weak var Address: UITextView!
    @IBOutlet weak var POBox: UITextField!
    @IBOutlet weak var Name: DCImagedTextField!
    @IBOutlet weak var JobTitle: DCImagedTextField!
    @IBOutlet weak var DefendantEmail: DCImagedTextField!
    @IBOutlet weak var DefendantMobile: DCImagedTextField!
    
    @IBOutlet var proxyTableView: DCAttachmentTableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if iPad {
            sectionSpace.backgroundColor = UIColor.clearColor()
        }
        self.attachmentsTableView.proxyCell = true
        self.proxyTableView.proxyCell = true

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if iPhone {
            switch indexPath.row {
            case 8:
                return 67
            case 10:
                return 50 + CGFloat(self.attachmentsTableView.attachmentsArray.count * 80)// Attachments
            case 12, 18:
                return 60
            case 11, 19:
                return 15
            case 17:
                return 50 + CGFloat(self.proxyTableView.attachmentsArray.count * 80) // Attachments
            default:
                return 44
            }
        }else {
            switch indexPath.row {
            case 5:
                return 83
            case 7:
                return 50 + CGFloat(self.attachmentsTableView.attachmentsArray.count * 80)// Attachments
            case 8:
                return 16
            case 12:
                return 50 + CGFloat(self.proxyTableView.attachmentsArray.count * 80) // Attachments
            case 13:
                return 60
            default:
                return 44
            }
        }
    }
    
    @IBAction func emiratesAction(sender: AnyObject)
    {
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var emiratesArray: [String] = []
        
        if LOOKUP_EMIRATES?.count != 0 {
            for i in 0..<LOOKUP_EMIRATES!.count {
                emiratesArray.append(LOOKUP_EMIRATES![i][fetchKey] as! String)
            }
        }
        self.showSelectionTableView("Emirates", options: emiratesArray)
        { (selectedString, index) in
            
            self.emirateLabel.text = selectedString
            
        }
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    // MARK: - TextField Delegates
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if textView.text == placeholderText
        {
            textView.textColor = UIColor.blackColor()
            textView.text = ""
        }
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == placeholderText || textView.text == ""
        {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = placeholderText
        }
    }
    
    // MARK: - Actions
    @IBAction func ComplaintDetails(sender: AnyObject)
    {
        let complaintsDetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.ComplaintdetailsVC)
        self.navigationController?.pushViewController(complaintsDetailsVC!, animated: true)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func tradeAction(sender: AnyObject)
    {
        if self.attachmentsTableView.attachmentsArray.count > 0
        {
            if self.attachmentsTableView.attachmentsArray[0].images.count >= 5
            {
                UIAlertView(title: "", message: "5 Images are supported", delegate: nil, cancelButtonTitle: "OK").show()
                return
            }
        }
        
        self.openImagesActionSheet
            { (image) in
                
                if let pickedImage = image
                {
                    if self.attachmentsTableView.attachmentsArray.count > 0
                    {
                        self.attachmentsTableView.attachmentsArray[0].images.append(pickedImage)
                    }
                    else
                    {
                        let attachment = DCAttachmentObject()
                        attachment.images.append(pickedImage)
                        self.attachmentsTableView.attachmentsArray = [attachment]
                    }
                    
                    self.attachmentsTableView.commentArray.append("")
                    self.attachmentsTableView.reloadData()
                    self.tableView.reloadData()
                }
                
        }

    }
   
    @IBAction func proxyAction(sender: AnyObject)
    {
        if self.proxyTableView.attachmentsArray.count > 0
        {
            if self.proxyTableView.attachmentsArray[0].images.count >= 5
            {
                UIAlertView(title: "", message: "5 Images are supported", delegate: nil, cancelButtonTitle: "OK").show()
                return
            }
        }
        
        self.openImagesActionSheet
            { (image) in
                
                if let pickedImage = image
                {
                    if self.proxyTableView.attachmentsArray.count > 0
                    {
                        self.proxyTableView.attachmentsArray[0].images.append(pickedImage)
                    }
                    else
                    {
                        let attachment = DCAttachmentObject()
                        attachment.images.append(pickedImage)
                        self.proxyTableView.attachmentsArray = [attachment]
                    }
                    
                    self.proxyTableView.commentArray.append("")
                    self.proxyTableView.reloadData()
                    self.tableView.reloadData()
                }
                
        }

    }
}
