//
//  SubProductsViewController.swift
//  Ded C
//
//  Created by iOS Dev 1 on 13/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

extension UIView {
    func rotate(toValue: CGFloat, duration: CFTimeInterval = 0.2, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.toValue = toValue
        rotateAnimation.duration = duration
        rotateAnimation.removedOnCompletion = false
        rotateAnimation.fillMode = kCAFillModeForwards
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate
        }
        self.layer.addAnimation(rotateAnimation, forKey: nil)
    }
}

class SubProductsViewController: UIViewController {
    struct Section {
        var name: String!
        var items: [String]!
        var collapsed: Bool!
        
        init(name: String, items: [String], collapsed: Bool = false) {
            self.name = name
            self.items = items
            self.collapsed = !collapsed
        }
    }
    @IBOutlet var SelectTbaleView: UITableView!
    var qtyArray = NSMutableArray()
    
    var sections = [Section]()

    @IBOutlet var cartButton: BadgeButton!
   
    var langDict = NSDictionary()
       var viewType = "" // Grocery or Cart Tab
    @IBOutlet var addToBskBtn: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
      
        self.langDict = LanguageClass.shared.languageDict.valueForKey("selectProducts") as! NSDictionary
        
        
        
        sections = [
            Section(name: "Mac", items: ["MacBook Air", "MacBook Pro", "iMac", "Mac Pro"]),
            Section(name: "iPad", items: ["iPad Pro", "iPad Air 2", "iPad mini 4", "Accessories"]),
            Section(name: "iPhone", items: ["iPhone 6s", "iPhone 6", "iPhone SE", "Accessories"])
        ]

        self.cartButton.badge.badgeValue = 1
       
        
        if viewType == "GroceryTab" {
            self.addToBskBtn.tag = 1
            if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
                self.title = self.langDict.valueForKey("selectProducts")as? String
                self.addToBskBtn.setTitle(self.langDict.valueForKey("comparePrices") as? String , forState: .Normal)
            }
            else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
                self.title = self.langDict.valueForKey("selectProducts")as? String
                self.addToBskBtn.setTitle(self.langDict.valueForKey("comparePrices") as? String , forState: .Normal)
            }

          //  NSLog("subGroceryTab")
        }
        else {
            self.addToBskBtn.tag = 2
            if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            self.title = self.langDict.valueForKey("selectProducts")as? String
            print(self.langDict.valueForKey("addToBasket") as? String)
            self.addToBskBtn.setTitle(self.langDict.valueForKey("addToBasket") as? String , forState: .Normal)
        }
        else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
            self.title = self.langDict.valueForKey("selectProducts")as? String
            self.addToBskBtn.setTitle(self.langDict.valueForKey("addToBasket") as? String , forState: .Normal)
            }
        //  NSLog("subcartTab")
        }
        
       SelectTbaleView.tableFooterView = UIView(frame: CGRect.zero)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addToBskBtnClk(sender: AnyObject) {
        if addToBskBtn.tag == 1 {
            let priceCompareVC = self.storyboard?.instantiateViewControllerWithIdentifier("PriceCompareViewController") as! PriceCompareViewController
            self.navigationController?.pushViewController(priceCompareVC, animated: true)
        }
        else if addToBskBtn.tag == 2{
           
        print("Added to Basket")
        }
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 65
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (sections[section].collapsed!) ? 0 : sections[section].items.count
    }
    
     func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header : SubProductsHeader!
        if DC_DEFAULTS.valueForKey("language") as? String == "ar"{
            header = tableView.dequeueReusableCellWithIdentifier("ArabicHeader") as! SubProductsHeader
            
            header.sectionButton.tag = section
            
            if (section == 0){
                header.TitleLabel.text = "Beaf - Fresh"
            }
            if(section == 1){
                header.TitleLabel.text = "Fresh Lamb"
                
            }
            if(section == 2){
                header.TitleLabel.text = "Frozen Meat"
                
            }
            
            if (sections[section].collapsed! == true)
            {
                header.toggleImageView.rotate(0.0)
                header.toggleImageView.image = UIImage(named: "ArrowDown.png")
                
            }
            else{
                header.toggleImageView.rotate(CGFloat(M_PI))
                
                header.contentView.backgroundColor = UIColor(red: 0.18, green: 0.28, blue: 0.33, alpha: 1.0)
                
                header.headView.backgroundColor = UIColor(red: 0.18, green: 0.28, blue: 0.33, alpha: 1.0)
                
                header.countLabel.backgroundColor = UIColor.whiteColor()
                header.TitleLabel.textColor = UIColor.whiteColor()
                header.countLabel.textColor = UIColor.darkGrayColor()
                header.toggleImageView.image = UIImage(named: "toggle.png")
                
                
            }
        }
        else{
            header = tableView.dequeueReusableCellWithIdentifier("EnglishHeader") as! SubProductsHeader
            
            header.sectionButton.tag = section
            
            if (section == 0){
                header.TitleLabel.text = "Beaf - Fresh"
            }
            if(section == 1){
                header.TitleLabel.text = "Fresh Lamb"
                
            }
            if(section == 2){
                header.TitleLabel.text = "Frozen Meat"
                
            }
            
            if (sections[section].collapsed! == true)
            {
                header.toggleImageView.rotate(0.0)
                header.toggleImageView.image = UIImage(named: "ArrowDown.png")
                
            }
            else{
                header.toggleImageView.rotate(CGFloat(M_PI))
                
                header.contentView.backgroundColor = UIColor(red: 0.18, green: 0.28, blue: 0.33, alpha: 1.0)
                
                header.headView.backgroundColor = UIColor(red: 0.18, green: 0.28, blue: 0.33, alpha: 1.0)
                
                header.countLabel.backgroundColor = UIColor.whiteColor()
                header.TitleLabel.textColor = UIColor.whiteColor()
                header.countLabel.textColor = UIColor.darkGrayColor()
                header.toggleImageView.image = UIImage(named: "toggle.png")
                
                
            }
            

        }
        
                return header.contentView
    }
    
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : SubProductsCell!
        if DC_DEFAULTS.valueForKey("language") as? String == "ar"{
             cell = tableView.dequeueReusableCellWithIdentifier("ArabicCell") as! SubProductsCell
            cell.countLabel.layer.borderColor = UIColor(red:0.00, green:0.40 ,blue:0.40 , alpha:1.00).CGColor
        }
        else{
               cell = tableView.dequeueReusableCellWithIdentifier("EnglishCell") as! SubProductsCell
            cell.countLabel.layer.borderColor = UIColor(red:0.00, green:0.40 ,blue:0.40 , alpha:1.00).CGColor
        }
       
        //cell.textLabel?.text = sections[indexPath.section].items[indexPath.row]
        
        return cell
    }
    
    
   
    //
    // MARK: - Event Handlers
    //
    @IBAction func tapgesture(sender: UIButton) {
        let section = sender.tag
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed
        
        
        
        // Reload section
        self.SelectTbaleView.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
    }
    
   
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
    }
    
}



