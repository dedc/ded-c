//
//  DCNetwork.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 03/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCNetwork: NSObject
{
    
    static let sharedNetwork = DCNetwork()
    
    func postJson(url: String, method: String, params: AnyObject, is_Dictionary: Bool, postCompleted : (succeeded: Bool, json: AnyObject) -> ()) {
        if IS_REACHABLE == true {
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            let session = NSURLSession.sharedSession()
            
            request.HTTPMethod = method
            do {
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
                
                let body = NSString(data: request.HTTPBody!, encoding: NSUTF8StringEncoding)
                print("body : \(body)")
                
            } catch let error as NSError {
                NSErrorPointer().memory = error
                request.HTTPBody = nil
            }
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print(params, terminator: "")
            let task = session.dataTaskWithRequest(request) {(data, response, error) in
                dispatch_async(dispatch_get_main_queue(), {
                    if response != nil {
                        print(data)
                        var jsonResult: AnyObject!
                        let string = NSString(data: data!, encoding:NSUTF8StringEncoding)
                        print("response string : \(string)")
                        
                        if is_Dictionary == true {
                            do {
                                jsonResult = (try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                            } catch {
                                postCompleted(succeeded: false, json: "")
                            }
                            
                        }else {
                            do {
                                jsonResult = (try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSArray
                            }catch {
                                postCompleted(succeeded: false, json: "")
                            }
                            
                        }
//                        postCompleted(succeeded: true, json: jsonResult)
                    }else {
                        postCompleted(succeeded: false, json: "")
                    }
                })
                
            }
            task.resume()
        }else {
            postCompleted(succeeded: false, json: "")
            DC_DELEGATE.showAlert("Message", message: "No Internet Connection", buttonTitle: "OK")
        }
    }
    
    
    func postString(url: String, method: String, params: String, is_Dictionary: Bool, postCompleted : (succeeded: Bool, json: AnyObject) -> ()) {
        if IS_REACHABLE == true {
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            let session = NSURLSession.sharedSession()
            
            request.HTTPMethod = method
            request.HTTPBody = params.dataUsingEncoding(NSUTF8StringEncoding)
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//            request.addValue("application/json", forHTTPHeaderField: "Accept")
//            print(params, terminator: "")
            let task = session.dataTaskWithRequest(request) {(data, response, error) in
                
                print(data)
                
                dispatch_async(dispatch_get_main_queue(), {
                    if response != nil {
                        var jsonResult: AnyObject!
                        if is_Dictionary == true {
                            do {
                                jsonResult = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                            }catch {
                                postCompleted(succeeded: false, json: "")
                            }
                            
                        }else {
                            jsonResult = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSArray
                        }
                        postCompleted(succeeded: true, json: jsonResult)
                    }else {
                        postCompleted(succeeded: false, json: "")
                    }
                })
                
            }
            task.resume()
        }else {
            postCompleted(succeeded: false, json: "")
            DC_DELEGATE.showAlert("Message", message: "No Internet Connection", buttonTitle: "OK")
        }
    }

    
    
    func getJson(url: String, method: String,params: [String:String], postCompleted : (succeeded: Bool, json: AnyObject) -> ())
    {
        if IS_REACHABLE == true
        {
         
            var urlString = url

            
            var i = 0
            
            for (key,value) in params
            {
                if i == 0
                {
                    urlString = urlString + "?"
                }
                else
                {
                    urlString = urlString + "&"

                }
                
                urlString = urlString + key + "=" + value
                
                i += 1
                
            }
            
            let request : NSMutableURLRequest = NSMutableURLRequest()
            request.URL = NSURL(string: urlString)
            request.HTTPMethod = method
            
            print(url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                
                dispatch_async(dispatch_get_main_queue(), {
                    if response != nil {
                        
                        var jsonResult = NSDictionary()
                        jsonResult = (try! NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                        postCompleted(succeeded: true, json: jsonResult)
                        print(jsonResult)
                    }else {
                        postCompleted(succeeded: false, json: [:])
                    }
                })
            })
        }else {
            postCompleted(succeeded: false, json: "")
            DC_DELEGATE.showAlert("Message", message: "No Internet Connection", buttonTitle: "OK")
        }
    }
}
