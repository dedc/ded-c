//
//  DCComplaintTrackTable.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 04/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCComplaintTrackTable: UITableViewController
{

    var searchResults: NSDictionary = [:]
    var status: String = ""
    var complaintDate: String = ""
    var complaintTime: String = ""
    var employeename: String = ""
    var email: String = ""
    var phone: String = ""
    var complaintID: String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        if let WorkflowRequest = searchResults["WorkflowRequest"] {
            if let complaintid = WorkflowRequest["RequestCode"] {
                complaintID = complaintid as! String
            }
        }
        if let requestDetails = searchResults["RequestDetails"] {
            if let participantDetails = requestDetails["ParticipantDetails"] {
                if let TradeLicenseStatus = participantDetails!["TradeLicenseStatus"] as? String {
                    status = TradeLicenseStatus
                    
                }
                if let CreatedDate = participantDetails!["CreatedDate"] as? String {
                    let formated = CreatedDate.stringByReplacingOccurrencesOfString("T", withString: " ")
                    complaintDate = DC_DELEGATE.Custom_StringToDate(formated, fromType: "yyyy-dd-MM HH:mm:ss", toType: "dd MMM yyyy")
                    complaintTime = DC_DELEGATE.Custom_StringToDate(formated, fromType: "yyyy-dd-MM HH:mm:ss", toType: "hh:mm a")
                }
            }
            if let processDetails = requestDetails["ProcessDetails"] {
                if let EmployeeName = processDetails!["EmployeeName"] as? String {
                    employeename = EmployeeName
                }
                if let phoneNum = processDetails!["Phone"] as? String {
                    phone = phoneNum
                }
                if let emailId = processDetails!["Email"] as? String {
                    email = emailId 
                }
            }
        }
        
        print(complaintID)
        print(status)
        print(complaintDate)
        print(employeename)
        print(phone)
        print(email)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchResults.count != 0 {
            return 1
        }else {
            return 0
        }
        
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("complaintCell", forIndexPath: indexPath) as! DCComplaintTrackCell

        cell.backgroundColor = UIColor.clearColor()
        cell.complaintStatus.text = status
        cell.headdingID.text = complaintID
        cell.complaintDate.text = complaintDate
        cell.complaintTime.text = complaintTime
        cell.employeeName.text = employeename
        cell.employeeMailId.text = email
        cell.employeePhoneNum.text = phone
        

        return cell
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class DCComplaintTrackCell: UITableViewCell {
    
    @IBOutlet weak var employeePhoneNum: UILabel!
    @IBOutlet weak var employeeMailId: UILabel!
    @IBOutlet weak var employeeName: UILabel!
    @IBOutlet weak var complaintTime: UILabel!
    @IBOutlet weak var complaintDate: UILabel!
    @IBOutlet weak var complaintStatus: UILabel!
    @IBOutlet weak var headdingID: UILabel!
}
