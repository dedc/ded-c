//
//  BadgeButton.swift
//  Lamcon
//
//  Created by Abdur Rehman on 20/06/16.
//  Copyright © 2016 hashinclude.io. All rights reserved.
//

import UIKit
import Sheriff

class BadgeButton: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    let badge = GIBadgeView()
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        badge.frame = CGRectMake(0, 0, 10, 10)
        badge.backgroundColor = UIColor.appThemeYellowColor()
        badge.topOffset = 5
        badge.rightOffset = 25
        badge.textColor = UIColor.blackColor()
        badge.userInteractionEnabled = false
        self.addSubview(badge)
    }

}
