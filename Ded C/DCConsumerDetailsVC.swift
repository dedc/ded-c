//
//  SDConsumerDetailsVC.swift
//  DedC
//
//  Created by Focaloid on 8/3/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

enum DCConsumerComplaintType : Int
{
    case Complaint = 0
    case Inquiry = 1
    case Comment = 2
}


class DCConsumerDetailsVC: UITableViewController
{
    
    @IBOutlet weak var HeaddingLabel: UILabel!
    @IBOutlet var touristButton: UIButton!
    @IBOutlet var residentButton: UIButton!
    
    @IBOutlet var detailsButton: UIButton!
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet weak var mobNumCode: UITextField!
    @IBOutlet weak var mobNumber: UITextField!
    @IBOutlet weak var secondCode: UITextField!
    @IBOutlet weak var secondNumber: UITextField!
    @IBOutlet weak var email: DCImagedTextField!
    
    @IBOutlet var touristTick: UIImageView!
    @IBOutlet var residentTick: UIImageView!
    
    var complaintType : DCConsumerComplaintType = .Complaint
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        if iPad {
            if self.complaintType == .Complaint {
                HeaddingLabel.text = "Complaint Details".localized(LANGUAGE!)
            }else {
                HeaddingLabel.text = "Consumer Details".localized(LANGUAGE!)
            }
        }
    }

    @IBAction func detailsAction(sender: AnyObject)
    {
        if self.complaintType == .Complaint
        {
            let defendantDetailsVC = DCStoryBoards.sharedStoryBoard.secondaryStoryBoard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.DefendantdetailsVC)
            self.navigationController?.pushViewController(defendantDetailsVC, animated: true)
        }
        
        if self.complaintType == .Inquiry
        {
            let defendantDetailsVC = DCStoryBoards.sharedStoryBoard.secondaryStoryBoard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.ConsumerInquiryDetailsVC)
            self.navigationController?.pushViewController(defendantDetailsVC, animated: true)
        }
       
        if self.complaintType == .Comment
        {
            let defendantDetailsVC = DCStoryBoards.sharedStoryBoard.secondaryStoryBoard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.ConsumerCommentDetailsVC)
            self.navigationController?.pushViewController(defendantDetailsVC, animated: true)
        }

        
    }
    @IBAction func residentAction(sender: AnyObject)
    {
        residentButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
            touristButton.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        
        residentTick.hidden = false
        touristTick.hidden = true
        
        
    }
    
    @IBAction func touristAction(sender: AnyObject)
    {
        touristButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        residentButton.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        
        residentTick.hidden = true
        touristTick.hidden = false
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if iPhone {
            switch indexPath.row {
            case 0:
                return 50
            case 2:
                return 95
            case 3:
                return 75
            case 6, 8:
                return 55
            case 7:
                return 65
            default:
                return 44
            }
        }else {
            switch indexPath.row {
            case 2:
                return 92
            case 4:
                return 134
            case 5:
                return 65
            default:
                return 44
            }
        }
    }
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
}
