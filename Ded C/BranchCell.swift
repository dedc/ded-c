//
//  BranchCell.swift
//  Ded C
//
//  Created by iOS Dev 1 on 12/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class BranchCell: UITableViewCell {

    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var statusBtn: UIButton!
    @IBOutlet var labelTotal: UILabel!
    @IBOutlet var labelPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
