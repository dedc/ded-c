//
//  EditCell.swift
//  Ded C
//
//  Created by iOS Dev 1 on 12/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class EditCell: UITableViewCell {

    @IBOutlet var labelSubTitle: UILabel!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelQty: UILabel!
    @IBOutlet var plusBtn: UIButton!
    @IBOutlet var minusBtn: UIButton!
    
    var indexValue = NSInteger()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        labelQty.layer.borderWidth = 1.5
        labelQty.layer.backgroundColor = UIColor(red:19.0/255.0, green:66.0/255.0, blue:64.0/255.0, alpha:1).CGColor
        labelQty.layer.borderColor = UIColor(red:0.00, green:0.40, blue:0.40 ,alpha:1.00).CGColor
        labelQty.layer.cornerRadius = 5.0
        labelQty.clipsToBounds = true
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    @IBAction func plusBtnClk(sender: UIButton) {
        let plusCount = Int(self.labelQty.text!)
        let value:NSString = String(plusCount!+1)
        if value == "26"{
        
        }
        else{
           labelQty.text = value as String
           let dict = NSMutableDictionary()
           let key:NSString = String(format: "%d",indexValue)
           dict.setObject(labelQty.text!, forKey: key)
           NSNotificationCenter.defaultCenter().postNotificationName("update", object: dict)
        }
    }
    
    @IBAction func minusBtnClk(sender: UIButton) {
        let minusCount = Int(self.labelQty.text!)
        let value:NSString = String(minusCount!-1)
        if value == "0"{
        }
        else{
          labelQty.text = value as String
            let dict = NSMutableDictionary()
            let key:NSString = String(format: "%d",indexValue)
            dict.setObject(labelQty.text!, forKey: key)
            NSNotificationCenter.defaultCenter().postNotificationName("update", object: dict)
         }
    }
}
