//
//  DCConsumerInquiryTableVC.swift
//  Ded C
//
//  Created by Focaloid on 8/8/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCConsumerInquiryTableVC: UITableViewController
{

    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var acceptTermsImageView: UIImageView!
    var placeholderText = "Inquiry Details".localized(LANGUAGE!)
    var acceptedterms : Bool = false
    @IBOutlet weak var termsCell: UITableViewCell!
    @IBOutlet weak var inquieryDetails: UITextView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if iPad {
            termsCell.backgroundColor = UIColor.clearColor()
        }
        
        acceptedterms = false
        acceptTermsImageView.image = UIImage(named: "uncheck")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func TermsNConditions(sender: AnyObject)
    {
        let TermsNConditionsVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.TermsNConditionsVC)
        self.presentViewController(TermsNConditionsVC, animated: true, completion: nil)
    }
    
    @IBAction func SubmitConsumerInquiry(sender: AnyObject) {
        let emptyArray : NSArray = [] as! NSArray
        
        let RequestDetails = ["RequestDetails":
            ["ParticipantDetails":
                [
                    "ID": "",
                    "MobileNumber": ConsumerVariables.complaint_MobileCode + ConsumerVariables.complaint_MobileNumber,
                    "NationalityID":125,
                    "Email": ConsumerVariables.complaint_Email,
                    "Type":"CONSUMER",
                    "NameEN": ConsumerVariables.complaint_FullName,
                    "NameAR": ConsumerVariables.complaint_FullName,
                    "SecondMobileNumber": ConsumerVariables.complaint_SecondCode + ConsumerVariables.complaint_SecondNumber,
                    "PhoneNumber": ConsumerVariables.defendand_phone,
                    "EmirateID":1,
                    "Address": ConsumerVariables.defendand_address,
                    "PO_Box": ConsumerVariables.defendand_poBox,
                    "CallingSystem":0,
                    "PreferedLanguage":0,
                    "ResidencyID":1,
                    "AccountID":"",
                    "CreatedDate":"2016-09-22T10:30:00",
                    "ModifiedDate":"2016-09-22T10:30:00",
                    "CreatedBy":"N/A",
                    "ModifiedBy":"N/A",
                    "AdditionalInfo":""
                ],
                "CommentSource":"2",
                "CommentType":"2",
                "CallerComment":"Test Submit",
                "Role":"CM_SUBMIT",
                "Type": 3,
                "Subject": "Subject",
                "ComplaintDetails": "Test",
                "DateOfPurchase": "2016-09-22T10:30:00",
                "ValueOfPurchase": "",
                "Currency": "28",
                "DesireRefundAmount": "2342",
                "PaymentMethod": "Visa",
                "TradeLicenseNumber": "1"
            ],
                              "Comments": emptyArray,
                              "Attachments": emptyArray,
                              "WorkflowRequest": [
                                "Workflow": ["Id": WORKFLOW_ID!],
                                "Action": WORKFLOW_ACTION
            ]
        ]
        print(RequestDetails)
        
        
        DC_NETWORK.postJson(DCAPIConstants.BaseURL + DCAPIConstants.SubmitComplaint, method: "POST", params: RequestDetails, is_Dictionary: true) { (succeeded, json) in
            dispatch_async(dispatch_get_main_queue(), {
                print(json)
            })
        }
    }
    @IBAction func categoryAction(sender: AnyObject)
    {
        
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var categoryArray: [String] = []
        
        if LOOKUP_COMMERCIAL_SECTOR?.count != 0 {
            for i in 0..<LOOKUP_COMMERCIAL_SECTOR!.count {
                categoryArray.append(LOOKUP_COMMERCIAL_SECTOR![i][fetchKey] as! String)
            }
        }
        
        self.showSelectionTableView("Category", options: categoryArray)
        { (selectedString, index) in
            
            self.categoryLabel.text = selectedString
            
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if iPad {
            switch indexPath.row {
            case 2:
                return 78
            case 3:
                return 60
            case 5:
                return 47
            default:
                return 44
            }
        }else {
            switch indexPath.row {
            case 2:
                return 78
            case 3:
                return 60
            default:
                return 44
            }
        }
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - TextView Delegates
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if textView.text == placeholderText
        {
            textView.textColor = UIColor.blackColor()
            textView.text = ""
        }
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == placeholderText || textView.text == ""
        {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = placeholderText
        }
    }
    @IBAction func acceptTermsAction(sender: AnyObject)
    {
        
//        acceptedterms = !acceptedterms
        
        if acceptedterms == true
        {
            acceptTermsImageView.image = UIImage(named: "uncheck")
            acceptedterms = false
        }
        else
        {
            acceptTermsImageView.image = UIImage(named: "Check")
            acceptedterms = true
        }
        
    }
    
    
    

}
