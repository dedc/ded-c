//
//  PlaceOrderViewController.swift
//  Ded C
//
//  Created by iOS Dev 1 on 13/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class PlaceOrderViewController: UIViewController {

    @IBOutlet var mainHeaderView: UIView!
    @IBOutlet var englishHeaderView: UIView!
    @IBOutlet var arabicHeaderView: UIView!
    
    @IBOutlet var orderBtn: UIButton!
    @IBOutlet var budgetLabel: UILabel!
    @IBOutlet var expenseLabel: UILabel!
    @IBOutlet var yourbudget: UILabel!
    
    var type = ""  // pickup or delivery
    var cellIdentifier = ""
    var deliveryDict = NSDictionary()
    var pickupDict = NSDictionary()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.deliveryDict = LanguageClass.shared.languageDict.valueForKey("yourOrderDelivery") as! NSDictionary
        self.pickupDict = LanguageClass.shared.languageDict.valueForKey("yourOrderPickup") as! NSDictionary
    
        if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            self.title = self.deliveryDict.valueForKey("yourOrder") as? String
            self.budgetLabel.text = self.deliveryDict.valueForKey("budget") as? String
            self.expenseLabel.text = self.deliveryDict.valueForKey("expense") as? String
            self.yourbudget.text = self.deliveryDict.valueForKey("yourBudget") as? String
            self.yourbudget.textAlignment = .Right
            self.arabicHeaderView.hidden = false
            self.englishHeaderView.hidden = true
             self.mainHeaderView.frame = CGRectMake(self.mainHeaderView.frame.origin.x, self.mainHeaderView.frame.origin.y, self.mainHeaderView.frame.size.width , 133)
        }
        else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
            self.budgetLabel.text = self.deliveryDict.valueForKey("budget") as? String
            self.expenseLabel.text = self.deliveryDict.valueForKey("expense") as? String
            self.yourbudget.text = self.deliveryDict.valueForKey("yourBudget") as? String
            self.title = self.deliveryDict.valueForKey("yourOrder") as? String
            self.arabicHeaderView.hidden = true
            self.englishHeaderView.hidden = false
            self.mainHeaderView.frame = CGRectMake(self.mainHeaderView.frame.origin.x, self.mainHeaderView.frame.origin.y, self.mainHeaderView.frame.size.width , 133)
            
        }


        
        type = NSUserDefaults.standardUserDefaults().stringForKey("DeliveryType")!
        
        if type == "PickUp"{
            self.orderBtn.tag = 1
            
           cellIdentifier = "CellPickUpAddress"
            
            if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
                orderBtn.setTitle(self.pickupDict.valueForKey("getMyOrderReady") as? String, forState: UIControlState.Normal)
                cellIdentifier = "ArabicCellPickUpAddress"

            }
            else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
                orderBtn.setTitle(self.pickupDict.valueForKey("getMyOrderReady") as? String, forState: UIControlState.Normal)
                cellIdentifier = "EnglishCellPickUpAddress"
            }
        }
        
        else if type == "Delivery"{
            self.orderBtn.tag = 2
            
            if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
                orderBtn.setTitle(self.deliveryDict.valueForKey("orderNow") as? String, forState: UIControlState.Normal)
                cellIdentifier = "ArabicCellDeliveryAddress"
                
            }
            else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
                orderBtn.setTitle(self.deliveryDict.valueForKey("orderNow") as? String, forState: UIControlState.Normal)
                cellIdentifier = "EnglishCellDeliveryAddress"
            }

            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if type == "PickUp"{
              return 1
        }
        else {
             return 3
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
        if type == "PickUp"{
            let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! OrderCell
            cell.selectionStyle = .None
            cell.userInteractionEnabled = false
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! OrderCell
            cell.selectionStyle = .None
            cell.userInteractionEnabled = true
            cell.statusBtn.hidden = true
            return cell
        }

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 45.0
    }

    ///////////////////////// TableView Header Section
    
    func tableView(tableView: UITableView,viewForHeaderInSection section: Int) -> UIView?{
        let cell : UIView!
        if DC_DEFAULTS.valueForKey("language") as? String == "ar" {
            cell = tableView.dequeueReusableCellWithIdentifier("ArabicHeaderAddress")! as UIView
            
            let titleLabel:UILabel = cell.viewWithTag(300) as! UILabel
            let imgView:UIImageView = cell.viewWithTag(400) as! UIImageView
            if  DC_DEFAULTS.valueForKey("") as? String == "ar" {
                if type == "PickUp"{
                    titleLabel.text = self.pickupDict.valueForKey("pickUp") as? String
                    imgView.hidden = true
                }
                else {
                    titleLabel.text = self.deliveryDict.valueForKey("shippingAddress") as? String
                    imgView.hidden = false
                }
            }
            else{
                if type == "PickUp"{
                    titleLabel.text = self.pickupDict.valueForKey("pickUp") as? String
                    imgView.hidden = true
                }
                else {
                    titleLabel.text = self.deliveryDict.valueForKey("shippingAddress") as? String
                    imgView.hidden = false
                }
                
            }

        }
        else{
            cell = tableView.dequeueReusableCellWithIdentifier("EnglishHeaderAddress")! as UIView
            
            let titleLabel:UILabel = cell.viewWithTag(300) as! UILabel
            let imgView:UIImageView = cell.viewWithTag(400) as! UIImageView
            if  DC_DEFAULTS.valueForKey("") as? String == "ar" {
                if type == "PickUp"{
                    titleLabel.text = self.pickupDict.valueForKey("pickUp") as? String
                    imgView.hidden = true
                }
                else {
                    titleLabel.text = self.deliveryDict.valueForKey("shippingAddress") as? String
                    imgView.hidden = false
                }
            }
            else{
                if type == "PickUp"{
                    titleLabel.text = self.pickupDict.valueForKey("pickUp") as? String
                    imgView.hidden = true
                }
                else {
                    titleLabel.text = self.deliveryDict.valueForKey("shippingAddress") as? String
                    imgView.hidden = false
                }
                
            }

        }
        

        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 50
    }

    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! OrderCell
        cell.statusBtn.hidden = false
        cell.labelAddress.font = UIFont.boldSystemFontOfSize(14)
}

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath){
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! OrderCell
        cell.statusBtn.hidden = true
        cell.labelAddress.font = UIFont.systemFontOfSize(14)
            
    }

    
    @IBAction func orderBtnClk(sender: UIButton) {
//
//        NSUserDefaults.standardUserDefaults().removeObjectForKey("DeliveryType")
//        NSUserDefaults.standardUserDefaults().synchronize()
//
        if self.orderBtn.tag == 2 {
           // Delivery
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        
        else if self.orderBtn.tag == 1{  // Pickup
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        
    }
   
}
