//
//  DCAttachmentTableView.swift
//  Ded C
//
//  Created by Focaloid on 8/6/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCAttachmentTableView: UITableView,UITableViewDelegate,UITableViewDataSource
{
    
    var attachmentsArray : [DCAttachmentObject] = []
    var commentArray : [String] = []
    var type : String = ""
    var proxyCell: Bool = false
    
    weak var attachmentCellDelegate : DCMultipleAttachmentCellDelegate?

    var placeHolderText = "Add Comment to attachment".localized(LANGUAGE!)
    
    override func layoutSubviews()
    {
        
        super.layoutSubviews()
        self.separatorStyle = .None
        self.allowsSelection = false
        self.bounces = false
        self.delegate = self
        self.dataSource = self
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.endEditing(true)
        return true
    }
    
    //MARK: TableViewDatasource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return attachmentsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var nibIdentifier: String!
        
        if proxyCell == false
        {
            
            nibIdentifier = "DCMultipleAttachmentCell"
            
            let nib = NSBundle.mainBundle().loadNibNamed(nibIdentifier, owner: nil, options: nil)
            var cell : DCMultipleAttachmentCell
            
            
            if LANGUAGE == "en"
            {
                cell = nib[0] as! DCMultipleAttachmentCell
            }
            else {
                cell = nib[1] as! DCMultipleAttachmentCell
            }
            
            
            cell.images = attachmentsArray[indexPath.row].images
            cell.commentTextView.text = attachmentsArray[indexPath.row].comment
            cell.attachmentDelegate = self.attachmentCellDelegate
            cell.imagesCollection.reloadData()
            cell.comment = attachmentsArray[indexPath.row].comment
            cell.attachmentType = self.type

            return cell


        }

        
        

        
        
      //        if commentArray.count > indexPath.row
//        {
//            if commentArray[indexPath.row] != ""
//            {
//                cell.commentTextView.text = commentArray[indexPath.row]
//                return cell
//            }
//        }
//      
//        cell.commentTextView.text = placeHolderText
        
        
        nibIdentifier = "DCTradeAttachmentCell"
        
        let nib = NSBundle.mainBundle().loadNibNamed(nibIdentifier, owner: nil, options: nil)
        
        let cell = nib[0] as! DCTradeAttachmentCell
        
        cell.images = attachmentsArray[indexPath.row].images
        cell.imagesCollection.reloadData()
        cell.attachmentType = self.type
        
        return cell
        

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if proxyCell == true
        {
            return 80
        }
        else
        {
            return 140
        }
        
    }

}
