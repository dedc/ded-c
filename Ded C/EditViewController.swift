//
//  EditViewController.swift
//  Ded C
//
//  Created by iOS Dev 1 on 12/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {
    
    @IBOutlet var tableViewEdit: UITableView!
    
    var fromWhichView = "" // Grocery or Cart Tab
    
    @IBOutlet var addProductBtn: UIButton!
    @IBOutlet var clearProductBtn: UIButton!
    var langDict = NSDictionary()
    var editDict = NSDictionary()
    var qtyArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if fromWhichView == "GroceryTab" {
           NSLog("GroceryTab")
            self.clearProductBtn.tag = 0
            self.addProductBtn.tag = 0
            

            self.langDict = LanguageClass.shared.languageDict.valueForKey("basket") as! NSDictionary
            
            if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
                self.title = self.langDict.valueForKey("basket")as? String
                self.addProductBtn.setTitle(self.langDict.valueForKey("placeOrder")as? String ,forState:.Normal)
                self.clearProductBtn.setTitle(self.langDict.valueForKey("addProducts")as? String ,forState:.Normal)
                
            }
            else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
                self.title = self.langDict.valueForKey("basket")as? String
                    self.addProductBtn.setTitle(self.langDict.valueForKey("placeOrder")as? String ,forState:.Normal)
                    self.clearProductBtn.setTitle(self.langDict.valueForKey("addProducts")as? String ,forState:.Normal)
                }
            
            }
            
            
        else if fromWhichView == "CartTab"{
            self.addProductBtn.tag = 1
            self.clearProductBtn.tag = 1

            self.editDict = LanguageClass.shared.languageDict.valueForKey("editCart") as! NSDictionary
            print(self.editDict)
            if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
                 self.title = self.editDict.valueForKey("edit") as? String
                 self.addProductBtn.setTitle(self.editDict.valueForKey("addProducts")as? String ,forState:.Normal)
                self.clearProductBtn.setTitle(self.editDict.valueForKey("clearCart")as? String ,forState:.Normal)
            }
            else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
                 self.title = self.editDict.valueForKey("edit") as? String
                print(self.editDict.valueForKey("addProducts")as? String)
                self.addProductBtn.setTitle(self.editDict.valueForKey("addProducts")as? String ,forState:.Normal)
                self.clearProductBtn.setTitle(self.editDict.valueForKey("clearCart")as? String ,forState:.Normal)
               
            }
        }
        
        
    qtyArray = ["2","5","12","1","3","6","7","9","15","11"]
        
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(EditViewController.updateQtyArray(_:)), name:"update", object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let  cell : EditCell!
        if DC_DEFAULTS.valueForKey("language") as? String == "ar" {
            cell = tableView.dequeueReusableCellWithIdentifier("ArabicCellEdit", forIndexPath: indexPath) as! EditCell
            cell.labelQty.text = qtyArray.objectAtIndex(indexPath.row) as? String
            cell.indexValue = indexPath.row

        }
        else{
            cell = tableView.dequeueReusableCellWithIdentifier("EnglishCellEdit", forIndexPath: indexPath) as! EditCell
            cell.labelQty.text = qtyArray.objectAtIndex(indexPath.row) as? String
            cell.indexValue = indexPath.row

        }
                return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 65
    }

    func updateQtyArray(notification:NSNotification){
        let dict = notification.object as! NSMutableDictionary
        let key:NSArray = dict.allKeys
        let value = dict.valueForKey(key.objectAtIndex(0)as! String)
        let index = (key.objectAtIndex(0) as! NSString).integerValue
        qtyArray.replaceObjectAtIndex(index, withObject: value as! String)
    }
    
    
    @IBAction func addProductsBtnClk(sender: AnyObject) {
        if addProductBtn.tag == 0{
            let selectProductsVC = self.storyboard?.instantiateViewControllerWithIdentifier("SelectProductsViewController") as! SelectProductsViewController
            self.navigationController?.pushViewController(selectProductsVC, animated: true)
        }
        else if addProductBtn.tag == 1{
            if NSUserDefaults.standardUserDefaults().stringForKey("DeliveryType") != nil{
                let placeOrderVC = self.storyboard?.instantiateViewControllerWithIdentifier("PlaceOrderViewController") as! PlaceOrderViewController
                self.navigationController?.pushViewController(placeOrderVC, animated: true)
            }
            else{
                let storyboard = UIStoryboard(name: "Groceries", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("PopViewController") as! PopViewController
                vc.setPopinTransitionStyle(.SpringySlide)
                vc.setPopinAlignment(.Centered)
                //  vc.setPreferedPopinContentSize(CGSizeMake(self.view.frame.size.width-30,self.view.frame.size.width/2))
                vc.setPreferedPopinContentSize(CGSizeMake(self.view.frame.size.width-30,165))
                vc.setPopinOptions(.DisableParallaxEffect)
                self.presentPopinController(vc, animated:true, completion: nil)
            }

            
            }
        }
    
    @IBAction func clearCartBtnClk(sender: AnyObject) {
        if self.clearProductBtn.tag == 0{
            NSLog("Clear Cart in cart Tab")
        }
        else if self.clearProductBtn.tag == 1{
            let selectProductsVC = self.storyboard?.instantiateViewControllerWithIdentifier("SelectProductsViewController") as! SelectProductsViewController
            self.navigationController?.pushViewController(selectProductsVC, animated: true)
        }
        
    }
    
}
