//
//  DCBusinessInquiryDetailsTableVC.swift
//  Ded C
//
//  Created by Focaloid on 8/7/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCBusinessInquiryDetailsTableVC: UITableViewController
{

    // MARK: - Variables
    var delegate: showAlertDelegate?
    @IBOutlet weak var termsCell: UITableViewCell!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var acceptTermsImageView: UIImageView!
    
    var acceptedterms : Bool = false
    
    @IBOutlet weak var InquieryDetails: UITextView!
    
    // MARK: - View Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        if iPad {
            termsCell.backgroundColor = UIColor.clearColor()
        }
        
        acceptedterms = false
        acceptTermsImageView.image = UIImage(named: "uncheck")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    @IBAction func SubmitBusinessRequest(sender: AnyObject) {
        
        
        let emptyArray : NSArray = [] as! NSArray
        
        let RequestDetails = ["RequestDetails":
            ["ParticipantDetails":
                [
                    "TradeOrigin": BusinessVariables.complaint_tradeOrigin,
                    "TradeLicenseNumber": BusinessVariables.complaint_tradeLicence,
                    "TradeLicenseURL": BusinessVariables.complaint_tradeLicence,
                    "TradeLicenseType": 1,
                    "TradeLicenseStatus": 3,
                    "RepresentativeName": BusinessVariables.complaint_repName,
                    "RepresentativeJobTitle": BusinessVariables.complaint_repJobTitle,
                    "RepresentativeEmail": BusinessVariables.complaint_email,
                    "RepresentativeMobile": BusinessVariables.complaint_mobile,
                    "RepresentativeProxyLetter": BusinessVariables.complaint_proxyLetter,
                    "Origin": BusinessVariables.complaint_origin,
                    "ID": "",
                    "MobileNumber": BusinessVariables.complaint_repMobile,
                    "NationalityID": 0,
                    "Email": BusinessVariables.complaint_repEmail,
                    "Type": "BUSINESS",
                    "NameEN": BusinessVariables.complaint_repName,
                    "NameAR": "tradeArabicSecond",
                    "SecondMobileNumber": BusinessVariables.defendand_mobile,
                    "Website": BusinessVariables.complaint_website,
                    "PhoneNumber": BusinessVariables.complaint_phone,
                    "EmirateID": 0,
                    "Address": BusinessVariables.complaint_address,
                    "PO_Box": BusinessVariables.complaint_poBox,
                    "CallingSystem": 0,
                    "PreferedLanguage": 0,
                    "ResidencyID": 0,
                    "AccountID": "",
                    "CreatedDate": "2016-09-28T10:00:00",
                    "ModifiedDate": "2016-09-28T10:00:00",
                    "CreatedBy": "N/A",
                    "ModifiedBy": "N/A",
                    "AdditionalInfo": ""
                ],
                "InquirySource": "2",
                "InquiryCategory": BusinessVariables.Inquiry_Category,
                "InquiryDetails": BusinessVariables.Inquiry_Details,
                "DedReply": ""
            ],
                              "Comments": emptyArray,
                              "Attachments": emptyArray,
                              "WorkflowRequest": [
                                "Workflow": ["Id": WORKFLOW_ID!],
                                "Action": WORKFLOW_ACTION
            ]
        ]
        print(RequestDetails)
        
        
        DC_NETWORK.postJson(DCAPIConstants.BaseURL + DCAPIConstants.SubmitComplaint, method: "POST", params: RequestDetails, is_Dictionary: true) { (succeeded, json) in
            dispatch_async(dispatch_get_main_queue(), {
                print(json)
                self.delegate?.submitClicked!(true)
            })
        }
        
    }
    @IBAction func TermsNConditions(sender: AnyObject)
    {
        let TermsNConditionsVC = self.storyboard?.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.TermsNConditionsVC)
        self.presentViewController(TermsNConditionsVC!, animated: true, completion: nil)
    }
    
    @IBAction func categoryAction(sender: AnyObject)
    {
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var categoryArray: [String] = []
        
        if LOOKUP_COMMERCIAL_SECTOR?.count != 0 {
            for i in 0..<LOOKUP_COMMERCIAL_SECTOR!.count {
                categoryArray.append(LOOKUP_COMMERCIAL_SECTOR![i][fetchKey] as! String)
            }
        }
        
        self.showSelectionTableView("Category", options: categoryArray)
        { (selectedString, index) in
            
            self.categoryLabel.text = selectedString
            
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if iPhone {
            switch indexPath.row {
            case 2:
                return 78
            case 3:
                return 60
            case 4:
                return 35
            default:
                return 44
            }
        }else {
            switch indexPath.row {
            case 2:
                return 78
            case 3:
                return 60
            case 4:
                return 35
            default:
                return 44
            }
        }
    }
    @IBAction func acceptTermsAction(sender: AnyObject)
    {
        
//        acceptedterms = !acceptedterms
        
        
        if acceptedterms == true
        {
            acceptTermsImageView.image = UIImage(named: "uncheck")
            acceptedterms = false
        }
        else
        {
            acceptTermsImageView.image = UIImage(named: "Check")
            acceptedterms = true
        }
        
    }
    



  }
