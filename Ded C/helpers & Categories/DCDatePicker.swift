//
//  DCDatePicker.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 06/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

protocol datePickerDelegate {
    func passDate(date: NSDate)
}
class DCDatePicker: UIViewController {

    //MARK: - Variables
    var delegate: datePickerDelegate?
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //MARK: - Actions
    @IBAction func DismissPicker(sender: AnyObject) {
        delegate?.passDate(datePicker.date)
        dismissViewControllerAnimated(true) {
            
        }
    }
    
    @IBAction func CancelPicker(sender: AnyObject) {
        dismissViewControllerAnimated(true) { 
            
        }
    }
    
    @IBAction func pickerChangedDate(sender: UIDatePicker) {
        print(sender.date)
        
    }
    
    
    //MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
