//
//  UIViewControllerExtensions.swift
//  Ded C
//
//  Created by Focaloid on 8/6/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

extension UIViewController
{
    
    func addTapGesture()
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UIViewController.tapAction(_:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func tapAction(sender:AnyObject)
    {
        self.view.endEditing(true)
        
    }
    
    func openImagesActionSheet(completion:((UIImage?)->Void)?)
    {
        let alertController = UIAlertController()
        
        let cameraAction = UIAlertAction(title: "Camera", style: .Default)
        { (action) in
            

            DCImagePicker.sharedInstance.takePhoto(self, completion: completion)
            
        }
      
        let photosAction = UIAlertAction(title: "Photos", style: .Default)
        { (action) in
            

            DCImagePicker.sharedInstance.openPhotos(self, completion: completion)
            
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel)
        { (action) in
            
            alertController.dismissViewControllerAnimated(true
                , completion:nil)
        }
        
        alertController.addAction(cameraAction)
        alertController.addAction(photosAction)
        alertController.addAction(cancelAction)
        
        if iPad {
            alertController.popoverPresentationController!.sourceView = self.view
            alertController.popoverPresentationController!.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0)
            alertController.popoverPresentationController?.permittedArrowDirections =  UIPopoverArrowDirection(rawValue: 0)

        }
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func showSelectionTableView(title:String, options:[String],completion:((String,Int)->Void)?)
    {
        let popupConfig = STZPopupViewConfig()
        popupConfig.dismissTouchBackground = true
        popupConfig.cornerRadius = 2
        popupConfig.overlayColor = UIColor.blackColor().colorWithAlphaComponent(0.7)
        popupConfig.showAnimation = STZPopupShowAnimation.SlideInFromBottom
        popupConfig.dismissAnimation = STZPopupDismissAnimation.SlideOutToBottom
        popupConfig.dismissCompletion = nil
        
        let tableView = DCSelectionTableView(frame: CGRect(x: 0,y: 0,width: 240,height: 240) , style: .Plain)
        tableView.headerTitle = title
        tableView.optionsArray = options
        tableView.delegate = tableView
        tableView.dataSource = tableView
        tableView.reloadData()
        
        tableView.bounces = false
        tableView.backgroundColor = UIColor.appThemeLightGreyColor()
        tableView.tableFooterView = UIView()
        
        tableView.selectionCompletion = {(selectedOption,index) in
            
                                          completion?(selectedOption,index)
                                          self.dismissPopupView()
            
                                        }
        self.presentPopupView(tableView, config: popupConfig)
    }
    
    
    //MARK: Workflow actions
    
    func getWorkflowIDForComplaintType(type:DCWorkflowType,completion:(workflowID:String?)->Void)
    {
        var params = "\"BUSINESS\""
        if type != DCWorkflowType.BusinessInquiry && type != DCWorkflowType.BusinessComplaint
        {
            params = "\"CONSUMER\""
        }
        
        DCNetwork.sharedNetwork.postString(DCAPIConstants.GetWorkflow, method: "POST", params: params, is_Dictionary: true)
        { (succeeded, json) in
            
            if let json = json as? [String:AnyObject]
            {
                
                if let workflows = json["Data"] as? [[String:AnyObject]]
                {
                    for workflow in workflows
                    {
                        if let uniqueKey = workflow["UniqueKey"] as? String where uniqueKey == type.rawValue
                        {
                            if let workflowId = workflow["Id"] as? String
                            {
                                completion(workflowID: workflowId)
                                return
                            }
                        }
                    }
                }
                print("workflow result : \(json)")
            }else {
                completion(workflowID: "")
            }
            
        }
    }
    
    
    func getWorkflowActions(id:String,completion:(success:Bool,actionDict:[String:AnyObject]?) -> ())
    {
        
        DCNetwork.sharedNetwork.getJson(DCAPIConstants.GetWorkflowActions, method: "GET",params: ["workflowId":id])
        { (succeeded, json) in
            
            if let json = json as? [String:AnyObject]
            {
                print("json : \(json)")
                
                completion(success:true,actionDict:json)
                
            }else {
                completion(success:false,actionDict:[:])
            }
            
            
            
        }
        
    }
    
    //MARK: Use this function to get workflow object
    
    func getWorkflowRequestObject(type:DCWorkflowType,completion:(workflowID:String?,success:Bool,WorkflowRequest:[String:AnyObject]?)->Void)
    {
        self.getWorkflowIDForComplaintType(DCWorkflowType.BusinessComplaint)
        { (workflowID) in
            
            if let id = workflowID
            {
                self.getWorkflowActions(id, completion:
                    {
                        (success: Bool, actionDict: [String : AnyObject]?) in
                        if let actions = actionDict where success == true
                        {
                            let workflowRequestDict : [String:AnyObject] = ["Workflow":["Id":id],"Action":actions]
                            completion(workflowID: id, success: true, WorkflowRequest: workflowRequestDict)
                            return
                            //We get actions here
                        }
                        
                        completion(workflowID: id, success: false, WorkflowRequest: nil)
                        
                        
                })
            }
            
            completion(workflowID: nil, success: false, WorkflowRequest: nil)
        }
    }
    
    
}