//
//  DCStoryBoards.swift
//  DedC
//
//  Created by Focaloid on 8/3/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

struct DCViewControllerIdentifiers
{
    
    //mainStoryboard
    static let BusinessDefendantDetailsVC = "DCDefendantDetails"
    static let ConsumerInquiryDetailsVC = "consumerInquiryDetailsVC"
    static let ConsumerComplaintDetailsVC = "consumerComplaintDetailsVC"
    static let ConsumerCommentDetailsVC = "consumerCommentDetailsVC"
    
    //complaintStoryBoard
    
    static let BusinessCompalaintVC = "businessMainVC"
    static let ConsumerComplaintVC = "consumerComplaintMainVC"
    static let ComplaintTrackVC = "DCComplaintTrack"
    static let AddCommentVC = "addCommentVC"
    
    //secondaryStoryboard
    static let DefendantdetailsVC = "defendantDetailsVC"
    
    
    //ComplaintDetails
    
    static let ComplaintdetailsVC = "DCComplaintDetails"
    
    static let InquierydetailsVC = "DCInquieryDetails"
    
    static let TermsNConditionsVC = "DCTermsNConditionsVC"
    
}

class DCStoryBoards
{
    static let sharedStoryBoard = DCStoryBoards()
    
    var mainStoryboard : UIStoryboard
    var secondaryStoryBoard : UIStoryboard
    var complaintStoryboard : UIStoryboard
    
    init()
    {
        
        
        if iPad {
            mainStoryboard = UIStoryboard(name: "Main_iPad", bundle: nil)
            if LANGUAGE == "en" {
                secondaryStoryBoard = UIStoryboard(name: "SecondStoryBoard_iPad", bundle: nil)
                complaintStoryboard = UIStoryboard(name: "DCComplaintStoryboard_iPad", bundle: nil)
            }else {
                secondaryStoryBoard = UIStoryboard(name: "SecondStoryBoard_ar_iPad", bundle: nil)
                complaintStoryboard = UIStoryboard(name: "DCComplaintStoryboard_ar_iPad", bundle: nil)
            }
        }else {
            mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if LANGUAGE == "en" {
                secondaryStoryBoard = UIStoryboard(name: "SecondStoryBoard", bundle: nil)
                complaintStoryboard = UIStoryboard(name: "DCComplaintStoryboard", bundle: nil)
            }else {
                secondaryStoryBoard = UIStoryboard(name: "SecondStoryBoard_ar", bundle: nil)
                complaintStoryboard = UIStoryboard(name: "DCComplaintStoryboard_ar", bundle: nil)
            }
        }
        
    }
    func changeLanguage() {
        if iPad {
            mainStoryboard = UIStoryboard(name: "Main_iPad", bundle: nil)
            if LANGUAGE == "en" {
                secondaryStoryBoard = UIStoryboard(name: "SecondStoryBoard_iPad", bundle: nil)
                complaintStoryboard = UIStoryboard(name: "DCComplaintStoryboard_iPad", bundle: nil)
            }else {
                secondaryStoryBoard = UIStoryboard(name: "SecondStoryBoard_ar_iPad", bundle: nil)
                complaintStoryboard = UIStoryboard(name: "DCComplaintStoryboard_ar_iPad", bundle: nil)
            }
        }else {
            mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if LANGUAGE == "en" {
                secondaryStoryBoard = UIStoryboard(name: "SecondStoryBoard", bundle: nil)
                complaintStoryboard = UIStoryboard(name: "DCComplaintStoryboard", bundle: nil)
            }else {
                secondaryStoryBoard = UIStoryboard(name: "SecondStoryBoard_ar", bundle: nil)
                complaintStoryboard = UIStoryboard(name: "DCComplaintStoryboard_ar", bundle: nil)
            }
        }
    }
}

