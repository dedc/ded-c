//
//  DCSelectionTableView.swift
//  Ded C
//
//  Created by Focaloid on 8/6/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit



class DCSelectionTableView: UITableView,UITableViewDelegate,UITableViewDataSource
{
    
    var optionsArray : [String] = []
    var headerTitle : String = ""
    
    var selectionCompletion : ((String,Int)->Void)?
    
    override func numberOfRowsInSection(section: Int) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return optionsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = self.dequeueReusableCellWithIdentifier("cell")
        if cell == nil
        {
            cell = UITableViewCell(style: .Default, reuseIdentifier: "cell")
        }
        
        cell?.backgroundColor = UIColor.appThemeLightGreyColor()
        cell?.textLabel?.textColor = UIColor.appThemeTextBlackColor()
        cell?.textLabel?.textAlignment = .Center
        cell?.textLabel?.text = optionsArray[indexPath.row]
        cell?.textLabel?.font = UIFont.systemFontOfSize(12)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let titlelabel = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.size.width, height: 40))
        titlelabel.backgroundColor = UIColor.appThemeDarkGreyColor()
        titlelabel.textColor = UIColor.blackColor()
        titlelabel.font = UIFont.systemFontOfSize(15)
        titlelabel.textAlignment = .Center
        titlelabel.text = headerTitle
        
        return titlelabel
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 35
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 40
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        self.deselectRowAtIndexPath(indexPath, animated: true)
        self.selectionCompletion?(optionsArray[indexPath.row],indexPath.row)
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.endEditing(true)
        return true
    }
    //MARK: EdgeInsets
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10)
        self.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0)
        self.scrollsToTop = true;
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        cell.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0)
        cell.preservesSuperviewLayoutMargins = false;
    }

    
    

}
