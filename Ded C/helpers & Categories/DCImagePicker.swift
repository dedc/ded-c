//
//  DCImagePicker.swift
//  Ded C
//
//  Created by Focaloid on 8/6/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCImagePicker: NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{

    static let sharedInstance = DCImagePicker()
    
    var imagePicker : UIImagePickerController = UIImagePickerController()
    var imagePickerCompletion : ((UIImage?) -> Void)?

    
    func takePhoto(viewController : UIViewController ,completion : ((UIImage?) -> Void)?)
    {
     
        
        if UIImagePickerController.isSourceTypeAvailable(.Camera)
        {
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .Camera
            self.imagePicker.delegate = self
            self.imagePicker.showsCameraControls = true
            self.imagePicker.cameraDevice = .Rear
            self.imagePickerCompletion = completion
            viewController.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            UIAlertView(title: "Camera Permissions Denied", message: "", delegate: nil, cancelButtonTitle: "OK").show()
        }
        
    }
    
    func takeSelfie(viewController : UIViewController , completion : ((UIImage?) -> Void)?)
    {
       
        if UIImagePickerController.isSourceTypeAvailable(.Camera)
        {
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .Camera
            self.imagePicker.delegate = self
            self.imagePicker.showsCameraControls = true
            self.imagePicker.cameraDevice = .Front
            self.imagePickerCompletion = completion
            viewController.presentViewController(imagePicker, animated: true, completion: nil)

        }
        else
        {
            UIAlertView(title: "Camera Permissions Denied", message: "", delegate: nil, cancelButtonTitle: "OK").show()
        }
        
    }
    
    func openPhotos(viewController : UIViewController , completion : ((UIImage?) -> Void)?)
    {
      
        if UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary)
        {

            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .PhotoLibrary
            self.imagePicker.delegate = self
            self.imagePickerCompletion = completion
            dispatch_async(dispatch_get_main_queue(),
            {
                let appdelegate = UIApplication.sharedApplication().delegate as? AppDelegate
                appdelegate?.iPadPortrait = true
                viewController.presentViewController(self.imagePicker, animated: true, completion: nil)
            })
        }
        else
        {
            UIAlertView(title: "Photos Permissions Denied", message: "", delegate: nil, cancelButtonTitle: "OK").show()
        }
        
    }

    
    //MARK : ImagePickerDelegate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        self.imagePickerCompletion?(pickedImage)
        dispatch_async(dispatch_get_main_queue())
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate?.iPadPortrait = false
            picker.dismissViewControllerAnimated(true,completion: nil)

        }

    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.imagePickerCompletion?(nil)
        dispatch_async(dispatch_get_main_queue())
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate?.iPadPortrait = false
            picker.dismissViewControllerAnimated(true,completion: nil)
            
        }

    }
    
    

    
    
}
