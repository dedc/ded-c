//
//  SDConsumerComplaintsVC.swift
//  DedC
//
//  Created by Focaloid on 8/3/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit
import iCarousel

class DCConsumerComplaintsVC: UIViewController, iCarouselDataSource, iCarouselDelegate
{

    @IBOutlet weak var carouselView: iCarousel!
    var iCarouselView: [UIView] = []
    @IBOutlet weak var pageDots: UIPageControl!
    @IBOutlet var segmentControl: ADVSegmentedControl!
    @IBOutlet weak var mainContainer: UIView!
    var segmentIndex: Int = 0
    
    weak var detailsVC : DCConsumerDetailsVC!
    weak var detailsDefendand : DCConsumerdefendantDetailsVC!
    weak var detailsComplaint : DCConsumerComplaintDetailsTableVC!
    weak var detailsInquiry : DCConsumerInquiryTableVC!
    weak var detailsComment : DCConsumerCommentTableVC!
    
    var ConsumerComplaintID = ""
    var ConsumerInquiryID = ""
    var ConsumerCommentID = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.title = "Consumer Complaint".localized(LANGUAGE!)
        segmentControl.items = ["Complaint".localized(LANGUAGE!), "Inquiry".localized(LANGUAGE!), "Comment".localized(LANGUAGE!)]
        
        addTapGesture()
        changeSelectedBgColor()
        
    }
    override func viewWillAppear(animated: Bool) {
        if iPad {
            carouselView.delegate = self
            carouselView.dataSource = self
            
            detailsVC = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("DCConsumerDetailsVC") as! DCConsumerDetailsVC
            detailsVC.view.backgroundColor = UIColor.clearColor()
            
            detailsDefendand = DCStoryBoards().secondaryStoryBoard.instantiateViewControllerWithIdentifier("DCConsumerdefendantDetailsVC") as! DCConsumerdefendantDetailsVC
            detailsDefendand.view.backgroundColor = UIColor.clearColor()
            
            detailsComplaint = DCStoryBoards().secondaryStoryBoard.instantiateViewControllerWithIdentifier("DCConsumerComplaintDetailsTableVC") as! DCConsumerComplaintDetailsTableVC
            
            detailsComplaint.view.backgroundColor = UIColor.clearColor()
            detailsInquiry = DCStoryBoards().secondaryStoryBoard.instantiateViewControllerWithIdentifier("DCInquieryTable") as! DCConsumerInquiryTableVC
            
            detailsInquiry.view.backgroundColor = UIColor.clearColor()
            detailsComment = DCStoryBoards().secondaryStoryBoard.instantiateViewControllerWithIdentifier("DCConsumerCommentTableVC") as! DCConsumerCommentTableVC
            
            detailsComplaint.view.backgroundColor = UIColor.clearColor()
            
            carouselView.backgroundColor = UIColor.clearColor()
            carouselView.type = .Linear
            carouselView.pagingEnabled = true
            carouselView.bounces = false
            carouselView.bounceDistance = 20
            carouselView.stopAtItemBoundary = true
            self.pageDots.numberOfPages = 3
            
            iCarouselView = [detailsVC.tableView, detailsDefendand.tableView, detailsComplaint.tableView]
            self.addChildViewController(detailsVC)
            detailsVC.didMoveToParentViewController(self)
            
            self.addChildViewController(detailsDefendand)
            detailsDefendand.didMoveToParentViewController(self)
            
            self.addChildViewController(detailsComplaint)
            detailsComplaint.didMoveToParentViewController(self)
          
            detailsVC.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
            detailsDefendand.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
            detailsComplaint.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
            detailsInquiry.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
            detailsComment.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
            
            segmentControl.selectedIndex = 0
            carouselView.reloadData()
            
            
        }
        if ConsumerComplaintID == "" {
            getWorkflowIDForComplaintType(DCWorkflowType.ConsumerComplaint) { (workflowID) in
                
                dispatch_async(dispatch_get_main_queue(), {
                    print(workflowID!)
                    WORKFLOW_ID = workflowID!
                    self.ConsumerComplaintID = workflowID!
                    self.callWorkFlowAction(self.ConsumerComplaintID)
                    
                })
                
            }
        }
    }
    func callWorkFlowAction(workflowID: String) {
        getWorkflowActions(workflowID, completion: { (success, actionDict) in
            dispatch_async(dispatch_get_main_queue(), {
                print(actionDict!)
                WORKFLOW_ACTION = (actionDict!["Data"]) as! NSDictionary
                
            })
        })
    }
    @IBAction func segmentAction(sender: ADVSegmentedControl)
    {
        segmentIndex = sender.selectedIndex
        if iPhone {
            if segmentControl.selectedIndex == DCConsumerComplaintType.Complaint.rawValue
            {
                self.detailsVC.detailsButton.setTitle("Defendant Details".localized(LANGUAGE!), forState: .Normal)
                self.detailsVC.HeaddingLabel.text = "Complaint Details".localized(LANGUAGE!)
                
                self.detailsVC.complaintType = .Complaint
                
                if ConsumerComplaintID == "" {
                    getWorkflowIDForComplaintType(DCWorkflowType.ConsumerComplaint) { (workflowID) in
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            print(workflowID!)
                            WORKFLOW_ID = workflowID!
                            self.ConsumerComplaintID = workflowID!
                            self.callWorkFlowAction(self.ConsumerComplaintID)
                            
                        })
                        
                    }
                }
                
            }
            
            if segmentControl.selectedIndex == DCConsumerComplaintType.Inquiry.rawValue
            {
                self.detailsVC.detailsButton.setTitle("Inquiry Details".localized(LANGUAGE!), forState: .Normal)
                self.detailsVC.HeaddingLabel.text = "Consumer Details".localized(LANGUAGE!)
                self.detailsVC.complaintType = .Inquiry
                
                if ConsumerInquiryID == "" {
                    getWorkflowIDForComplaintType(DCWorkflowType.ConsumerInquiry) { (workflowID) in
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            print(workflowID!)
                            WORKFLOW_ID = workflowID!
                            self.ConsumerInquiryID = workflowID!
                            self.callWorkFlowAction(self.ConsumerInquiryID)
                            
                        })
                        
                    }
                }
                
            }
            
            if segmentControl.selectedIndex == DCConsumerComplaintType.Comment.rawValue
            {
                self.detailsVC.detailsButton.setTitle("Comment Details".localized(LANGUAGE!), forState: .Normal)
                self.detailsVC.HeaddingLabel.text = "Consumer Details".localized(LANGUAGE!)
                self.detailsVC.complaintType = .Comment
                
                if ConsumerCommentID == "" {
                    getWorkflowIDForComplaintType(DCWorkflowType.ConsumerComment) { (workflowID) in
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            print(workflowID!)
                            WORKFLOW_ID = workflowID!
                            self.ConsumerCommentID = workflowID!
                            self.callWorkFlowAction(self.ConsumerCommentID)
                            
                        })
                        
                    }
                }
                
            }
        }else {
            
            carouselView.currentItemIndex = 0
            
            if segmentIndex == 0 {
                if segmentControl.selectedIndex == DCConsumerComplaintType.Complaint.rawValue {
                    self.detailsVC.HeaddingLabel.text = "Complaint Details".localized(LANGUAGE!)
                    
                    self.detailsVC.complaintType = .Complaint
                }
                
                if segmentControl.selectedIndex == DCConsumerComplaintType.Inquiry.rawValue {
                    self.detailsVC.HeaddingLabel.text = "Consumer Details".localized(LANGUAGE!)
                    self.detailsVC.complaintType = .Inquiry
                }
                
                if segmentControl.selectedIndex == DCConsumerComplaintType.Comment.rawValue {
                    self.detailsVC.HeaddingLabel.text = "Consumer Details".localized(LANGUAGE!)
                    self.detailsVC.complaintType = .Comment
                }
                iCarouselView = [detailsVC.tableView, detailsDefendand.tableView, detailsComplaint.tableView]
                detailsDefendand.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
                detailsComplaint.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
                self.addChildViewController(detailsVC)
                detailsVC.didMoveToParentViewController(self)
                
                self.addChildViewController(detailsDefendand)
                detailsDefendand.didMoveToParentViewController(self)
                
                self.addChildViewController(detailsComplaint)
                detailsComplaint.didMoveToParentViewController(self)
                
                if ConsumerComplaintID == "" {
                    getWorkflowIDForComplaintType(DCWorkflowType.ConsumerComplaint) { (workflowID) in
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            print(workflowID!)
                            WORKFLOW_ID = workflowID!
                            self.ConsumerComplaintID = workflowID!
                            self.callWorkFlowAction(self.ConsumerComplaintID)
                            
                        })
                        
                    }
                }
            }else if segmentIndex == 1 {
                self.detailsVC.complaintType = .Inquiry
                detailsInquiry = DCStoryBoards().secondaryStoryBoard.instantiateViewControllerWithIdentifier("DCInquieryTable") as! DCConsumerInquiryTableVC
                iCarouselView = [detailsVC.tableView, detailsInquiry.tableView]
                detailsInquiry.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
                self.addChildViewController(detailsVC)
                detailsVC.didMoveToParentViewController(self)
                
                self.addChildViewController(detailsInquiry)
                detailsInquiry.didMoveToParentViewController(self)
                
                if ConsumerInquiryID == "" {
                    getWorkflowIDForComplaintType(DCWorkflowType.ConsumerInquiry) { (workflowID) in
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            print(workflowID!)
                            WORKFLOW_ID = workflowID!
                            self.ConsumerInquiryID = workflowID!
                            self.callWorkFlowAction(self.ConsumerInquiryID)
                            
                        })
                        
                    }
                }
            }else {
                self.detailsVC.complaintType = .Comment
                detailsComment = DCStoryBoards().secondaryStoryBoard.instantiateViewControllerWithIdentifier("DCConsumerCommentTableVC") as! DCConsumerCommentTableVC
                iCarouselView = [detailsVC.tableView, detailsComment.tableView]
                detailsComment.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
                self.addChildViewController(detailsVC)
                detailsVC.didMoveToParentViewController(self)
                
                self.addChildViewController(detailsComment)
                detailsComment.didMoveToParentViewController(self)
                
                if ConsumerCommentID == "" {
                    getWorkflowIDForComplaintType(DCWorkflowType.ConsumerComment) { (workflowID) in
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            print(workflowID!)
                            WORKFLOW_ID = workflowID!
                            self.ConsumerCommentID = workflowID!
                            self.callWorkFlowAction(self.ConsumerCommentID)
                            
                        })
                        
                    }
                }
            }
            
            detailsVC.tableView.contentInset = UIEdgeInsetsMake(20, 0, 20, 0);
            
//            segmentControl.selectedIndex = 0
            carouselView.reloadData()
        }
        
        changeSelectedBgColor()
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        self.pageDots.numberOfPages = iCarouselView.count
        return iCarouselView.count
    }
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView {
        //        var label: UILabel
        let itemView = iCarouselView[index]
        itemView.frame = CGRectMake(0, 0, 700, 535)
        itemView.backgroundColor = UIColor.clearColor()
        
        for view in itemView.subviews {
            view.backgroundColor = UIColor.clearColor()
            print(view)
        }
        
//        if (view == nil) {
//            itemView = UIView(frame:CGRect(x:0, y:0, width: 660, height:520))
//            itemView.clipsToBounds = true
//            
//            itemView.contentMode = .ScaleAspectFill
//        } else {
//            itemView = view;
//        }
//        if index == 0 {
//            
//            itemView.addSubview(detailsVC.view)
//        }else if index == 1 {
////            itemView.addSubview(detailsVCInq.view)
//        }else {
////            itemView.addSubview(detailsVCType.view)
//        }
//        itemView.backgroundColor = UIColor.redColor()
//        let imageDict = imageArray[index]
//        let url = URLs.kBaseURL.stringByAppendingString(imageDict.valueForKey("image") as! String)
//        itemView.sd_setImageWithURL(NSURL(string: url), placeholderImage: UIImage(named: "placeholder"), options: .ProgressiveDownload)
        return itemView
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch (option) {
        case .Spacing:
            return value+0.1
        default:
            return value
        }
    }
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int) {
        
    }
    
    func carouselDidEndScrollingAnimation(carousel: iCarousel) {
        pageDots.currentPage = carousel.currentItemIndex
    }

    func setScrollCollection(scrollArray: [UIView]) {
        var subViewArray: [UIView]!
        let view1 = UIView()
        let view2 = UIView()
        let view3 = UIView()
        subViewArray = [view1, view2, view3]
        
        let x = ((self.view.frame.size.width - 700) / 2)
//        carouselView.contentSize = CGSizeMake(self.view.frame.size.width * CGFloat(scrollArray.count), 0)
//        let colorArray = [UIColor.redColor(), UIColor.grayColor(), UIColor.blueColor()]
        
        for i in 0..<scrollArray.count {
            subViewArray[i].frame = CGRectMake((CGFloat(i) * CGFloat(self.view.frame.size.width)) + x, 0, 700, 580)
            
            print(subViewArray[i].frame)
//            subViewArray[i].backgroundColor = colorArray[i]
            let mainView = [detailsVC.tableView, detailsDefendand.tableView, detailsComplaint.tableView]
            //            mainView[i].frame = CGRectMake((self.view.frame.size.width - 700) / 2 , 0, 700, 535)
            mainView[i].backgroundColor = UIColor.clearColor()
            subViewArray[i].addSubview(mainView[i])
            //            self.addChildViewController(mainView[i])
            //            mainView[i].didMoveToParentViewController(self)
            //            subViewArray[i].addSubview(mainView)
            carouselView.addSubview(subViewArray[i])
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if let detailsVC = segue.destinationViewController as? DCConsumerDetailsVC
        {
            self.detailsVC = detailsVC
        }
    }
    
    
    func changeSelectedBgColor() {
        for subview in segmentControl.subviews
        {
            if let control = subview as? UIControl
            {
                if control.selected == true
                {
                    control.tintColor = UIColor.appThemeYellowColor()
                }
                else
                {
                    control.tintColor = UIColor.appThemeYellowColor()
                }
            }
        }
    }
}
