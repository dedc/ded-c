
//
//  DCComplaintSubmitView.swift
//  Ded C
//
//  Created by Focaloid on 8/7/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

protocol DCComplaintSubmitDelegate : NSObjectProtocol
{
    func submitViewDidPressSubmit()
}

class DCComplaintSubmitView: UIView
{

    @IBOutlet var complaintlabel: UILabel!
    
    weak var delegate : DCComplaintSubmitDelegate?
    
    
//    override func layoutSubviews()
//    {
//        super.layoutSubviews()
//        
//        self.layer.cornerRadius = 3
//        
//    }
    
    @IBAction func doneAction(sender: AnyObject)
    {
     
        delegate?.submitViewDidPressSubmit()
        
    }

}
