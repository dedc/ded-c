//
//  DCBusinessMainVC.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 03/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit
import iCarousel

class DCBusinessMainVC: UIViewController, DefendandDelegate, iCarouselDataSource, iCarouselDelegate{

    //MARK:- Variables
    @IBOutlet weak var HomeTable: UITableView!
    @IBOutlet weak var HomeSegment: ADVSegmentedControl!
    @IBOutlet weak var carouselView: iCarousel!
    @IBOutlet weak var complaintContainer: UIView!
    @IBOutlet weak var inquieryContainer: UIView!
    @IBOutlet weak var pageDots: UIPageControl!
    var segmentIndex: Int = 0
    var iCarouselView: [UIView] = []
    weak var businessComplaint : DCBusinessComplaintTableVC!
    weak var businessDefendand : DCBusinessDefendantTableVC!
    weak var businessComplaintDetails : DCBusinessComplaintDetailsTableVC!
    weak var businessInquiery : DCBusinessInquiryTableVC!
    weak var businessInquieryDetails : DCBusinessInquiryDetailsTableVC!
    
    var firstTime = true
    var BusinessInquiryID = ""
    var BusinessComplaintID = ""
    
    //MARK:- View Methods
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
//        self.navigationController?.navigationBarHidden = true
        if iPad && firstTime == true
        {
            firstTime = false
            carouselView.delegate = self
            carouselView.dataSource = self
            // 1- DCBusiness
            businessComplaint = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("DCBusiness") as! DCBusinessComplaintTableVC
            businessComplaint.view.backgroundColor = UIColor.clearColor()
            
            // 2- DCDefendantTable
            businessDefendand = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("DCDefendantTable") as! DCBusinessDefendantTableVC
            businessDefendand.view.backgroundColor = UIColor.clearColor()
            
            // 3- Complaint Details
            businessComplaintDetails = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("DCComplaintsTable") as! DCBusinessComplaintDetailsTableVC
            businessComplaintDetails.view.backgroundColor = UIColor.clearColor()
            
            // 4- Inquiery DCInquiery
            businessInquiery = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("DCInquiery") as! DCBusinessInquiryTableVC
            businessInquiery.view.backgroundColor = UIColor.clearColor()
            
            // 5- Inquiery Details
            businessInquieryDetails = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("DCInquieryTable") as! DCBusinessInquiryDetailsTableVC
            businessInquieryDetails.view.backgroundColor = UIColor.clearColor()
            
            
            
            carouselView.backgroundColor = UIColor.clearColor()
            carouselView.type = .Linear
            carouselView.pagingEnabled = true
            carouselView.bounces = false
            carouselView.clipsToBounds = false
            carouselView.bounceDistance = 20
            carouselView.stopAtItemBoundary = true
            self.pageDots.numberOfPages = 2
            
            iCarouselView = [businessComplaint.tableView, businessDefendand.tableView, businessComplaintDetails.tableView]
            self.addChildViewController(businessComplaint)
            businessComplaint.didMoveToParentViewController(self)
            
            self.addChildViewController(businessDefendand)
            businessDefendand.didMoveToParentViewController(self)
            
            self.addChildViewController(businessComplaintDetails)
            businessComplaintDetails.didMoveToParentViewController(self)
            
            
            businessComplaint.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
            businessDefendand.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
            businessComplaintDetails.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
            
            HomeSegment.selectedIndex = 0
            carouselView.reloadData()
            
            
        }
        
        getWorkflowIDForComplaintType(DCWorkflowType.BusinessComplaint) { (workflowID) in
            
            dispatch_async(dispatch_get_main_queue(), {
                print(workflowID!)
                WORKFLOW_ID = workflowID!
                self.callWorkFlowAction(workflowID!)
            })
            
        }
    }
    func callWorkFlowAction(workflowID: String) {
        getWorkflowActions(workflowID, completion: { (success, actionDict) in
            dispatch_async(dispatch_get_main_queue(), {
                print(actionDict!)
                WORKFLOW_ACTION = (actionDict!["Data"]) as! NSDictionary
            })
        })
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if iPad {
            for view in carouselView.visibleItemViews {
                (view as? UIView)?.backgroundColor = UIColor.clearColor()
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        HomeSegment.items = ["Complaint".localized(LANGUAGE!), "Inquiry".localized(LANGUAGE!)]
        
        self.addTapGesture()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - iCarousel Delegates
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        self.pageDots.numberOfPages = iCarouselView.count
        return iCarouselView.count
    }
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView {
        //        var label: UILabel
        let itemView = iCarouselView[index]
        view?.backgroundColor = UIColor.clearColor()
        itemView.backgroundColor = UIColor.clearColor()
        itemView.frame = CGRectMake(0, 0, 700, 535)
        //        if (view == nil) {
        //            itemView = UIView(frame:CGRect(x:0, y:0, width: 660, height:520))
        //            itemView.clipsToBounds = true
        //
        //            itemView.contentMode = .ScaleAspectFill
        //        } else {
        //            itemView = view;
        //        }
        //        if index == 0 {
        //
        //            itemView.addSubview(detailsVC.view)
        //        }else if index == 1 {
        ////            itemView.addSubview(detailsVCInq.view)
        //        }else {
        ////            itemView.addSubview(detailsVCType.view)
        //        }
        //        itemView.backgroundColor = UIColor.redColor()
        //        let imageDict = imageArray[index]
        //        let url = URLs.kBaseURL.stringByAppendingString(imageDict.valueForKey("image") as! String)
        //        itemView.sd_setImageWithURL(NSURL(string: url), placeholderImage: UIImage(named: "placeholder"), options: .ProgressiveDownload)
        return itemView
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch (option) {
        case .Spacing:
            return value+0.1
        default:
            return value
        }
    }
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int) {
        
    }
    
    func carouselDidEndScrollingAnimation(carousel: iCarousel) {
        pageDots.currentPage = carousel.currentItemIndex
    }
    
    //MARK:- Actions
    func defendandClicked(isClicked: Bool)
    {
//        self.performSegueWithIdentifier("defendandDetails", sender: self)
        let businessDefendantDetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.BusinessDefendantDetailsVC)
        self.navigationController?.pushViewController(businessDefendantDetailsVC!, animated: true)
        
    }
    
    
    @IBAction func DefendandDetails(sender: AnyObject) {
        
    }
    @IBAction func SwitchForm(sender: ADVSegmentedControl) {
        segmentIndex = sender.selectedIndex
        
        if iPhone {
            if sender.selectedIndex == 0 {
                complaintContainer.hidden = false
                inquieryContainer.hidden = true
            }else {
                complaintContainer.hidden = true
                inquieryContainer.hidden = false
            }
        }else {
            
            carouselView.currentItemIndex = 0
            
            if segmentIndex == 0 {
                iCarouselView = [businessComplaint.tableView, businessDefendand.tableView, businessComplaintDetails.tableView]
                self.addChildViewController(businessComplaint)
                businessComplaint.didMoveToParentViewController(self)
                
                self.addChildViewController(businessDefendand)
                businessDefendand.didMoveToParentViewController(self)
                
                self.addChildViewController(businessComplaintDetails)
                businessComplaintDetails.didMoveToParentViewController(self)
                
                businessComplaint.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
                businessDefendand.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
                businessComplaintDetails.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
            }else {
                
                businessInquiery = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("DCInquiery") as! DCBusinessInquiryTableVC
                businessInquiery.tableView.backgroundColor = UIColor.clearColor()
                
                businessInquieryDetails = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("DCInquieryTable") as! DCBusinessInquiryDetailsTableVC
                businessInquiery.tableView.backgroundColor = UIColor.clearColor()
                
                iCarouselView = [businessInquiery.tableView, businessInquieryDetails.tableView]
                
                self.addChildViewController(businessInquiery)
                businessInquiery.didMoveToParentViewController(self)
                
                self.addChildViewController(businessInquieryDetails)
                businessInquieryDetails.didMoveToParentViewController(self)
                
                businessInquiery.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
                businessInquieryDetails.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
            }
            
            carouselView.reloadData()
        }
        
    }
    
    //MARK:- TableView Delegate Methods
    

    //MARK:- System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "Defendand"
        {
            let business = segue.destinationViewController as? DCBusinessComplaintTableVC
            business?.delegate = self
        }
    }

}
class DCBusinessHomeCell: UITableViewCell {
    
    
}