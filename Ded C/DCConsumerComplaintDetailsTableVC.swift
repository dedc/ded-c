//
//  DCConsumerComplaintDetailsTableVC.swift
//  Ded C
//
//  Created by Focaloid on 8/8/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCConsumerComplaintDetailsTableVC: UITableViewController,DCAddCommentDelegate,DCMultipleAttachmentCellDelegate, UIPopoverPresentationControllerDelegate, datePickerDelegate {
    
    @IBOutlet weak var termsCell: UITableViewCell!
    @IBOutlet var acceptTermsImageView: UIImageView!
    // MARK: - Variables
    var delegate: showAlertDelegate?
    
    @IBOutlet weak var dateOfPurchase: UILabel!
    @IBOutlet var commercialSectorLabel: UILabel!
    
    @IBOutlet var complaintTypeLabel: UILabel!
    @IBOutlet weak var subject: DCImagedTextField!
    @IBOutlet weak var complaintDetails: UITextView!
    @IBOutlet weak var valueOfPurchase: DCImagedTextField!
    @IBOutlet weak var paymentMethod: DCImagedTextField!
    
    @IBOutlet var currencylabel: UILabel!
    let popupConfig = STZPopupViewConfig()
    var acceptedterms : Bool = false
    var placeholderText = "Complaint Details".localized(LANGUAGE!)
    @IBOutlet var attachmentstableView: DCAttachmentTableView!
    
    
    // MARK: - View Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if iPad {
            termsCell.backgroundColor = UIColor.clearColor()
        }
        
        self.attachmentstableView.attachmentCellDelegate = self

        acceptedterms = false
        acceptTermsImageView.image = UIImage(named: "uncheck")
        
        self.tableView.showsVerticalScrollIndicator = false

        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if iPhone {
            switch indexPath.row
            {
            case 0,1,2,3,5,6,7:
                return 44
            case 4:
                return 80
            case 8:
                return 50 + CGFloat(self.attachmentstableView.attachmentsArray.count * 140) // Attachments
            case 9:
                return 65
            case 10:
                return 50
            case 11:
                return 40
            default:
                return 44
            }
        }else {
            switch indexPath.row
            {
            case 3:
                return 80
            case 5:
                return 57
            case 6:
                return 50 + CGFloat(self.attachmentstableView.attachmentsArray.count * 140) // Attachments
            case 7:
                return 65
            case 8:
                return 50
            default:
                return 44
            }
        }
        
    }
    
    // MARK: - Actions
    
    @IBAction func SubmitComplaint(sender: AnyObject) {
        
        
        let emptyArray : NSArray = [] as! NSArray
        
        let attachment = [
            "User":[
                "Id":"OTVENTURES\\remon.adel",
                "UserName":"Remon Adel",
                "SerialNumber":1415,
                "ST_HC":"Y ",
                "LastLoginDate":"",
                "NumberOfLogin":"",
                "Code":"1 ",
                "TitleCode":"",
                "PhoneOffice":"",
                "HomePhone":"",
                "MobilePhone":"",
                "FaxNumber":"",
                "EmailAddress":"Remon.Adel@linkdev.com",
                "DepartmentCode":"",
                "ReportTo":"",
                "OrganizationUnit":"",
                "DstCode":1,
                "OriginAd":0,
                "DistinguishName":"",
                "UserNameAr":"ريمون عادل",
                "UserNameEn":"Remon Adel",
                "AdditionalInfo":""
            ],
            "Accessability":"2",
            "Value":"First Comment To DED",
            "CALLING_SYSTEM":"WEB",
            "CRUD":"1",
            "Id":"",
            "ModifiedDate":"2016-08-31T11:11:47.246Z",
            "Attachments": [
                "DMSId":"DMSID",
                "Provider":"PROVIDER",
                "Path":"PATH",
                "Name":"NAME",
                "Extension":"Exetention",
                "Data": emptyArray,
                "$$hashKey":"object:567"
            ]
        ]
        let RequestDetails = ["RequestDetails":
            ["ParticipantDetails":
                ["ID": "",
                    "MobileNumber": ConsumerVariables.complaint_MobileCode + ConsumerVariables.complaint_MobileNumber,
                    "NationalityID":125,
                    "Email": ConsumerVariables.complaint_Email,
                    "Type":"CONSUMER",
                    "NameEN": ConsumerVariables.complaint_FullName,
                    "NameAR": ConsumerVariables.complaint_FullName,
                    "SecondMobileNumber": ConsumerVariables.complaint_SecondCode + ConsumerVariables.complaint_SecondNumber,
                    "PhoneNumber": ConsumerVariables.defendand_phone,
                    "EmirateID":1,
                    "Address": ConsumerVariables.defendand_address,
                    "PO_Box": ConsumerVariables.defendand_poBox,
                    "CallingSystem":0,
                    "PreferedLanguage":0,
                    "ResidencyID":1,
                    "AccountID":"",
                    "CreatedDate":"2016-09-22T10:30:00",
                    "ModifiedDate":"2016-09-22T10:30:00",
                    "CreatedBy":"N/A",
                    "ModifiedBy":"N/A",
                    "AdditionalInfo":""
                ],
                "CommentSource":"2",
                "CommentType":"2",
                "CallerComment":"Test Submit",
                "Role":"CM_SUBMIT",
                "Type": 3,
                "Subject": "Subject",
                "ComplaintDetails": "Test",
                "DateOfPurchase": "2016-09-22T10:30:00",
                "ValueOfPurchase": "",
                "Currency": "28",
                "DesireRefundAmount": "2342",
                "PaymentMethod": "Visa",
                "TradeLicenseNumber": "1"
            ],
                              "Comments": emptyArray,
                              "WorkflowRequest": [
                                "Workflow": ["Id": WORKFLOW_ID!],
                                "Action": WORKFLOW_ACTION
            ]
        ]
        print(RequestDetails)
        
        
        DC_NETWORK.postJson(DCAPIConstants.BaseURL + DCAPIConstants.SubmitComplaint, method: "POST", params: RequestDetails, is_Dictionary: true) { (succeeded, json) in
            dispatch_async(dispatch_get_main_queue(), {
                print(json)
            })
        }
        
    }
    
    func passDate(date: NSDate) {
        dateOfPurchase.text = DC_DELEGATE.DateToString(date, format: "dd/MM/yyyy")
        print(date)
    }
    @IBAction func showDatePicker(sender: AnyObject) {
        let menuViewController: DCDatePicker = DCStoryBoards().mainStoryboard.instantiateViewControllerWithIdentifier("DCDatePicker") as! DCDatePicker
        menuViewController.delegate = self
        menuViewController.modalPresentationStyle = .Popover
        menuViewController.preferredContentSize = CGSizeMake(320, 260)
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.delegate = self
        if iPad {
            popoverMenuViewController?.permittedArrowDirections = .Any
            popoverMenuViewController?.sourceView = sender as? UIView
            popoverMenuViewController?.sourceRect = CGRect(
                x: sender.frame.width / 2,
                y: sender.frame.height / 2,
                width: 1,
                height: 1)
        }else {
            popoverMenuViewController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            popoverMenuViewController?.sourceView = self.view
            popoverMenuViewController?.sourceRect = CGRect(
                x: self.view.frame.width / 2,
                y: self.view.frame.height / 2,
                width: 1,
                height: 1)
        }
        
        presentViewController(
            menuViewController,
            animated: true,
            completion: nil)
    }
    
    @IBAction func TermsNConditions(sender: AnyObject)
    {
        let TermsNConditionsVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.TermsNConditionsVC)
        self.presentViewController(TermsNConditionsVC, animated: true, completion: nil)
    }
    
    @IBAction func commercialSectorAction(sender: AnyObject)
    {
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var commercialSectorArray: [String] = []
        
        if LOOKUP_CURRENCY?.count != 0 {
            for i in 0..<LOOKUP_CURRENCY!.count {
                commercialSectorArray.append(LOOKUP_CURRENCY![i][fetchKey] as! String)
            }
        }

        self.showSelectionTableView("Commercial Sector", options: commercialSectorArray)
        { (selectedString, index) in
            
            self.commercialSectorLabel.text = selectedString
            
        }
    }
    
    
    
    @IBAction func currencyTypeAction(sender: AnyObject)
    {
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var currencyArray: [String] = []
        
        if LOOKUP_CURRENCY?.count != 0 {
            for i in 0..<LOOKUP_CURRENCY!.count {
                currencyArray.append(LOOKUP_CURRENCY![i][fetchKey] as! String)
            }
        }
        self.showSelectionTableView("Currency", options: currencyArray)
        { (selectedString, index) in
            
            self.currencylabel.text = selectedString
            
        }
    }
    
    
    
    @IBAction func complaintTypeAction(sender: AnyObject)
    {
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var complaintTypeArray: [String] = []
        
        if LOOKUP_COMPLAINT_TYPE?.count != 0 {
            for i in 0..<LOOKUP_COMPLAINT_TYPE!.count {
                complaintTypeArray.append(LOOKUP_COMPLAINT_TYPE![i][fetchKey] as! String)
            }
        }
        
        self.showSelectionTableView("Complaint Type", options: complaintTypeArray)
        { (selectedString, index) in
            
            self.complaintTypeLabel.text = selectedString
            
        }
    }
    
    
    @IBAction func acceptTermsAction(sender: AnyObject)
    {
        
//        acceptedterms = !acceptedterms
        
        
        if acceptedterms == true
        {
            acceptTermsImageView.image = UIImage(named: "uncheck")
            acceptedterms = false
        }
        else
        {
            acceptTermsImageView.image = UIImage(named: "Check")
            acceptedterms = true
        }
        
    }

    @IBAction func attachmentsAction(sender: AnyObject)
    {
        if iPad {
            popupConfig.dismissTouchBackground = true
            popupConfig.showAnimation = STZPopupShowAnimation.SlideInFromBottom
            popupConfig.dismissAnimation = STZPopupDismissAnimation.SlideOutToBottom
            popupConfig.dismissCompletion = nil
            var index: Int!
            if LANGUAGE == "en" {
                index = 0
            }else {
                index = 1
            }
            
            let commentVC = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("addCommentVC") as? DCAddCommentVC
            commentVC!.type = "trade"
            let popupView = commentVC?.view
            popupView!.frame = CGRect(x: 0, y: 0, width: 600, height: 450)
            popupView?.clipsToBounds = true
            self.presentPopupView(popupView!, config: popupConfig)
        }else {
            let commentVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.AddCommentVC) as! DCAddCommentVC
            commentVC.type = "trade"
            commentVC.commentDelegate = self
            self.presentViewController(commentVC, animated: true, completion: nil)
        }
        

    }
    
    

    
    //MARK: Multiple Attachment Delegates
    
    func attachmentCellDidPressMore(cell: DCMultipleAttachmentCell)
    {
        
        let alertController = UIAlertController()
        
        let editAction = UIAlertAction(title: "Edit", style: .Default)
        { (action) in
            
            
            let commentVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.AddCommentVC) as! DCAddCommentVC
            commentVC.commentDelegate = self
            commentVC.images = cell.images
            commentVC.comment = cell.comment
            commentVC.editMode = true
            if let indexPath = self.attachmentstableView.indexPathForCell(cell)
            {
                commentVC.indexPath = indexPath
                
            }
            
            self.presentViewController(commentVC, animated: true, completion: nil)
            
        }
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Default)
        { (action) in
            
            
            if let indexPath = self.attachmentstableView.indexPathForCell(cell)
            {
                
                self.attachmentstableView.attachmentsArray.removeAtIndex(indexPath.row)
                self.attachmentstableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel)
        { (action) in
            
            alertController.dismissViewControllerAnimated(true
                , completion:nil)
        }
        
        alertController.addAction(editAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: - PopOver Delegate
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    // MARK: - TextField Delegates
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if textView.text == placeholderText
        {
            textView.textColor = UIColor.blackColor()
            textView.text = ""
        }
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == placeholderText || textView.text == ""
        {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = placeholderText
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    //MARK: Add Attchment s Delegate
    
    func commentVCDidAddComments(attachmentObject: DCAttachmentObject,type:String)
    {
        
        self.attachmentstableView.attachmentsArray.append(attachmentObject)
        self.attachmentstableView.reloadData()
        self.tableView.reloadData()
        
    }
    
    func commentVCDidEditComments(attachmentObject : DCAttachmentObject,indexPath : NSIndexPath?,type:String)
    {
        
        if let indexPath = indexPath
        {
            self.attachmentstableView.attachmentsArray[indexPath.row] = attachmentObject
            self.attachmentstableView.reloadData()
            self.tableView.reloadData()
        }
        
        
    }

 
}
