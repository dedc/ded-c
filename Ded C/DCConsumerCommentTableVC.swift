//
//  DCConsumerCommentTableVC.swift
//  Ded C
//
//  Created by Focaloid on 8/8/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCConsumerCommentTableVC: UITableViewController,DCAddCommentDelegate,DCMultipleAttachmentCellDelegate
{

    @IBOutlet var commentCheck: UIImageView!
    @IBOutlet var suggestionCheck: UIImageView!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var suggestionLabel: UILabel!
    var placeholderText = "Comment".localized(LANGUAGE!)
    @IBOutlet var acceptTermsImageView: UIImageView!
    let popupConfig = STZPopupViewConfig()
    var acceptedterms : Bool = false
    @IBOutlet weak var termsCell: UITableViewCell!
    @IBOutlet weak var sectionSpace: UITableViewCell!
    @IBOutlet weak var Comments: UITextView!
    
    
    @IBOutlet var attachmentsTableView: DCAttachmentTableView!

    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if iPad {
            termsCell.backgroundColor = UIColor.clearColor()
            sectionSpace.backgroundColor = UIColor.clearColor()
        }
        suggestionCheck.image = UIImage(named: "Check_black")
        commentCheck.image = UIImage(named: "Check_black")

        commentCheck.hidden = true
        
        acceptedterms = false
        acceptTermsImageView.image = UIImage(named: "uncheck")
        
        self.attachmentsTableView.attachmentCellDelegate = self
        
        self.tableView.showsVerticalScrollIndicator = false


    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if iPad {
            switch indexPath.row {
            case 1:
                return 70
            case 2:
                return 86
            case 3:
                return 50 + CGFloat(attachmentsTableView.attachmentsArray.count * 140) // Attachment
            case 4:
                return 20
            case 5:
                return 40
            case 6, 7:
                return 50
            default:
                return 44
            }
        }else {
            switch indexPath.row {
            case 1:
                return 70
            case 2:
                return 78
            case 3:
                return 50 + CGFloat(attachmentsTableView.attachmentsArray.count * 140) // Attachment
            case 4:
                return 60
            case 5, 7:
                return 50
            case 6:
                return 40
            default:
                return 44
            }
        }
    }
    
    @IBAction func suggestionAction(sender: AnyObject)
    {
        suggestionLabel.textColor = UIColor.blackColor()
        commentLabel.textColor = UIColor.lightGrayColor()
        commentCheck.hidden = true
        suggestionCheck.hidden = false
        
    }
   
    @IBAction func commentAction(sender: AnyObject)
    {
        commentLabel.textColor = UIColor.blackColor()
        suggestionLabel.textColor = UIColor.lightGrayColor()
        commentCheck.hidden = false
        suggestionCheck.hidden = true

    }
    
    @IBAction func termsAction(sender: AnyObject)
    {
        let TermsNConditionsVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.TermsNConditionsVC)
        self.presentViewController(TermsNConditionsVC, animated: true, completion: nil)

    }
    
    @IBAction func acceptAction(sender: AnyObject)
    {
        
//        acceptedterms = !acceptedterms
        
        if acceptedterms == true
        {
            acceptTermsImageView.image = UIImage(named: "uncheck")
            acceptedterms = false
        }
        else
        {
            acceptTermsImageView.image = UIImage(named: "Check")
            acceptedterms = true
        }
        
    }

    
    @IBAction func commentsAction(sender: AnyObject)
    {
        
        if iPad {
            popupConfig.dismissTouchBackground = true
            popupConfig.showAnimation = STZPopupShowAnimation.SlideInFromBottom
            popupConfig.dismissAnimation = STZPopupDismissAnimation.SlideOutToBottom
            popupConfig.dismissCompletion = nil
            var index: Int!
            if LANGUAGE == "en" {
                index = 0
            }else {
                index = 1
            }
            
            let commentVC = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("addCommentVC") as? DCAddCommentVC
            commentVC!.type = "trade"
            let popupView = commentVC?.view
            popupView!.frame = CGRect(x: 0, y: 0, width: 600, height: 450)
            popupView?.clipsToBounds = true
            self.presentPopupView(popupView!, config: popupConfig)
        }else {
            let commentVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.AddCommentVC) as! DCAddCommentVC
            commentVC.type = "trade"
            commentVC.commentDelegate = self
            self.presentViewController(commentVC, animated: true, completion: nil)
        }
        
    }

    

    
    //MARK: Multiple Attachment Delegates
    
    func attachmentCellDidPressMore(cell: DCMultipleAttachmentCell)
    {
        
        let alertController = UIAlertController()
        
        let editAction = UIAlertAction(title: "Edit", style: .Default)
        { (action) in
            
            
            let commentVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.AddCommentVC) as! DCAddCommentVC
            commentVC.commentDelegate = self
            commentVC.images = cell.images
            commentVC.comment = cell.comment
            commentVC.editMode = true
            if let indexPath = self.attachmentsTableView.indexPathForCell(cell)
            {
                commentVC.indexPath = indexPath
                
            }
            
            self.presentViewController(commentVC, animated: true, completion: nil)
            
        }
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Default)
        { (action) in
            
            
            if let indexPath = self.attachmentsTableView.indexPathForCell(cell)
            {
                
                self.attachmentsTableView.attachmentsArray.removeAtIndex(indexPath.row)
                self.attachmentsTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel)
        { (action) in
            
            alertController.dismissViewControllerAnimated(true
                , completion:nil)
        }
        
        alertController.addAction(editAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    //MARK : TextViewDelegate Methods
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if textView.text == placeholderText
        {
            textView.textColor = UIColor.blackColor()
            textView.text = ""
        }
        return true
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == placeholderText || textView.text == ""
        {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = placeholderText
        }
    }
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    //MARK: Add Attchment s Delegate
    
    func commentVCDidAddComments(attachmentObject: DCAttachmentObject,type:String)
    {
        
        self.attachmentsTableView.attachmentsArray.append(attachmentObject)
        self.attachmentsTableView.reloadData()
        self.tableView.reloadData()
        
    }
    
    func commentVCDidEditComments(attachmentObject : DCAttachmentObject,indexPath : NSIndexPath?,type:String)
    {
        
        if let indexPath = indexPath
        {
            self.attachmentsTableView.attachmentsArray[indexPath.row] = attachmentObject
            self.attachmentsTableView.reloadData()
            self.tableView.reloadData()
        }
        
        
    }
    
}
