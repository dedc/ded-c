//
//  DCBusinessDefendantVC.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 03/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCBusinessDefendantVC: UIViewController
{

    // MARK: - Variables
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool)
    {
//        self.navigationController?.navigationBarHidden = true
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        addTapGesture()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    @IBAction func Back(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
