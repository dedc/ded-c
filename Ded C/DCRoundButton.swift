//
//  DCRoundButton.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 03/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCRoundButton: UIButton {

    let gradientLayer = CAGradientLayer()
    @IBInspectable var Boarder_Colour: UIColor = UIColor.clearColor()
    @IBInspectable var Boarder_Width: CGFloat = 0.0
    @IBInspectable var Corner_Radius: CGFloat = 0.0
    
    override func drawRect(rect: CGRect) {
        
        self.layer.borderColor = Boarder_Colour.CGColor
        self.layer.borderWidth = Boarder_Width
        self.layer.cornerRadius = Corner_Radius
        self.layer.masksToBounds = true
        
    }
}
