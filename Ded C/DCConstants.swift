//
//  DCConstants.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 03/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

let App_Version = "1.0.0"
let APPDELEGATE = UIApplication.sharedApplication().delegate as? AppDelegate
var WEB_SERVICE = "http://dedhub.linkdev.com/api/"
let THEME_COLOUR = UIColor(red:0.11, green:0.49, blue:0.48, alpha:1.0) /* #1D7C7B */

// MARK:- Public Variables
let DC_DELEGATE: DCCustomDelegates = DCCustomDelegates()
let DC_NETWORK: DCNetwork = DCNetwork()
let FILE_MANAGER = NSFileManager.defaultManager()
let FONT_NAME = "Helvetica-Light"
let FONT_NAME_B = "Helvetica-Bold"
let FONT_NAME_R = "Helvetica"
let DC_DEFAULTS = NSUserDefaults.standardUserDefaults()
let DC_DATE_FORMATTER = NSDateFormatter()
var IS_REACHABLE: Bool!
var USER_NAME = DC_DEFAULTS.stringForKey("User_Name")
var PROFILE_PICTURE = DC_DEFAULTS.stringForKey("ProfilePic")
var KEYBOARD_FRAME: CGFloat = 216.0
let Device = UIDevice.currentDevice()
var iPad =  (Device.userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
var iPhone = (Device.userInterfaceIdiom == UIUserInterfaceIdiom.Phone)
var bounds: CGRect = UIScreen.mainScreen().bounds
var SCREEN_HEIGHT:CGFloat = bounds.size.height
var LANGUAGE = DC_DEFAULTS.stringForKey("language")
var complaintDictionary : [String:AnyObject] = [String:AnyObject] ()
var WORKFLOW_ID: String!
var WORKFLOW_ACTION: NSDictionary!

var LOOKUP_CURRENCY = DC_DEFAULTS.valueForKey(DCLookupCodes.Currency.rawValue)
var LOOKUP_COMPLAINT_TYPE = DC_DEFAULTS.valueForKey(DCLookupCodes.ComplaintType.rawValue)
var LOOKUP_COMMERCIAL_SECTOR = DC_DEFAULTS.valueForKey(DCLookupCodes.CommercialSector.rawValue)
var LOOKUP_NATIONALITY = DC_DEFAULTS.valueForKey(DCLookupCodes.Nationality.rawValue)
var LOOKUP_EMIRATES = DC_DEFAULTS.valueForKey(DCLookupCodes.Emirates.rawValue)
var LOOKUP_ORIGIN = DC_DEFAULTS.valueForKey(DCLookupCodes.Origin.rawValue)
var LOOKUP_CITIZEN = DC_DEFAULTS.valueForKey(DCLookupCodes.Citizen.rawValue)
var LOOKUP_CATEGORY = DC_DEFAULTS.valueForKey(DCLookupCodes.Category.rawValue)
var LOOKUP_LOCATION = DC_DEFAULTS.valueForKey(DCLookupCodes.Location.rawValue)
var ConsumerVariables = DCConsumerVariables()
var BusinessVariables = DCBusinessVariables()


enum DCWorkflowType : String
{
    case ConsumerComplaint = "CONSUMER_COMPLAINT_WF"
    case ConsumerInquiry = "CONSUMER_INQUIRY_WF"
    case ConsumerComment = "CONSUMER_COMMENT_WF"
    case BusinessInquiry = "BUSINESS_INQUIRY_WF"
    case BusinessComplaint = "BUSINESS_COMPLAINT_WF"
}

enum DCLookupCodes : String
{
    case Currency = "CM_CRRNCY_Lookup"
    case ComplaintType = "complaint_type"
    case CommercialSector = "CM_CMRC_LGL_TYPE_Lookup"
    case Nationality = "CM_NAT_Lookup"
    case Emirates = "emirate_hc"
    case Origin = "oriCNagin"
    case Citizen = "ctzn_hc"
    case Location = "location"
    case Category = "inquiry_category"    
}

struct DCAPIConstants
{
    static let BaseURL = "http://dedhub.linkdev.com/api/"
    static let GetWorkflow = BaseURL + "workflow/GetWorkFlows" //Post API to get Workflows - BODY
    static let GetWorkflowActions = BaseURL + "workflow/GetStartupStepAction" // Get worfkflow actions . Params : ?workflowId=d44290ac-165a-4d40-aa21-d9a11c890775"
    static let GetComplaint = BaseURL + "request/" //Get API to get complaint Details * ?id=c0741ea0-1b22-43a9-a270-31afdc2c1c7b
    static let GetComplaintCode = BaseURL + "request/" // Get API ?code=CMS-16-412732"
    static let Lookups = BaseURL + "lookups/" // get Lookups. Add lookup code eg CM_CRRNCY_Lookup for currency
    static let SubmitComplaint = BaseURL + "request/" // Post Complaint
    
}


class DCBusinessVariables {
    var complaint_tradeOrigin: String = ""
    var complaint_origin: String = ""
    var complaint_tradeName: String = ""
    var complaint_tradeLicence: String = ""
    var complaint_website: String = ""
    var complaint_email: String = ""
    var complaint_phone: String = ""
    var complaint_mobile: String = ""
    var complaint_emirates: String = ""
    var complaint_poBox: String = ""
    var complaint_address: String = ""
    var complaint_repName: String = ""
    var complaint_repJobTitle: String = ""
    var complaint_repEmail: String = ""
    var complaint_repMobile: String = ""
    var complaint_proxyLetter: String = ""
    
    var defendand_tradeName: String = ""
    var defendand_tradeLicenceNo: String = ""
    var defendand_webSite: String = ""
    var defendand_email: String = ""
    var defendand_phone: String = ""
    var defendand_mobile: String = ""
    var defendand_emirates: String = ""
    var defendand_address: String = ""
    var defendand_poBox: String = ""
    var defendand_repName: String = ""
    var defendand_repJobTitle: String = ""
    var defendand_repEmail: String = ""
    var defendand_repMobile: String = ""
    
    var complaint_CommercialSector: String = ""
    var complaint_Location: String = ""
    var complaint_ComplaintType: String = ""
    var complaint_Subject: String = ""
    var complaint_Details: String = ""
    var complaint_DateOfContract: String = ""
    var complaint_ContractValue: String = ""
    var complaint_DesiredRefund: String = ""
    var complaint_PaymentMethod: String = ""
    
    var Inquiry_Category: String = ""
    var Inquiry_Details: String = ""
}

class DCConsumerVariables {
    var complaint_FullName: String = ""
    var complaint_MobileCode: String = ""
    var complaint_MobileNumber: String = ""
    var complaint_SecondCode: String = ""
    var complaint_SecondNumber: String = ""
    var complaint_Email: String = ""
    var complaint_Nationality: String = ""
    var complaint_Iam: String = ""
    
    var defendand_tradeName: String = ""
    var defendand_tradeLicenceNo: String = ""
    var complaint_Location: String = ""
    var defendand_phone: String = ""
    var defendand_mobile: String = ""
    var defendand_address: String = ""
    var defendand_poBox: String = ""
    var defendand_CommercialSector: String = ""
    var defendand_ComplaintType: String = ""
    var defendand_Subject: String = ""
    var defendand_ComplaintDetails: String = ""
    var defendand_DOP: String = ""
    var defendand_VOP: String = ""
    var defendand_PaymentMethod: String = ""
    
    var Inquiry_Category: String = ""
    var Inquiry_Details: String = ""
    
    var Comment_Type: String = ""
    var Comment_Details: String = ""
}
