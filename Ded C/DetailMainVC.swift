//
//  DetailMainVC.swift
//  Ded C
//
//  Created by iOS Dev 1 on 09/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit
import Sheriff
import MaryPopin

class DetailMainVC: UIViewController,UITextViewDelegate {
  
    
    @IBOutlet var subTitle: UILabel!
    @IBOutlet var mainTitle: UILabel!
    @IBOutlet var rightLogo: UIImageView!
    @IBOutlet var leftLogo: UIImageView!
    
    @IBOutlet var rightDetailCount: UILabel!
    @IBOutlet var cookingBasketLabel: UILabel!
    @IBOutlet var leftDetailCount: UILabel!
    
    @IBOutlet var addToBasketButton: UIButton!
    @IBOutlet var secondRightLabel: UILabel!
    @IBOutlet var firstRightLabel: UILabel!
    @IBOutlet var secondLeftLabel: UILabel!
    @IBOutlet var firstLeftLabel: UILabel!
    @IBOutlet var cartButton: BadgeButton!
    var textRect = CGRectZero
    var langDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.langDict = LanguageClass.shared.languageDict.valueForKey("details") as! NSDictionary
        if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            self.title = self.langDict.valueForKey("details") as? String
            self.firstLeftLabel.text = "ACD29495"
            self.secondLeftLabel.text = "22 September"
            self.firstRightLabel.text = self.langDict.valueForKey("permitNumber") as? String
            self.secondRightLabel.text = self.langDict.valueForKey("validTill") as? String
            self.addToBasketButton.setTitle(self.langDict.valueForKey("addToBasket") as? String, forState: .Normal)
            self.leftDetailCount.hidden = false
            self.rightDetailCount.hidden = true
            self.cookingBasketLabel.textAlignment = .Right
            self.subTitle.textAlignment = .Right
            self.mainTitle.textAlignment = .Right
            self.rightLogo.hidden = false
            self.leftLogo.hidden = true
        }else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
             self.title = self.langDict.valueForKey("promotionDetails") as? String
            self.title = self.langDict.valueForKey("details") as? String
            self.firstLeftLabel.text = self.langDict.valueForKey("permitNumber") as? String
            self.secondLeftLabel.text = self.langDict.valueForKey("validTill") as? String
            self.firstRightLabel.text = "ACD29495"
            self.secondRightLabel.text =  "22 September"
            self.addToBasketButton.setTitle(self.langDict.valueForKey("addToBasket") as? String, forState: .Normal)
            self.leftDetailCount.hidden = true
            self.rightDetailCount.hidden = false
            self.cookingBasketLabel.textAlignment = .Left
            self.subTitle.textAlignment = .Left
            self.mainTitle.textAlignment = .Left
            self.rightLogo.hidden = true
            self.leftLogo.hidden = false
        }
       
        self.cartButton.badge.badgeValue = 1
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
            }
    

    
    // MARK: - Table view data source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellDetails", forIndexPath: indexPath) as! DetailCell
        cell.labelDescription.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
        if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            cell.promotionDetails.textAlignment = .Right
            cell.promotionDetails.text = self.langDict.valueForKey("promotionDetails") as? String
            cell.promotionDetails.text = cell.promotionDetails.text! + ":"
        }
        else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
            cell.promotionDetails.textAlignment = .Left
            cell.promotionDetails.text = self.langDict.valueForKey("promotionDetails") as? String
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
            let problemString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
            textRect =  problemString.boundingRectWithSize(CGSizeMake(tableView.frame.size.width-20, 10000000), options: .UsesLineFragmentOrigin, attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(14)], context: nil)
        return textRect.size.height + 40
    }
    
    @IBAction func addToCart(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Groceries", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("PopViewController") as! PopViewController
        vc.tabType = "Grocery"
        vc.clickOption = "GroceryDetail"
        vc.setPopinTransitionStyle(.Slide)
        vc.setPopinAlignment(.Centered)
        //  vc.setPreferedPopinContentSize(CGSizeMake(self.view.frame.size.width-30,self.view.frame.size.width/2))
        vc.setPreferedPopinContentSize(CGSizeMake(self.view.frame.size.width-30,165))
        vc.setPopinOptions(.DisableParallaxEffect)
        self.presentPopinController(vc, animated:true, completion: nil)

        
    }

}
