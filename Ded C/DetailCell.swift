//
//  DetailCell.swift
//  Ded C
//
//  Created by iOS Dev 1 on 13/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {

    @IBOutlet var promotionDetails: UILabel!
    @IBOutlet var labelDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
