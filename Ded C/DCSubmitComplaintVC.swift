//
//  DCSubmitComplaintVC.swift
//  Ded C
//
//  Created by Focaloid on 8/6/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCSubmitComplaintVC: UIViewController,DCComplaintpopupDelegate
{
    // MARK: - Variables
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var containerScroll: UIScrollView!
    
    let popupConfig = STZPopupViewConfig()
    var lookUpCalled = 0
    var lookUpArray: [String]!

    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        submitButton.setTitle("Submit A Complaint".localized(LANGUAGE!), forState: UIControlState.Normal)
        
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()

        var leftBarButtonItem : UIBarButtonItem!
        leftBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        containerScroll.contentSize = CGSizeMake(0, 290)
//        LANGUAGE = "en"
        DCStoryBoards.sharedStoryBoard.changeLanguage()
        
        lookUpArray = [DCLookupCodes.Currency.rawValue,
                       DCLookupCodes.ComplaintType.rawValue,
                       DCLookupCodes.CommercialSector.rawValue,
                       DCLookupCodes.Nationality.rawValue,
                       DCLookupCodes.Emirates.rawValue,
                       DCLookupCodes.Origin.rawValue,
                       DCLookupCodes.Citizen.rawValue,
                       DCLookupCodes.Category.rawValue,
                       DCLookupCodes.Location.rawValue]
        
        if DC_DEFAULTS.valueForKey("lookUpCalled") != nil {
            LOOKUP_CURRENCY = fetchLookUpArrays(lookUpArray[0])
            LOOKUP_COMPLAINT_TYPE = fetchLookUpArrays(lookUpArray[1])
            LOOKUP_COMMERCIAL_SECTOR = fetchLookUpArrays(lookUpArray[2])
            LOOKUP_NATIONALITY = fetchLookUpArrays(lookUpArray[3])
            LOOKUP_EMIRATES = fetchLookUpArrays(lookUpArray[4])
            LOOKUP_ORIGIN = fetchLookUpArrays(lookUpArray[5])
            LOOKUP_CITIZEN = fetchLookUpArrays(lookUpArray[6])
            LOOKUP_CATEGORY = fetchLookUpArrays(lookUpArray[7])
            LOOKUP_LOCATION = fetchLookUpArrays(lookUpArray[8])
        }
        
        callLookUp()
        
//        getComplaints(DCWorkflowType.ConsumerComplaint.rawValue)
        
        submitButton.setTitle("FILE A CONSUMER COMPLAINT".localized(LANGUAGE!), forState: UIControlState.Normal)
        
        // Do any additional setup after loading the view.
    }
    func callLookUp() {
        if lookUpCalled != lookUpArray.count {
            postLookUp(lookUpArray[lookUpCalled])
        }else {
            DC_DEFAULTS.setValue(true, forKey: "lookUpCalled")
            LOOKUP_CURRENCY = fetchLookUpArrays(lookUpArray[0])
            LOOKUP_COMPLAINT_TYPE = fetchLookUpArrays(lookUpArray[1])
            LOOKUP_COMMERCIAL_SECTOR = fetchLookUpArrays(lookUpArray[2])
            LOOKUP_NATIONALITY = fetchLookUpArrays(lookUpArray[3])
            LOOKUP_EMIRATES = fetchLookUpArrays(lookUpArray[4])
            LOOKUP_ORIGIN = fetchLookUpArrays(lookUpArray[5])
            LOOKUP_CITIZEN = fetchLookUpArrays(lookUpArray[6])
            LOOKUP_CATEGORY = fetchLookUpArrays(lookUpArray[7])
            LOOKUP_LOCATION = fetchLookUpArrays(lookUpArray[8])
        }
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        let _ = DCStoryBoards.sharedStoryBoard
        self.tabBarController?.tabBar.userInteractionEnabled = false


    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func englishAction(sender: AnyObject)
    {
        
        

    }
    
    @IBAction func arabicAction(sender: AnyObject)
    {

        if DC_DEFAULTS.valueForKey("language") as! String != "ar"
        {
            DC_DEFAULTS.setValue("ar", forKey: "language")
 
            DCStoryBoards.sharedStoryBoard.changeLanguage()
        }
        LanguageClass.shared.loadTranslation()
        
        
        submitButton.setTitle("Submit A Complaint".localized(LANGUAGE!), forState: UIControlState.Normal)
        
    }
    
    @IBAction func submitAction(sender: AnyObject)
    {
        popupConfig.dismissTouchBackground = true
        popupConfig.cornerRadius = 10
        popupConfig.showAnimation = STZPopupShowAnimation.SlideInFromBottom
        popupConfig.dismissAnimation = STZPopupDismissAnimation.SlideOutToBottom
        popupConfig.dismissCompletion = nil
        var index: Int!
        if LANGUAGE == "en" {
            index = 0
        }else {
            index = 1
        }
        
        let popupView = NSBundle.mainBundle().loadNibNamed("DCComplaintPopupView", owner: nil, options: nil)[index] as! DCComplaintPopupView
        popupView.popupDelegate = self
        if iPad {
            popupView.frame = CGRect(x: 0, y: 0, width: 450, height: 270)
        }else {
            popupView.frame = CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.size.width - 30, height: 240)
        }
        
        self.presentPopupView(popupView, config: popupConfig)
    }
    
    // MARK: - WebServices
    func getComplaints(param: String) {
        APPDELEGATE?.showActivity(self.view, myTitle: "Loading..")
        DC_NETWORK.getJson(DCAPIConstants.BaseURL + "workflow/?key=" + param, method: "GET", params: [:]) { (succeeded, json) in
            dispatch_async(dispatch_get_main_queue(), {
                APPDELEGATE?.removeActivity(self.view)
                print(json["Data"])
                if let data = json["Data"] as? NSArray {
                    print(data)
                    print(param)
                    
                }
                
                //                let fetch = DC_DEFAULTS.objectForKey(rawValue)
                //                let myData = NSKeyedUnarchiver.unarchiveObjectWithData(fetch as! NSData)
                //                print(myData)
                
            })
        }
    }
    func postLookUp(rawValue: String) {
        if (DC_DEFAULTS.valueForKey("lookUpCalled") == nil) {
            APPDELEGATE?.showActivity(self.view, myTitle: "Loading..")
        }
        DC_NETWORK.getJson(DCAPIConstants.Lookups + rawValue, method: "GET", params: [:]) { (succeeded, json) in
            dispatch_async(dispatch_get_main_queue(), {
                APPDELEGATE?.removeActivity(self.view)
//                print(json["Data"])
                if let data = json["Data"] as? NSArray {
                    print(data)
                    print(rawValue)
                    let rawData: NSData = NSKeyedArchiver.archivedDataWithRootObject(data)
                    DC_DEFAULTS.setObject(rawData, forKey: rawValue)
                }
                
//                let fetch = DC_DEFAULTS.objectForKey(rawValue)
//                let myData = NSKeyedUnarchiver.unarchiveObjectWithData(fetch as! NSData)
//                print(myData)
                
                
                if self.lookUpCalled != self.lookUpArray.count {
                    self.lookUpCalled += 1
                    self.callLookUp()
                }
            })
        }
    }
    
    func fetchLookUpArrays(key: String) -> NSArray {
        var resultArray: NSArray = []
        if let fetch = DC_DEFAULTS.objectForKey(key) {
            resultArray = NSKeyedUnarchiver.unarchiveObjectWithData(fetch as! NSData) as! NSArray
            print(resultArray)
        }
        return resultArray
    }
    //MARK: PupupDelegate methods
    
    func popupDidSelectConsumerComplaint()
    {
        let consumerComplaintVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.ConsumerComplaintVC)
        popupConfig.dismissCompletion = {(UIView) in
            
            self.navigationController?.pushViewController(consumerComplaintVC, animated: true)
            
        }
        self.dismissPopupView()
        
        

    }
    
    func popupDidSelectBusinessComplaint()
    {
        let businessComplaintVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.BusinessCompalaintVC)
        popupConfig.dismissCompletion = {(UIView) in
            
                                            self.navigationController?.pushViewController(businessComplaintVC, animated: true)
            
                                        }
        self.dismissPopupView()
    }
    
    
    
    func popupDidSelectTrackComplaint()
    {
        let trackComplaintVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.ComplaintTrackVC)
        popupConfig.dismissCompletion =
            {(UIView) in
            
            self.navigationController?.pushViewController(trackComplaintVC, animated: true)
            
        }
        self.dismissPopupView()

        
        
    }
    @IBAction func goToGroceries(sender: AnyObject) {
       
        let VC : UIViewController = UIStoryboard.init(name: "Groceries", bundle: nil).instantiateViewControllerWithIdentifier("GroceriesMainVC") as! UIViewController
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
