//
//  DCAddCommentVC.swift
//  Ded C
//
//  Created by Focaloid on 8/8/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit


protocol DCAddCommentCellDelegate : NSObjectProtocol
{
    func commentCellDeleteCell(cell:DCommentImageCollectionCell)
    func commentCellDidPressAddButton()
}

protocol  DCAddCommentDelegate : NSObjectProtocol
{
    func commentVCDidAddComments(attachmentObject : DCAttachmentObject,type:String)
    func commentVCDidEditComments(attachmentObject : DCAttachmentObject,indexPath : NSIndexPath?,type:String)
    
}

class DCAddCommentVC: UIViewController,UITextViewDelegate,DCAddCommentCellDelegate,UICollectionViewDelegate,UICollectionViewDataSource
{
    @IBOutlet var commentTextView: UITextView!
    var images : [UIImage] = []
    
    var editMode : Bool = false
    
    let maxImages = 5
    
    var placeholderText = "Add your comment here".localized(LANGUAGE!)
    var comment : String = ""
    
    var commentDelegate : DCAddCommentDelegate?
    var indexPath : NSIndexPath?
    
    var type : String = ""
    
    @IBOutlet var imagesCollectionView: UICollectionView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        addTapGesture()
        
        if comment != ""
        {
            commentTextView.text = comment
        }
        
        self.imagesCollectionView.showsVerticalScrollIndicator = false

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        imagesCollectionView.reloadData()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func closeAction(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func addCommentAction(sender: AnyObject)
    {
        let attachmentObject = DCAttachmentObject()
        attachmentObject.images = self.images
        
        if self.commentTextView.text != placeholderText
        {
            attachmentObject.comment = self.commentTextView.text
        }
        
        if self.images.count == 0
        {
            return
        }
        
        if editMode == true
        {
            self.commentDelegate?.commentVCDidEditComments(attachmentObject, indexPath: indexPath,type: type)
        }
        else
        {
            self.commentDelegate?.commentVCDidAddComments(attachmentObject,type: type)

        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    //MARK: UICCollectionView Delgate and DataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return images.count >= maxImages  ? images.count : images.count + 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        var cell : DCommentImageCollectionCell

        
        if indexPath.row < images.count
        {
            cell = self.imagesCollectionView.dequeueReusableCellWithReuseIdentifier("imageCell", forIndexPath: indexPath) as! DCommentImageCollectionCell
            cell.commentImageView.image = images[indexPath.row]
        }
        else
        {
            cell = self.imagesCollectionView.dequeueReusableCellWithReuseIdentifier("addCell", forIndexPath: indexPath) as! DCommentImageCollectionCell

        }
        cell.commentDelegate = self
        
        return cell
    }
    
    //MARK Comment Cell Delegate 
    
    func commentCellDeleteCell(cell:DCommentImageCollectionCell)
    {
        if let indexPath = self.imagesCollectionView.indexPathForCell(cell)
        {
            self.images.removeAtIndex(indexPath.row)
            self.imagesCollectionView.reloadData()
//            self.imagesCollectionView.deleteItemsAtIndexPaths([indexPath])
        }
    }
    
    func commentCellDidPressAddButton()
    {
        self.openImagesActionSheet
            { (image) in
                
                if let pickedImage = image
                {
                    let indexPath = NSIndexPath(forItem: self.images.count, inSection: 0)
                    self.images.append(pickedImage)
                    self.imagesCollectionView.reloadData()
//                    self.imagesCollectionView.insertItemsAtIndexPaths([indexPath])
                }
                
        }
    }
    
    //MARK : TextViewDelegate Methods
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == placeholderText || textView.text == ""
        {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = placeholderText
        }
    }
   
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if textView.text == placeholderText
        {
            textView.textColor = UIColor.blackColor()
            textView.text = ""
        }
        return true
    }

}

class DCommentImageCollectionCell: UICollectionViewCell
{
    
    @IBOutlet var commentImageView: UIImageView!
    weak var commentDelegate : DCAddCommentCellDelegate?
    
    
    @IBAction func closeAction(sender: AnyObject)
    {
        commentDelegate?.commentCellDeleteCell(self)
    }
    
    @IBAction func addAction(sender: AnyObject)
    {
        commentDelegate?.commentCellDidPressAddButton()
    }
    
}