//
//  DCTab.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 03/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCTab: UITabBarController {

    //MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let tabBar = self.tabBar
    
         let viewcontroller = UIStoryboard.init(name: "Groceries", bundle: nil).instantiateViewControllerWithIdentifier("DefaultShopppingCart") as UIViewController
        self.viewControllers?.insert(viewcontroller, atIndex: 4)

//        self..tabBar.items objectAtIndex:3] setImage:cartTabImage];
//        self.viewControllers?.append(viewcontroller)
                       let home: UIImage! = UIImage(named: "Home_sel")?.imageWithRenderingMode(.AlwaysOriginal)
        let profile: UIImage! = UIImage(named: "Profile_sel")?.imageWithRenderingMode(.AlwaysOriginal)
        let qr: UIImage! = UIImage(named: "qr_sel")?.imageWithRenderingMode(.AlwaysOriginal)
        let call: UIImage! = UIImage(named: "Call_Sel")?.imageWithRenderingMode(.AlwaysOriginal)
        let cart: UIImage! = UIImage(named: "Cart_sel")?.imageWithRenderingMode(.AlwaysOriginal)
        let cart1: UIImage! = UIImage(named: "Cart")?.imageWithRenderingMode(.AlwaysOriginal)

        
        (tabBar.items![4].image = cart1)
        (tabBar.items![0]).selectedImage = home
        (tabBar.items![1]).selectedImage = profile
        (tabBar.items![2]).selectedImage = qr
        (tabBar.items![3]).selectedImage = call
        (tabBar.items![4]).selectedImage = cart
        
        tabBar.tintColor = UIColor.clearColor()
        // Do any additional setup after loading the view.
    }

//    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
//        if tabBar.items?.indexOf(item) == 4 {
//            let composeNavigation = self.viewControllers![0] as! UINavigationController
//            
//           
//         
//        }
//    }

    //MARK:- System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
