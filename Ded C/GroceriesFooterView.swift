//
//  GroceriesFooterView.swift
//  Ded C
//
//  Created by iOS Dev 1 on 08/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class GroceriesFooterView: UICollectionReusableView {
        
    @IBOutlet var englishViewAll: UIButton!
    @IBOutlet var arabicViewAll: UIButton!
}
