//
//  DCComplaintTrack.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 04/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCComplaintTrack: UIViewController
{

    @IBOutlet weak var SearchBar: UISearchBar!
    var complaintTracker: DCComplaintTrackTable!
    var searchResults: NSDictionary!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addTapGesture()

//        complaintTracker = self.childViewControllers[0] as! DCComplaintTrackTable
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        self.view.endEditing(true) 
        searchBar.resignFirstResponder()
        return true
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchBar.resignFirstResponder()
        
        postTracking(DC_DELEGATE.TrimString(searchBar.text!))
    }

    func postTracking(params: String) {
        APPDELEGATE?.showActivity(self.view, myTitle: "Searching..")
        DC_NETWORK.getJson(DCAPIConstants.GetComplaintCode + "?code=" + params, method: "GET", params: [:]) { (succeeded, json) in
            dispatch_async(dispatch_get_main_queue(), {
                APPDELEGATE?.removeActivity(self.view)
                print(json["Data"])
                if let data = json["Data"] as? NSDictionary {
                    print(data)
                    self.searchResults = data
                    self.complaintTracker.searchResults = self.searchResults
                    self.complaintTracker.viewDidLoad()
                    self.complaintTracker.tableView.reloadData()
                }
                
                //                let fetch = DC_DEFAULTS.objectForKey(rawValue)
                //                let myData = NSKeyedUnarchiver.unarchiveObjectWithData(fetch as! NSData)
                //                print(myData)
                
            })
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "tracking" {
            let trackingView = segue.destinationViewController as! DCComplaintTrackTable
            complaintTracker = trackingView
        }
    }
    

}
