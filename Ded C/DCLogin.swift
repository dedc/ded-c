//
//  DCLogin.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 27/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCLogin: UIViewController, loginDelegate {

    // MARK: - Variables
    @IBOutlet weak var EnglishContainer: UIView!
    @IBOutlet weak var ArabicContainer: UIView!
    var loginController: DCLoginTable?
    var loginController1: DCLoginTable?
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        if LANGUAGE == "en" {
            ArabicContainer.hidden = true
            EnglishContainer.hidden = false
        }else {
            EnglishContainer.hidden = true
            ArabicContainer.hidden = false
        }
        // Do any additional setup after loading the view.
    }

    // MARK: - Actions
    func didClickedLogin(isLoggeIn: Bool) {
        performSegueWithIdentifier("GoTomainMenu", sender: self)
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "GotoLogin" {
            loginController = segue.destinationViewController as? DCLoginTable
            loginController?.delegate = self
        }else {
            loginController1 = segue.destinationViewController as? DCLoginTable
            loginController1?.delegate = self
        }
    }
 
}
