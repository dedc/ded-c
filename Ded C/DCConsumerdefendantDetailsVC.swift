//
//  DCConsumerdefendantDetailsVC.swift
//  Ded C
//
//  Created by Focaloid on 8/5/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCConsumerdefendantDetailsVC: UITableViewController
{

    // MARK: - Variables
    @IBOutlet weak var tradeName: DCImagedTextField!
    @IBOutlet weak var TradeLicence: UITextField!
    @IBOutlet weak var Area: UITextField!
    @IBOutlet weak var PhoneNumber: DCImagedTextField!
    @IBOutlet weak var Mobile: DCImagedTextField!
    @IBOutlet weak var Address: UITextView!
    @IBOutlet weak var POBox: UITextField!
    
    // MARK: - View Methods
    var placeholderText = "Address".localized(LANGUAGE!)
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    // MARK: - TableView Delegates
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if iPad {
            switch indexPath.row {
            case 2:
                return 75
            case 4:
                return 81
            case 6:
                return 90
            default:
                return 44
            }
        }else {
            switch indexPath.row {
            case 0:
                return 50
            case 2:
                return 85
            case 3:
                return 75
            case 6:
                return 67
            case 8:
                return 65
            case 9:
                return 55
            default:
                return 44
            }
        }
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if textView.text == placeholderText
        {
            textView.textColor = UIColor.blackColor()
            textView.text = ""
        }
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == placeholderText || textView.text == ""
        {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = placeholderText
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func complaintDetailsAction(sender: AnyObject)
    {
        let complaintDetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.ConsumerComplaintDetailsVC)
        self.navigationController?.pushViewController(complaintDetailsVC!, animated: true)
    }
    // MARK: - Table view data source

 
}
