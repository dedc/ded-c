//
//  CartViewController.swift
//  Ded C
//
//  Created by iOS Dev 1 on 12/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {

    var mainHeaderArray = NSArray()
    var subHeaderArray:NSArray = NSArray()
    var rowHeight = CGFloat()
    var imageArray:NSArray = NSArray()
    
    @IBOutlet var orderButton: UIButton!
    @IBOutlet var mainTableView: UITableView!
    var langDict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.langDict = LanguageClass.shared.languageDict.valueForKey("defaultShoppingCart") as! NSDictionary
          self.title = self.langDict.valueForKey("defaultShoppingCart") as? String
        if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            self.orderButton.setTitle(self.langDict.valueForKey("order") as? String, forState: .Normal)
        }
        else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
            self.orderButton.setTitle(self.langDict.valueForKey("order") as? String, forState: .Normal)
        }

        mainHeaderArray = ["Meat and Poultry","Milk and Eggs","Fruits and Nuts"]
        
       // subHeaderArray = ["Chicken","Beef"]
        
        imageArray = ["meat.jpg","milk.jpg","fruits.jpg"]
        
        mainTableView.tableFooterView = UIView(frame: CGRect.zero)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return mainHeaderArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let  cell : CartCell!
        if DC_DEFAULTS.valueForKey("language") as? String == "ar" {
            cell = tableView.dequeueReusableCellWithIdentifier("ArabicMainCell")! as! CartCell
            
            if indexPath.section == 0 {  // meat
                cell.count = 2 // sub section count
                cell.type = "Meat and Poultry"
            }
            else if indexPath.section == 1{ // milk
                cell.count = 1
                cell.type = "Milk and Eggs"
            }
            else{ // fruits
                cell.count = 2
                cell.type = "Fruits and Nuts"
            }

        }
        else{
            cell = tableView.dequeueReusableCellWithIdentifier("EnglishMainCell")! as! CartCell
            
            if indexPath.section == 0 {  // meat
                cell.count = 2 // sub section count
                cell.type = "Meat and Poultry"
            }
            else if indexPath.section == 1{ // milk
                cell.count = 1
                cell.type = "Milk and Eggs"
            }
            else{ // fruits
                cell.count = 2
                cell.type = "Fruits and Nuts"
            }

        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        if indexPath.section == 0{
            return (2 + 3) * 34 // subsection count + total no of rows in all sections * height of a single row
        }
        else if indexPath.section == 1{
            return (1 + 2) * 33
        }
            
        else{
            return (2 + 4) * 29
        }
    }
    
    
    func tableView(tableView: UITableView,viewForHeaderInSection section: Int) -> UIView?{
        let  cell : UIView!
        if DC_DEFAULTS.valueForKey("language") as? String == "ar" {
            cell = tableView.dequeueReusableCellWithIdentifier("ArabicMainHeader")! as UIView
            let titleLabel:UILabel = cell.viewWithTag(500) as! UILabel
            titleLabel.text = mainHeaderArray.objectAtIndex(section) as? String
            
            let itemImage:UIImageView = cell.viewWithTag(800) as! UIImageView
            itemImage.image = UIImage.init(named: (imageArray.objectAtIndex(section) as? String)!)

        }
        else{
            cell = tableView.dequeueReusableCellWithIdentifier("EnglishMainHeader")! as UIView
            let titleLabel:UILabel = cell.viewWithTag(500) as! UILabel
            titleLabel.text = mainHeaderArray.objectAtIndex(section) as? String
            
            let itemImage:UIImageView = cell.viewWithTag(800) as! UIImageView
            itemImage.image = UIImage.init(named: (imageArray.objectAtIndex(section) as? String)!)
        }
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 60
    }
    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    @IBAction func orderBtnClk(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Groceries", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("PopViewController") as! PopViewController
        vc.tabType = "Cart"
        vc.setPopinTransitionStyle(.Slide)
        vc.setPopinAlignment(.Centered)
        //  vc.setPreferedPopinContentSize(CGSizeMake(self.view.frame.size.width-30,self.view.frame.size.width/2))
        vc.setPreferedPopinContentSize(CGSizeMake(self.view.frame.size.width-30,165))
        vc.setPopinOptions(.DisableParallaxEffect)
        self.presentPopinController(vc, animated:true, completion: nil)

    }
    
    @IBAction func editProductBtnClk(sender: AnyObject) {
        let editProductsVC = self.storyboard?.instantiateViewControllerWithIdentifier("EditViewController") as! EditViewController
        editProductsVC.fromWhichView = "CartTab"
        self.navigationController?.pushViewController(editProductsVC, animated: true)
    }

}
