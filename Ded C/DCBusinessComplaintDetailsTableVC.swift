//
//  DCBusinessComplaintDetailsTableVC.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 04/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

@objc protocol showAlertDelegate
{
    optional func submitClicked(isClicked: Bool)
}

class DCBusinessComplaintDetailsTableVC: UITableViewController,DCAddCommentDelegate,DCMultipleAttachmentCellDelegate, UIPopoverPresentationControllerDelegate, datePickerDelegate
{

    @IBOutlet weak var termsCell: UITableViewCell!
    @IBOutlet var acceptTermsImageView: UIImageView!
    // MARK: - Variables
    var delegate: showAlertDelegate?
    
    @IBOutlet weak var dateOfContract: UILabel!
    @IBOutlet var commercialSectorLabel: UILabel!
    
    @IBOutlet var locationLabel: UILabel!
    
    @IBOutlet var complaintTypeLabel: UILabel!
    
    @IBOutlet var currencylabel: UILabel!
    @IBOutlet weak var Subject: DCImagedTextField!
    @IBOutlet weak var ComplaintDetails: UITextView!
    @IBOutlet weak var DesiredRefund: DCImagedTextField!
    @IBOutlet weak var PaymentMethod: DCImagedTextField!
    
    var acceptedterms : Bool = false
    let popupConfig = STZPopupViewConfig()
    
    @IBOutlet var attachmentstableView: DCAttachmentTableView!
    
    // MARK: - View Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        if iPad {
            termsCell.backgroundColor = UIColor.clearColor()
        }
        
        self.attachmentstableView.attachmentCellDelegate = self
        
        self.tableView.showsVerticalScrollIndicator = false
        
        acceptedterms = false
        acceptTermsImageView.image = UIImage(named: "uncheck")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    // MARK: - Actions
    @IBAction func SubmitBusinessComplaint(sender: AnyObject) {
        
        
        let emptyArray : NSArray = [] as! NSArray
        
        let RequestDetails = ["RequestDetails":
            ["ParticipantDetails":
                [
                    "TradeOrigin": BusinessVariables.complaint_tradeOrigin,
                    "TradeLicenseNumber": BusinessVariables.complaint_tradeLicence,
                    "TradeLicenseURL": BusinessVariables.complaint_tradeLicence,
                    "TradeLicenseType": 1,
                    "TradeLicenseStatus": 3,
                    "RepresentativeName": BusinessVariables.complaint_repName,
                    "RepresentativeJobTitle": BusinessVariables.complaint_repJobTitle,
                    "RepresentativeEmail": BusinessVariables.complaint_email,
                    "RepresentativeMobile": BusinessVariables.complaint_mobile,
                    "RepresentativeProxyLetter": BusinessVariables.complaint_proxyLetter,
                    "Origin": BusinessVariables.complaint_origin,
                    "ID": "",
                    "MobileNumber": BusinessVariables.complaint_repMobile,
                    "NationalityID": 0,
                    "Email": BusinessVariables.complaint_repEmail,
                    "Type": "BUSINESS",
                    "NameEN": BusinessVariables.complaint_repName,
                    "NameAR": "tradeArabicSecond",
                    "SecondMobileNumber": BusinessVariables.defendand_mobile,
                    "Website": BusinessVariables.complaint_website,
                    "PhoneNumber": BusinessVariables.complaint_phone,
                    "EmirateID": 0,
                    "Address": BusinessVariables.complaint_address,
                    "PO_Box": BusinessVariables.complaint_poBox,
                    "CallingSystem": 0,
                    "PreferedLanguage": 0,
                    "ResidencyID": 0,
                    "AccountID": "",
                    "CreatedDate": "2016-09-28T10:00:00",
                    "ModifiedDate": "2016-09-28T10:00:00",
                    "CreatedBy": "N/A",
                    "ModifiedBy": "N/A",
                    "AdditionalInfo": ""
                ],
                "ComplaintSource": "2",
                "CommercialSector": BusinessVariables.complaint_CommercialSector,
                "Type": 3,
                "Subject": BusinessVariables.complaint_Subject,
                "ComplaintDetails": BusinessVariables.complaint_Details,
                "DateOfContract": "2016-09-28T21:00:00.000Z",
                "ContractValue": 123,
                "Currency": 49,
                "DesireRefundAmount": 123,
                "PaymentMethod": "Visa",
                "TradeLicenseNumber": 3
            ],
                              "Comments": emptyArray,
                              "Attachments": emptyArray,
                              "WorkflowRequest": [
                                "Workflow": ["Id": WORKFLOW_ID!],
                                "Action": WORKFLOW_ACTION
            ]
        ]
        print(RequestDetails)
        
        
        DC_NETWORK.postJson(DCAPIConstants.BaseURL + DCAPIConstants.SubmitComplaint, method: "POST", params: RequestDetails, is_Dictionary: true) { (succeeded, json) in
            dispatch_async(dispatch_get_main_queue(), {
                print(json)
                self.delegate?.submitClicked!(true)
            })
        }
        
    }
    
    func passDate(date: NSDate) {
        dateOfContract.text = DC_DELEGATE.DateToString(date, format: "dd/MM/yyyy")
    }
    @IBAction func showDatePicker(sender: UIButton) {
        let menuViewController: DCDatePicker = DCStoryBoards().mainStoryboard.instantiateViewControllerWithIdentifier("DCDatePicker") as! DCDatePicker
        menuViewController.delegate = self
        menuViewController.modalPresentationStyle = .Popover
        menuViewController.preferredContentSize = CGSizeMake(320, 260)
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.delegate = self
        if iPad {
            popoverMenuViewController?.permittedArrowDirections = .Left
            popoverMenuViewController?.sourceView = sender as? UIView
            popoverMenuViewController?.sourceRect = CGRect(
                x: sender.frame.width + 10,
                y: sender.frame.height / 2,
                width: 1,
                height: 1)
        }else {
            popoverMenuViewController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            popoverMenuViewController?.sourceView = self.view
            popoverMenuViewController?.sourceRect = CGRect(
                x: self.view.frame.width / 2,
                y: self.view.frame.height / 2,
                width: 1,
                height: 1)
        }
        
        presentViewController(
            menuViewController,
            animated: true,
            completion: nil)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if iPhone {
            switch indexPath.row {
            case 5:
                return 80
            case 10:
                return 50 + CGFloat(attachmentstableView.attachmentsArray.count * 140) // Attachment
            case 11:
                return 60
            case 12:
                return 35
            default:
                return 44
            }
        }else {
            switch indexPath.row {
            case 3:
                return 80
            case 5:
                return 58
            case 6:
                return 50 + CGFloat(attachmentstableView.attachmentsArray.count * 140) // Attachment
            case 7:
                return 60
            case 8:
                return 35
            default:
                return 44
            }
        }
    }
    @IBAction func TermsNConditions(sender: AnyObject)
    {
        let TermsNConditionsVC = self.storyboard?.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.TermsNConditionsVC)
        self.presentViewController(TermsNConditionsVC!, animated: true, completion: nil)
    }
    
    @IBAction func commercialSectorAction(sender: AnyObject)
    {
//        print(LOOKUP_COMMERCIAL_SECTOR)
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var commercialSectorArray: [String] = []
        
        if LOOKUP_COMMERCIAL_SECTOR?.count != 0 {
            for i in 0..<LOOKUP_COMMERCIAL_SECTOR!.count {
                commercialSectorArray.append(LOOKUP_COMMERCIAL_SECTOR![i][fetchKey] as! String)
            }
        }
        
//        print(commercialSectorArray)
        self.showSelectionTableView("Commercial Sector", options: commercialSectorArray)
        { (selectedString, index) in
            
            self.commercialSectorLabel.text = selectedString
            
        }
    }

  
    @IBAction func locationAction(sender: AnyObject)
    {
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var locationArray: [String] = []
        
        if LOOKUP_COMMERCIAL_SECTOR?.count != 0 {
            for i in 0..<LOOKUP_COMMERCIAL_SECTOR!.count {
                locationArray.append(LOOKUP_COMMERCIAL_SECTOR![i][fetchKey] as! String)
            }
        }
        
        self.showSelectionTableView("Location", options: locationArray)
        { (selectedString, index) in
            
            self.locationLabel.text = selectedString
            
        }
    }
   
    @IBAction func currencyTypeAction(sender: AnyObject)
    {
//        print(LOOKUP_CURRENCY)
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var currencyArray: [String] = []
        
        if LOOKUP_CURRENCY?.count != 0 {
            for i in 0..<LOOKUP_CURRENCY!.count {
                currencyArray.append(LOOKUP_CURRENCY![i][fetchKey] as! String)
            }
        }
        self.showSelectionTableView("Currency", options: currencyArray)
        { (selectedString, index) in
            
            self.currencylabel.text = selectedString
            
        }
    }


    
    @IBAction func complaintTypeAction(sender: AnyObject)
    {
//        print(LOOKUP_COMPLAINT_TYPE)
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var complaintTypeArray: [String] = []
        
        if LOOKUP_COMPLAINT_TYPE?.count != 0 {
            for i in 0..<LOOKUP_COMPLAINT_TYPE!.count {
                complaintTypeArray.append(LOOKUP_COMPLAINT_TYPE![i][fetchKey] as! String)
            }
        }
        
        self.showSelectionTableView("Complaint Type", options: complaintTypeArray)
        { (selectedString, index) in
            
            self.complaintTypeLabel.text = selectedString
            
        }
    }
    

    @IBAction func acceptTermsAction(sender: AnyObject)
    {
        
//        acceptedterms = !acceptedterms

        
        if acceptedterms == true
        {
            acceptTermsImageView.image = UIImage(named: "uncheck")
            acceptedterms = false
        }
        else
        {
            acceptTermsImageView.image = UIImage(named: "Check")
            acceptedterms = true
        }
        
    }
    
    
    @IBAction func Submit(sender: AnyObject)
    {
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func attachmentsAction(sender: AnyObject)
    {
        if iPad {
            popupConfig.dismissTouchBackground = true
            popupConfig.showAnimation = STZPopupShowAnimation.SlideInFromBottom
            popupConfig.dismissAnimation = STZPopupDismissAnimation.SlideOutToBottom
            popupConfig.dismissCompletion = nil
            var index: Int!
            if LANGUAGE == "en" {
                index = 0
            }else {
                index = 1
            }
            
            let commentVC = DCStoryBoards().complaintStoryboard.instantiateViewControllerWithIdentifier("addCommentVC") as? DCAddCommentVC
            let popupView = commentVC?.view
            popupView!.frame = CGRect(x: 0, y: 0, width: 600, height: 450)
            popupView?.clipsToBounds = true
            self.presentPopupView(popupView!, config: popupConfig)
        }else {
            let commentVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.AddCommentVC) as! DCAddCommentVC
            commentVC.type = "trade"
            commentVC.commentDelegate = self
            self.presentViewController(commentVC, animated: true, completion: nil)
        }
        

    }

    

    
    //MARK: Multiple Attachment Delegates
    
    func attachmentCellDidPressMore(cell: DCMultipleAttachmentCell)
    {
        
        let alertController = UIAlertController()
        
        let editAction = UIAlertAction(title: "Edit", style: .Default)
        { (action) in
            
            
            let commentVC = DCStoryBoards.sharedStoryBoard.complaintStoryboard.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.AddCommentVC) as! DCAddCommentVC
            commentVC.commentDelegate = self
            commentVC.images = cell.images
            commentVC.comment = cell.comment
            commentVC.editMode = true
            if let indexPath = self.attachmentstableView.indexPathForCell(cell)
            {
                commentVC.indexPath = indexPath
                
            }
          
            self.presentViewController(commentVC, animated: true, completion: nil)
            
        }
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Default)
        { (action) in
            
            
            if let indexPath = self.attachmentstableView.indexPathForCell(cell)
            {
                
                self.attachmentstableView.attachmentsArray.removeAtIndex(indexPath.row)
                self.attachmentstableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
                
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel)
        { (action) in
            
            alertController.dismissViewControllerAnimated(true
                , completion:nil)
        }
        
        alertController.addAction(editAction)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    //MARK: Add Attchment s Delegate
    
    func commentVCDidAddComments(attachmentObject: DCAttachmentObject,type:String)
    {

            self.attachmentstableView.attachmentsArray.append(attachmentObject)
            self.attachmentstableView.reloadData()
            self.tableView.reloadData()
        
    }
    
    func commentVCDidEditComments(attachmentObject : DCAttachmentObject,indexPath : NSIndexPath?,type:String)
    {
        
        if let indexPath = indexPath
        {
            self.attachmentstableView.attachmentsArray[indexPath.row] = attachmentObject
            self.attachmentstableView.reloadData()
            self.tableView.reloadData()
        }


    }

}

