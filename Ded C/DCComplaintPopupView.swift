//
//  DCComplaintPopupView.swift
//  Ded C
//
//  Created by Focaloid on 8/6/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

protocol DCComplaintpopupDelegate : NSObjectProtocol
{
    func popupDidSelectConsumerComplaint()
    func popupDidSelectBusinessComplaint()
    func popupDidSelectTrackComplaint()
    
}


class DCComplaintPopupView: UIView
{

    weak var popupDelegate : DCComplaintpopupDelegate?
    
    @IBAction func consumerComplaintAction(sender: AnyObject)
    {
        popupDelegate?.popupDidSelectConsumerComplaint()
    }
    
    @IBAction func businessComplaintAction(sender: AnyObject)
    {
        popupDelegate?.popupDidSelectBusinessComplaint()
    }
    
    @IBAction func trackComplaintAction(sender: AnyObject)
    {
        popupDelegate?.popupDidSelectTrackComplaint()
    }
    
}
