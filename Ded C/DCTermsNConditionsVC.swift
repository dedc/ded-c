//
//  DCTermsNConditionsVC.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 05/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCTermsNConditionsVC: UIViewController {

    // MARK: - Variables
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var CloseButton: UIButton!
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "Terms and Conditions".localized(LANGUAGE!)
        CloseButton.setTitle("Close".localized(LANGUAGE!), forState: UIControlState.Normal)
        // Do any additional setup after loading the view.
    }

    // MARK: - System
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func closeAction(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
