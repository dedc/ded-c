//
//  DetailViewCell.swift
//  Ded C
//
//  Created by iOS Dev 1 on 09/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DetailViewCell: UICollectionViewCell {
    
    @IBOutlet var detailTextView: UITextView!
}
