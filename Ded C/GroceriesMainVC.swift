//
//  GroceriesMainVC.swift
//  Ded C
//
//  Created by Abdur Rehman on 07/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit
import Sheriff

class GroceriesMainVC: UIViewController {
    
    var headerView: GroceriesHeaderView!
    
   
    @IBOutlet var shartShoppingButton: UIButton!
    @IBOutlet var startShoppingView: UIView!
    
    @IBOutlet var budgetLabel: UILabel!
    @IBOutlet var expenseLabel: UILabel!
    @IBOutlet var yourBudgetLabel: UILabel!
    @IBOutlet var cartButton: BadgeButton!
    var langDict = NSDictionary()
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        self.cartButton.setImage(UIImage(named:"Ic_cart-outline"), forState: .Normal)
        self.cartButton.badge.backgroundColor = UIColor.appThemeYellowColor()
        self.langDict = LanguageClass.shared.languageDict.valueForKey("buyYourGroceries") as! NSDictionary

        print((LanguageClass.shared.languageDict.valueForKey("buyYourGroceries")?.valueForKey("budget")))
            if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
                let viewTitle = self.langDict.valueForKey("buyYourGroceries") as! String
                self.title = viewTitle
               
                let budget = self.langDict.valueForKey("budget") as! String
                self.budgetLabel.text = budget
                self.budgetLabel.text = self.budgetLabel.text! + ":80"
                let yourbudget = self.langDict.valueForKey("yourBudget") as! String
                self.yourBudgetLabel.textAlignment = .Right
                self.yourBudgetLabel.text = yourbudget
                
                let expense = self.langDict.valueForKey("expense") as! String
                self.expenseLabel.text = expense
                self.expenseLabel.text = self.expenseLabel.text! + ":55"
                
                let startshopping = self.langDict.valueForKey("startShopping") as! String
                
                self.shartShoppingButton.setTitle(startshopping, forState: .Normal)
                
            }
            else if DC_DEFAULTS.valueForKey("language") as! String == "en" {
                self.title = "Buy Your Groceries"
                
                self.budgetLabel.text = "Budget"
                self.budgetLabel.text = self.budgetLabel.text! + ":80"
                self.yourBudgetLabel.text = "Your Budget"
                self.expenseLabel.text = "Expense"
                self.shartShoppingButton.setTitle("Start Shopping", forState: .Normal)
                self.expenseLabel.text = self.expenseLabel.text! + ": 55"

        }
        
  
        self.cartButton.badge.badgeValue = 1
       
        LanguageClass.shared.loadTranslation()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 4
    }

    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell : GroceriesCell!
        if DC_DEFAULTS.valueForKey("language") as? String == "ar" {
            cell = collectionView.dequeueReusableCellWithReuseIdentifier("ArabicGroceriesCell",
                                                                         forIndexPath: indexPath) as! GroceriesCell
            if(indexPath.row == 0){
                cell.itemImageView.image = UIImage(named: "1.jpg")
                
            }
            if(indexPath.row == 1){
                cell.itemImageView.image = UIImage(named: "2.jpg")
            }
            
            if(indexPath.row == 2){
                cell.itemImageView.image = UIImage(named: "3.jpg")
            }
            
            if(indexPath.row == 3){
                cell.itemImageView.image = UIImage(named: "4.jpg")
            }
            if(indexPath.row == 4){
                cell.itemImageView.image = UIImage(named: "2.jpg")
            }
            if(indexPath.row == 5){
                cell.itemImageView.image = UIImage(named: "3.jpg")
            }

        }
        else{
            cell = collectionView.dequeueReusableCellWithReuseIdentifier("EnglishGroceriesCell",
                                                                         forIndexPath: indexPath) as! GroceriesCell
            if(indexPath.row == 0){
                cell.itemImageView.image = UIImage(named: "1.jpg")
                
            }
            if(indexPath.row == 1){
                cell.itemImageView.image = UIImage(named: "2.jpg")
            }
            
            if(indexPath.row == 2){
                cell.itemImageView.image = UIImage(named: "3.jpg")
            }
            
            if(indexPath.row == 3){
                cell.itemImageView.image = UIImage(named: "4.jpg")
            }
            if(indexPath.row == 4){
                cell.itemImageView.image = UIImage(named: "2.jpg")
            }
            if(indexPath.row == 5){
                cell.itemImageView.image = UIImage(named: "3.jpg")
            }

        }
        
        return cell
        
    }
    
    
    func collectionView(collectionView: UICollectionView!,
                        layout collectionViewLayout: UICollectionViewLayout!,
                               sizeForItemAtIndexPath indexPath: NSIndexPath!) -> CGSize {
        return CGSize(width:((self.view.frame.size.width/2)-20), height:((self.view.frame.size.width/2)-20))
    }

    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
    
        self.performSegueWithIdentifier("ShowDetailFromHome", sender: indexPath)
    
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
           let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier:"GroceriesHeader" , forIndexPath: indexPath) as! GroceriesHeaderView
           if DC_DEFAULTS.valueForKey("language") as! String == "en"{
            headerView.promotionsLabel.text = "Promotions"
           }else if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
            
            let promotions = self.langDict.valueForKey("promotions") as! String
            headerView.promotionsLabel.text = promotions
            headerView.promotionsLabel.textAlignment = .Right
           }
           
            
            return headerView
            
        case UICollectionElementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "GroceriesFooter", forIndexPath: indexPath) as! GroceriesFooterView
             //footerView.englishViewAll.setTitle(" ", forState: .Disabled)
            if DC_DEFAULTS.valueForKey("language") as! String == "en"{
              // footerView.arabicViewAll.state = .Disabled
                footerView.englishViewAll.hidden = false
                footerView.arabicViewAll.hidden = true
            }else if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
                //footerView.arabicViewAll.text = " "
                footerView.englishViewAll.hidden = true
                footerView.arabicViewAll.hidden = false
            }
            
            return footerView
            
        default:
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier:"GroceriesHeader" , forIndexPath: indexPath) as! GroceriesHeaderView
            
            return headerView
            assert(false, "Unexpected element kind")
        }
        
       
    }

    
    @IBAction func startShoppingBtnClk(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Groceries", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("PopViewController") as! PopViewController
        vc.tabType = "Grocery"
        vc.clickOption = "StartShopping"
        vc.setPopinTransitionStyle(.SpringySlide)
        vc.setPopinAlignment(.Centered)
        //  vc.setPreferedPopinContentSize(CGSizeMake(self.view.frame.size.width-30,self.view.frame.size.width/2))
        vc.setPreferedPopinContentSize(CGSizeMake(self.view.frame.size.width-30,165))
        vc.setPopinOptions(.DisableParallaxEffect)
        self.presentPopinController(vc, animated:true, completion: nil)
    }
  
}
