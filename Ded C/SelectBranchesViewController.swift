//
//  SelectBranchesViewController.swift
//  Ded C
//
//  Created by iOS Dev 1 on 12/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class SelectBranchesViewController: UIViewController {

    var sectionTitleArray = NSArray()
    
    var section1Array = NSMutableArray()
    var section2Array = NSMutableArray()
    var section3Array = NSMutableArray()
    
    @IBOutlet var cartButton: BadgeButton!
    var fromWhichView = "" // Grocery or Cart Tab
    @IBOutlet var comparePriceBtn: UIButton!
    var langDict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        self.cartButton.badge.badgeValue = 1
       

        if fromWhichView == "GroceryTab" {
            NSLog("GroceryTab")
            
            self.langDict = LanguageClass.shared.languageDict.valueForKey("selectBranches") as! NSDictionary
            self.comparePriceBtn.tag = 1
            if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
                
                self.title = self.langDict.valueForKey("selectBranches") as? String
                comparePriceBtn.setTitle(self.langDict.valueForKey("selectProducts")as? String, forState: UIControlState.Normal)
            }
            else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
                
                self.title = self.langDict.valueForKey("selectBranches")as? String
                comparePriceBtn.setTitle(self.langDict.valueForKey("selectProducts")as? String, forState: UIControlState.Normal)
            }
        }
        else if fromWhichView == "CartTab"{
            self.comparePriceBtn.tag = 2
            self.langDict = LanguageClass.shared.languageDict.valueForKey("selectBranches") as! NSDictionary
            
            if DC_DEFAULTS.valueForKey("language") as! String == "ar"{
                
                self.title = self.langDict.valueForKey("selectProducts")as? String
                comparePriceBtn.setTitle(self.langDict.valueForKey("comparePrices")as? String, forState: UIControlState.Normal)

            }
            else if DC_DEFAULTS.valueForKey("language") as! String == "en"{
                
                self.title = self.langDict.valueForKey("selectProducts")as? String
                comparePriceBtn.setTitle(self.langDict.valueForKey("comparePrices")as? String, forState: UIControlState.Normal)

            }

                        NSLog("cartTab")
        }

        sectionTitleArray = ["Lulu Hyper","Hyper Panda","Oberon"]
    }

    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        else if section == 1 {
            return 3
         }
        else {
            return 5
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let  cell : BranchCell!
        if DC_DEFAULTS.valueForKey("language") as? String == "ar"{
            cell = tableView.dequeueReusableCellWithIdentifier("ArabicCellBranch", forIndexPath: indexPath) as! BranchCell
            cell.selectionStyle = .None
            NSLog("row: %d", indexPath.row)

        }
        else{
            cell = tableView.dequeueReusableCellWithIdentifier("EnglishCellBranch", forIndexPath: indexPath) as! BranchCell
            cell.selectionStyle = .None
            NSLog("row: %d", indexPath.row)
        }
        
//        if indexPath.section == 0
//        {
//            let selectRow = section1Array.objectAtIndex(indexPath.row)
//            
//            if indexPath.row == Int(selectRow as! NSNumber) {
//               NSLog("selected")
//            }
//            
//            else{
//            
//             NSLog("dselect")
//            }
//
//        }
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 40.0
        
    }
    
    ///////////////////////// TableView Header Section
    
    func tableView(tableView: UITableView,viewForHeaderInSection section: Int) -> UIView?{
        let cell : UIView!
        if DC_DEFAULTS.valueForKey("language") as? String == "ar"{
            
        cell = tableView.dequeueReusableCellWithIdentifier("ArabicHeader")! as UIView
        let titleLabel:UILabel = cell.viewWithTag(100) as! UILabel
       // NSLog("Text: %@", titleLabel.text!)
        titleLabel.text = sectionTitleArray.objectAtIndex(section) as? String
        }
        else{
            cell = tableView.dequeueReusableCellWithIdentifier("EnglishHeader")! as UIView
            let titleLabel:UILabel = cell.viewWithTag(100) as! UILabel
            // NSLog("Text: %@", titleLabel.text!)
            titleLabel.text = sectionTitleArray.objectAtIndex(section) as? String
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 60
    }

    
   /////////////////////////// Tableview Selection/Deselection 
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
//        if indexPath.section == 0 {
//            section1Array.addObject(indexPath.row)
//        }
//        else if indexPath.section == 1 {
//            section2Array.addObject(indexPath.row)
//        }
//        else if indexPath.section == 2 {
//            section3Array.addObject(indexPath.row)
//        }
////        
//        NSLog("section1: %@", section1Array)
//        NSLog("section2: %@", section2Array)
//        NSLog("section3: %@", section3Array)
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! BranchCell
        cell.labelTitle.textColor = UIColor.init(red: 36.0/255.0, green: 124.0/255.0, blue: 120.0/255.0, alpha: 1.0)
        cell.statusBtn.selected = true
       
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath){
//        if indexPath.section == 0 {
//            section1Array.removeObjectIdenticalTo(indexPath.row)
//        }
//        else if indexPath.section == 1 {
//            section2Array.removeObjectIdenticalTo(indexPath.row)
//        }
//        else if indexPath.section == 2 {
//            section3Array.removeObjectIdenticalTo(indexPath.row)
//        }

//        NSLog("Rsection1: %@", section1Array)
//        NSLog("Rsection2: %@", section2Array)
//        NSLog("Rsection3: %@", section3Array)
//
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! BranchCell
        cell.labelTitle.textColor = UIColor.blackColor()
        cell.statusBtn.selected = false
    }
    
    @IBAction func comparePriceBtnClk(sender: AnyObject) {
        if self.comparePriceBtn.tag == 1{
            let selectProductsVC = self.storyboard?.instantiateViewControllerWithIdentifier("SelectProductsViewController") as! SelectProductsViewController
            selectProductsVC.fromWhichView  = "GroceryTab"
            self.navigationController?.pushViewController(selectProductsVC, animated: true)
        }
        else if self.comparePriceBtn.tag == 2{
            let priceCompareVC = self.storyboard?.instantiateViewControllerWithIdentifier("PriceCompareViewController") as! PriceCompareViewController
            self.navigationController?.pushViewController(priceCompareVC, animated: true)
        }
    }

}
