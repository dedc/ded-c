//
//  CartCell.swift
//  Ded C
//
//  Created by iOS Dev 1 on 16/09/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {

    var count:Int!
    var typeArray = NSArray()
    var type = ""
    
    @IBOutlet var subTableView: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if type == "Meat and Poultry" {
            var tbFrame = subTableView.frame
            tbFrame.size.height = 5*35   // (section count + no of rows in each section count) * single row height
            subTableView.frame = tbFrame
        }
            
        else if type == "Milk and Eggs"{
            var tbFrame = subTableView.frame
            tbFrame.size.height = 3*34  // (section count + no of rows in each section count) * single row height
            subTableView.frame = tbFrame
            
        }
        else if type == "Fruits and Nuts"{
            var tbFrame = subTableView.frame
            tbFrame.size.height = 6*30   // (section count + no of rows in each section count) * single row height
            subTableView.frame = tbFrame
        }
        
        
        if section == 0{
            return 2
        }
        else{
            return 1
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MainSubCell")! as UITableViewCell
        let titleLabel:UILabel = cell.viewWithTag(700) as! UILabel
        
        if type == "Meat and Poultry" {
            if indexPath.section == 0{
                let array1 = ["Frozen Chicken Whole","Frozen Chicken Breast Boneless"] as NSArray
                
                titleLabel.text = array1.objectAtIndex(indexPath.row) as? String
            }
            else{
                titleLabel.text = "Fresh Beef (Australian) Boneless"
            }
        }
            
        else if type == "Milk and Eggs"{
            if indexPath.section == 0{
                let array2 = ["White Cheese Fata(Almarai)","Canned Cheddar Cheese(Kraft)"] as NSArray
               titleLabel.text = array2.objectAtIndex(indexPath.row) as? String
            }
        }
            
            
        else if type == "Fruits and Nuts"{
            if  indexPath.section == 0 {
                let array3 = ["Green Apples","Fresh Oranges"] as NSArray
                titleLabel.text = array3.objectAtIndex(indexPath.row) as? String
            }
                
            else if indexPath.section == 1{
                let array4 = ["Cashew"] as NSArray
                titleLabel.text = array4.objectAtIndex(indexPath.row) as? String
            }
//            else if indexPath.section == 2{
//                let array4 = ["Mango1"] as NSArray
//                titleLabel.text = array4.objectAtIndex(indexPath.row) as? String
//            }
            
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView,viewForHeaderInSection section: Int) -> UIView?{
        let cell = tableView.dequeueReusableCellWithIdentifier("MainSubHeader")! as UIView
        let titleLabel:UILabel = cell.viewWithTag(600) as! UILabel
        if type == "Meat and Poultry"{
            typeArray = ["Frozen Chicken","Beef-Fresh"]
        }
        else if type == "Milk and Eggs"{
            typeArray = ["Cheese"]
        }
        else if type == "Fruits and Nuts"{
            typeArray = ["Fruits","Nuts"]
        }
        
        titleLabel.text = typeArray.objectAtIndex(section) as? String
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 32
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
       return 32
        
    }

}
