//
//  DCBusinessInquiryTableVC.swift
//  Ded C
//
//  Created by Abdul Malik Aman on 04/08/16.
//  Copyright © 2016 Focaloid. All rights reserved.
//

import UIKit

class DCBusinessInquiryTableVC: UITableViewController
{

    
    @IBOutlet weak var sectionSpace: UITableViewCell!
    @IBOutlet var emirateLabel: UILabel!
    @IBOutlet var originLabel: UILabel!
    
    @IBOutlet var tradeOriginButtons: [UIButton]!
    @IBOutlet var tradeOriginLabels: [UILabel]!
    @IBOutlet var tradeOriginCheck: [UIImageView]!
    var placeholderText = "Address".localized(LANGUAGE!)
    
    @IBOutlet weak var tradeName: DCImagedTextField!
    @IBOutlet weak var tradeLicence: DCImagedTextField!
    @IBOutlet weak var webSite: DCImagedTextField!
    @IBOutlet weak var email: DCImagedTextField!
    @IBOutlet weak var phone: DCImagedTextField!
    @IBOutlet weak var mobile: DCImagedTextField!
    @IBOutlet weak var address: UITextView!
    @IBOutlet weak var poBox: UITextField!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if iPad {
            sectionSpace.backgroundColor = UIColor.clearColor()
        }
        
        self.tradeLicenseTableView.proxyCell = true

    }
    
    @IBOutlet var tradeLicenseTableView: DCAttachmentTableView!
    
    //MARK: - Actions
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 1:
            return 110
        case 9:
            return 67
        case 11:
            return 50 + CGFloat(self.tradeLicenseTableView.attachmentsArray.count * 80) // Attachments
        case 12:
            return 60
        case 13:
            return 11
        default:
            return 44
        }
    }
    @IBAction func tradeOriginAction(sender: UIButton)
    {
        if let selectedIndex = tradeOriginButtons.indexOf(sender)
        {
            for label in tradeOriginLabels
            {
                label.textColor = UIColor.lightGrayColor()
            }
            for imageView in tradeOriginCheck {
                imageView.hidden = true
            }
            tradeOriginCheck[selectedIndex].hidden = false
            tradeOriginLabels[selectedIndex].textColor = UIColor.blackColor()
            
        }
    }
    
    
    @IBAction func emiratesAction(sender: AnyObject)
    {
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var emiratesArray: [String] = []
        
        if LOOKUP_EMIRATES?.count != 0 {
            for i in 0..<LOOKUP_EMIRATES!.count {
                emiratesArray.append(LOOKUP_EMIRATES![i][fetchKey] as! String)
            }
        }
        
        
        self.showSelectionTableView("Emirates", options: emiratesArray)
        { (selectedString, index) in
            
            self.emirateLabel.text = selectedString
            
        }
    }
    

    
    
    @IBAction func originAction(sender: AnyObject)
    {
        var fetchKey: String!
        
        if LANGUAGE == "en" {
            fetchKey = "NameEn"
        }else {
            fetchKey = "NameAr"
        }
        var originArray: [String] = []
        
        if LOOKUP_ORIGIN?.count != 0 {
            for i in 0..<LOOKUP_ORIGIN!.count {
                originArray.append(LOOKUP_ORIGIN![i][fetchKey] as! String)
            }
        }
        
        self.showSelectionTableView("ORIGIN", options: originArray)
        { (selectedString, index) in
            
            self.originLabel.text = selectedString
            
        }
    }
    
    
    @IBAction func InquieryDetails(sender: AnyObject)
    {
        let InquierydetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.InquierydetailsVC)
        self.navigationController?.pushViewController(InquierydetailsVC!, animated: true)
        
    }
    
    
    @IBAction func TermsNConditions(sender: AnyObject)
    {
        let TermsNConditionsVC = self.storyboard?.instantiateViewControllerWithIdentifier(DCViewControllerIdentifiers.TermsNConditionsVC)
        self.navigationController?.pushViewController(TermsNConditionsVC!, animated: true)
    }

    //MARK : TextViewDelegate Methods
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if textView.text == placeholderText
        {
            textView.textColor = UIColor.blackColor()
            textView.text = ""
        }
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == placeholderText || textView.text == ""
        {
            textView.textColor = UIColor.lightGrayColor()
            textView.text = placeholderText
        }
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func tradeLicenseAction(sender: AnyObject)
    {
        if self.tradeLicenseTableView.attachmentsArray.count > 0
        {
            if self.tradeLicenseTableView.attachmentsArray[0].images.count >= 5
            {
                UIAlertView(title: "", message: "5 Images are supported", delegate: nil, cancelButtonTitle: "OK").show()
                return
            }
        }
        
        self.openImagesActionSheet
            { (image) in
                
                if let pickedImage = image
                {
                    if self.tradeLicenseTableView.attachmentsArray.count > 0
                    {
                        self.tradeLicenseTableView.attachmentsArray[0].images.append(pickedImage)
                    }
                    else
                    {
                        let attachment = DCAttachmentObject()
                        attachment.images.append(pickedImage)
                        self.tradeLicenseTableView.attachmentsArray = [attachment]
                    }
                    
                    self.tradeLicenseTableView.commentArray.append("")
                    self.tradeLicenseTableView.reloadData()
                    self.tableView.reloadData()
                }
                
        }

    }

}
